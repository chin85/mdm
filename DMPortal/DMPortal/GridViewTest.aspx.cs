﻿using DMPortal.Object;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class GridViewTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    BindGrid();
            //}

            if (!Page.IsPostBack)
            {
                createDT();

                //Bind data to the GridView control.
                BindData();
            }

        }

        private void createDT()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetObjAttVal", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@obj_id", SqlDbType.Int).Value = 1;
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "Obj");

                DataTable dt = ds.Tables["Obj"];

                //// Create a new table.
                //DataTable taskTable = new DataTable("TaskList");

                //for (int i = 0; i < dt.Columns.Count; i++)
                //{
                //    taskTable.Columns.Add(dt.Columns[i].ColumnName, typeof(string));

                //    DataRow tableRow = taskTable.NewRow();
                //    tableRow[dt.Columns[i].ColumnName] = dt.Rows[i][dt.Columns[i].ColumnName].ToString();
                //    taskTable.Rows.Add(tableRow);
                //}

                //Persist the table in the Session object.
                Session["TaskTable"] = dt;

                con.Close();
            }

        }

        protected void TaskGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TaskGridView.PageIndex = e.NewPageIndex;
            //Bind data to the GridView control.
            BindData();
        }

        protected void TaskGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Reset the edit index.
            TaskGridView.EditIndex = -1;
            //Bind data to the GridView control.
            BindData();
        }

        protected void TaskGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            TaskGridView.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            BindData();
        }

        protected void TaskGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //Retrieve the table from the session object.
            DataTable dt = (DataTable)Session["TaskTable"];

            int row_id = Convert.ToInt32(TaskGridView.DataKeys[e.RowIndex].Values[0]);

            //Update the values.
            GridViewRow row = TaskGridView.Rows[e.RowIndex];
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if(i > 1)
                    dt.Rows[row.DataItemIndex][dt.Columns[i].ColumnName] = ((TextBox)(row.Cells[i + 1].Controls[0])).Text;
            }

            //Reset the edit index.
            TaskGridView.EditIndex = -1;

            //Bind data to the GridView control.
            BindData();
        }

        private void BindData()
        {
            TaskGridView.DataSource = Session["TaskTable"];
            TaskGridView.DataBind();


        }

        protected void TaskGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowState & DataControlRowState.Edit) > 0) // Only on Edit row
            {
                for (Int16 I = 1; I < e.Row.Cells.Count; I++)    // Start @ 2nd col if 1st is Command Buttons
                {
                    if (I == 2 || I == 3)                                 // Allow Editing in 4th column
                    {
                        TextBox tb = e.Row.Cells[I].Controls[0] as TextBox;   // Try to cast control to TextBox
                        if ((tb != null))
                        {
                            tb.ReadOnly = true;                    // Set ReadOnly
                            tb.Enabled = false;                    // Disable TextBox
                        }
                    }
                }
            }
        }

        //private void BindGrid()
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
        //    {
        //        con.Open();
        //        SqlCommand cmd = new SqlCommand("GetObjAttVal", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@obj_id", SqlDbType.Int).Value = 1;
        //        SqlDataAdapter sql = new SqlDataAdapter(cmd);
        //        DataSet ds = new DataSet();
        //        sql.Fill(ds, "Obj");

        //        DataTable dt = ds.Tables["Obj"];

        //        for (int i = 0; i < dt.Columns.Count; i++)
        //        {
        //            TemplateField tf1 = new TemplateField();
        //            tf1.HeaderTemplate = new GridViewTemplate(DataControlRowType.Header, dt.Columns[i].ColumnName, dt.Columns[i].ColumnName, new Label());
        //            tf1.ItemTemplate = new GridViewTemplate(DataControlRowType.DataRow, dt.Columns[i].ColumnName, dt.Columns[i].ColumnName, new Label());

        //            if (i > 2)
        //                tf1.EditItemTemplate = new GridViewTemplate(DataControlRowType.DataRow, dt.Columns[i].ColumnName, dt.Columns[i].ColumnName, new TextBox());
        //            gridData.Columns.Add(tf1);
        //        }

        //        gridData.DataSource = dt;
        //        gridData.DataBind();

        //        con.Close();
        //    }
        //}

        ////protected void OnUpdate(object sender, EventArgs e)
        ////{
        ////    GridViewRow row = (sender as LinkButton).NamingContainer as GridViewRow;
        ////    int x = row.Cells.Count;
        ////    string name = (row.Cells[0].Controls[0] as TextBox).Text;
        ////    string country = (row.Cells[1].Controls[0] as TextBox).Text;
        ////    //DataTable dt = ViewState["dt"] as DataTable;
        ////    //dt.Rows[row.RowIndex]["Name"] = name;
        ////    //dt.Rows[row.RowIndex]["Country"] = country;
        ////    //ViewState["dt"] = dt;
        ////    //GridView1.EditIndex = -1;
        ////    //this.BindGrid();
        ////}

        ////protected void OnCancel(object sender, EventArgs e)
        ////{

        ////}

        //protected void gridData_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gridData.EditIndex = e.NewEditIndex;
        //    this.BindGrid();
        //}

        //protected void gridData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    //GridViewRow row = (GridViewRow)gridData.Rows[e.RowIndex];

        //    GridViewRow row = gridData.Rows[e.RowIndex];
        //    int id = Int32.Parse(gridData.DataKeys[e.RowIndex].Value.ToString());
        //   //string a =  ((TextBox)(row.Cells[4].Controls[0])).Text;

        //}

        //protected void gridData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gridData.EditIndex = -1;
        //    this.BindGrid();
        //}


    }
}