﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MakerDashboard.aspx.cs" Inherits="DMPortal.MakerDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Maker Summary</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-4 control-label" style="width: 120px">Project : </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" Width="290px" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-4 control-label" style="width: 120px">Object : </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlobjid" runat="server" CssClass="form-control" Width="290px">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="3" class="auto-style1" style="width: 100px; text-align: center">
                                        <asp:Button ID="ObjBtn" runat="server" OnClick="Obj_Search" Text="Search" CssClass="btn btn-success" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="obj_id" EmptyDataText="No records." OnPageIndexChanging="gv_PageIndexChanging" OnRowDataBound="gv_RowDataBound" >
                        <Columns>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Project" DataField="Project"></asp:BoundField>
                            <asp:BoundField HeaderText="Source System" DataField="Source_Sys"></asp:BoundField>
                            <asp:BoundField HeaderText="Object" DataField="obj"></asp:BoundField>
                            <asp:BoundField HeaderText="Approved (Count)" DataField="APPROVED"></asp:BoundField>
                            <asp:BoundField HeaderText="Rejected (Count)" DataField="REJECTED"></asp:BoundField>
                            <asp:BoundField HeaderText="Awaiting Approval (Count)" DataField="AWAITING_APPROVAL"></asp:BoundField>
							<%--<asp:BoundField HeaderText="Reject Reasons" DataField="REJECT_REASONS"></asp:BoundField>--%>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("obj_id")%>'
                                        Font-Underline="false" OnClick="gv_edit" CausesValidation="false">Details</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ObjBtn" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
</div>
</asp:Content>
