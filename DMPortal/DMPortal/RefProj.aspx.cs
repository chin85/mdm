﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class RefProj : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                ViewState["proj"] = string.Empty;
                //btndelete.Enabled = false;
                bindgv();
            }
        }

        private void bindgv()
        {
            string srcProj = ViewState["proj"].ToString();

            using (MDMEntities context = new MDMEntities())
            {
                var oObj = (from c in context.Ref_Proj
                            select new { c.ID, c.Project, c.Description, c.MODIFY_DATETIME }).ToList();

                if (!string.IsNullOrEmpty(srcProj))
                    oObj = oObj.Where(x => x.Project.ToLower().Contains(srcProj.ToLower())).ToList();

                gv.DataSource = oObj;
                gv.DataBind();
            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        Ref_Proj _oproj = context.Ref_Proj.SingleOrDefault(i => i.ID == id);

                        if (_oproj != null)
                        {
                            context.Ref_Proj.Remove(_oproj);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj " + _oproj.Project + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);

                        bindgv();
                    }
                }
            }
            bindgv();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            //try
            //{
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            if (txtr1.Text != null)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        var role = context.Ref_Proj.Where(i => i.Project == txtr1.Text).SingleOrDefault();
                        if (role != null)
                        {
                            UserAlert.ShowAlert("Project exist.", CommonEnum.AlertType.Exclamation);
                            return;

                        }
                        else
                        {
                            Ref_Proj oProj = new Ref_Proj();
                            oProj.Project = txtr1.Text;
                            oProj.Description = txtdesc.Text;
                            oProj.CREATED_BY = this.logonId;
                            oProj.CREATE_DATETIME = DateTime.Now;
                            oProj.MODIFIER_ID = this.logonId;
                            oProj.MODIFY_DATETIME = DateTime.Now;
                            context.Ref_Proj.Add(oProj);
                            context.SaveChanges();
                            pkid = (int)oProj.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Proj", FIELD_NAME = "Project", OLD_VALUE = "", NEW_VALUE = txtr1.Text, DESC = "New project name added.", CREATOR_ID = this.logonId });
                            //oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Proj", FIELD_NAME = "Description", OLD_VALUE = "", NEW_VALUE = txtr1.Text, DESC = "New project description added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var role = context.Ref_Proj.Where(i => i.ID == id).SingleOrDefault();

                        if (role != null)
                        {
                            if (role.Project != txtr1.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Proj", FIELD_NAME = "Project", OLD_VALUE = role.Project, NEW_VALUE = txtr1.Text, DESC = "Ref. project name has been modified", CREATOR_ID = this.logonId });
                            if (role.Description != txtdesc.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Proj", FIELD_NAME = "Description", OLD_VALUE = role.Description, NEW_VALUE = txtdesc.Text, DESC = "Ref. project description has been modified", CREATOR_ID = this.logonId });

                            role.Project = txtr1.Text;
                            role.Description = txtdesc.Text;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;

                        }

                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }

            }
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}

            bindgv();
            txtr1.Text = string.Empty;
            txtdesc.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtr1.Text = "";
            txtdesc.Text = "";
            ViewState["PageMode"] = "A";
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv();
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var proj = context.Ref_Proj.Where(i => i.ID == id).SingleOrDefault();

                    if (proj != null)
                    {
                        txtr1.Text = proj.Project;
                        txtdesc.Text = proj.Description;
                    }
                }
            }
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            ViewState["proj"] = txtproj.Text;

            bindgv();
        }
    }
}