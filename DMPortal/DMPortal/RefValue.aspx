﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefValue.aspx.cs" Inherits="DMPortal.RefValue" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/comm.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to delete?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert runat="server" ID="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Reference Object Value</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-md-4 control-label">Ref. Object: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlobj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlobj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlobj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. object."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4 control-label">Ref. Object Type: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddltype_SelectedIndexChanged">
                                            <asp:ListItem Value="" Selected="True">- SELECT -</asp:ListItem>
                                            <asp:ListItem Value="LIST"> List </asp:ListItem>
                                            <asp:ListItem Value="RANGE"> Range </asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddltype" InitialValue="" ForeColor="Red" ErrorMessage="Please select object type."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="col-md-4 control-label">Ref. Object Attribute: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlatt" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlatt" InitialValue="" ForeColor="Red" ErrorMessage="Please select object attribute."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" OnPageIndexChanging="gv_PageIndexChanging" EmptyDataText="No records." OnRowDataBound="gv_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server"  onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("ID")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Reference Object" DataField="obj"></asp:BoundField>
                            <asp:TemplateField HeaderText="Min Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblMin" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Max Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblMax" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reference Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblval" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Reference Desc." DataField="obj_value_desc"></asp:BoundField>
                            <asp:BoundField HeaderText="Last Modified" DataField="MODIFY_DATETIME" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ID")%>'
                                        Font-Underline="false" OnClick="gv_edit" CausesValidation="false">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btndelete" />
                    <asp:AsyncPostBackTrigger ControlID="btnsave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger" CausesValidation="false" OnClick="btndelete_Click" OnClientClick="return checkselector(this)" />
        </div>
        <br />
        <br />
        <div class="col-md-12" style="text-align: right">
            <div style="margin: auto">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <legend>Reference Object Detail</legend>

                        <div id="rdiv" runat="server">
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">* Min Value :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtmin" runat="server" CssClass="form-control input-md" onkeypress="NumAndsixDecimals(event,this)" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtmin" ForeColor="Red" ErrorMessage="can not be empty."></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">* Max Value :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtmax" runat="server" CssClass="form-control input-md" onkeypress="NumAndsixDecimals(event,this)" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtmax" ForeColor="Red" ErrorMessage="can not be empty."></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div id="ldiv" runat="server">
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">* List Value :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtvalue" runat="server" CssClass="form-control input-md" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtvalue" ForeColor="Red" ErrorMessage="can not be empty."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <%--<div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">Value Description :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtdesc" runat="server" CssClass="form-control input-md" Enabled="true"></asp:TextBox>

                            </div>
                        </div>--%>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                        <asp:AsyncPostBackTrigger ControlID="btncancel" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnsave_Click" />
            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
        </div>
    </div>
</asp:Content>
