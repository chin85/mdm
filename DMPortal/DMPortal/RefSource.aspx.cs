﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class RefSource : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                ViewState["source"] = string.Empty;
                //btndelete.Enabled = false;
                bindDLL();
                bindgv(0);
            }
        }

        private void bindgv(int projid)
        {
            string src = ViewState["source"].ToString();

            using (MDMEntities db = new MDMEntities())
            {
                var sourcesys = (from pd in db.Ref_Proj
                                 join od in db.Ref_Source on pd.ID equals od.Proj_Id
                                 where pd.ID == projid
                                 orderby od.Source_Sys
                                 select new
                                 {
                                     od.ID,
                                     od.Source_Sys,
                                     od.Description,
                                     od.MODIFY_DATETIME,
                                     pd.Project
                                 }).ToList();

                if (!string.IsNullOrEmpty(src))
                    sourcesys = sourcesys.Where(x => x.Source_Sys.ToLower().Contains(src.ToLower())).ToList();

                gv.DataSource = sourcesys;
                gv.DataBind();
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {

                var oproj = (from c in db.Ref_Proj
                             select new { c.ID, objDESC = c.Project + " - " + c.Description }).OrderBy(i => i.objDESC).ToList();

                if (oproj.Count > 0)
                {
                    foreach (var item in oproj)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            //try
            //{
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            if (txtsn.Text != null)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        var role = context.Ref_Source.Where(i => i.Proj_Id == projid && i.Source_Sys == txtsn.Text).SingleOrDefault();
                        if (role != null)
                        {
                            UserAlert.ShowAlert("Source exist.", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                        else
                        {
                            Ref_Source srefobj = new Ref_Source();
                            srefobj.Source_Sys = txtsn.Text;
                            srefobj.Description = txtdesc.Text;
                            srefobj.Proj_Id = projid;
                            srefobj.CREATED_BY = this.logonId;
                            srefobj.CREATE_DATETIME = DateTime.Now;
                            srefobj.MODIFIER_ID = this.logonId;
                            srefobj.MODIFY_DATETIME = DateTime.Now;
                            context.Ref_Source.Add(srefobj);
                            context.SaveChanges();
                            pkid = (int)srefobj.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Source", FIELD_NAME = "Source_Sys", OLD_VALUE = "", NEW_VALUE = txtsn.Text, DESC = "New Source name added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var role = context.Ref_Source.Where(i => i.ID == id).SingleOrDefault();

                        if (role != null)
                        {
                            if (role.Source_Sys != txtsn.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Source", FIELD_NAME = "Source_Sys", OLD_VALUE = role.Source_Sys, NEW_VALUE = txtsn.Text, DESC = "Ref. Source name has been modified", CREATOR_ID = this.logonId });
                            if (role.Description != txtdesc.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Source", FIELD_NAME = "Description", OLD_VALUE = role.Description, NEW_VALUE = txtsn.Text, DESC = "Ref. Source desc. has been modified", CREATOR_ID = this.logonId });

                            role.Proj_Id = projid;
                            role.Source_Sys = txtsn.Text;
                            role.Description = txtdesc.Text;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;

                        }

                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }

            }
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}

            bindgv(ddlproj.SelectedValue.ConvertTo<int>());
            //ddlproj.SelectedValue = "0";
            txtsn.Text = string.Empty;
            txtdesc.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            //ddlproj.SelectedValue = "0";
            txtsn.Text = "";
            txtdesc.Text = "";
            ViewState["PageMode"] = "A";
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        Ref_Source _osource = context.Ref_Source.SingleOrDefault(i => i.ID == id);

                        if (_osource != null)
                        {
                            context.Ref_Source.Remove(_osource);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Source", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Source " + _osource.Source_Sys + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);

                        bindgv(ddlproj.SelectedValue.ConvertTo<int>());
                    }
                }
            }
            bindgv(ddlproj.SelectedValue.ConvertTo<int>());
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv(ddlproj.SelectedValue.ConvertTo<int>());
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var source = context.Ref_Source.Where(i => i.ID == id).SingleOrDefault();

                    if (source != null)
                    {
                        txtsn.Text = source.Source_Sys;
                        txtdesc.Text = source.Description;
                        ddlproj.SelectedValue = source.Proj_Id.ToString();
                    }
                }
            }
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            ViewState["source"] = txtSource.Text;
            bindgv(projid);
        }
    }
}