﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefObj.aspx.cs" Inherits="DMPortal.WebForm1" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to delete?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert runat="server" ID="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Reference Object</h4>
            </div>
        </div>
          <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-md-4 control-label">Project: </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. Project."></asp:RequiredFieldValidator>
                                    </td>
                                    
                                </tr>
                                  <tr>
                                    <td class="col-md-4 control-label">Source Sys.: </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlsource" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                       
                                    </td>
                                 
                                </tr>
                                 <tr>
                                    <td class="col-md-4 control-label">Object: </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:TextBox ID="txtobj" runat="server" CssClass="form-control"></asp:TextBox>                                
                                    </td>
                                      <td colspan="3" class="auto-style1" style="width: 100px; text-align: center">
                                        <asp:Button ID="ObjBtn" runat="server" OnClick="Obj_Search" Text="Search" CssClass="btn btn-success" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" OnPageIndexChanging="gv_PageIndexChanging" EmptyDataText="No records.">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("ID")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Object" DataField="obj"></asp:BoundField>
                            <asp:BoundField HeaderText="Source System" DataField="Source_Sys"></asp:BoundField>
                            <asp:BoundField HeaderText="Last Modified" DataField="MODIFY_DATETIME" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ID")%>'
                                        Font-Underline="false" OnClick="gv_edit" CausesValidation="false">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btndelete" />
                    <asp:AsyncPostBackTrigger ControlID="btnsave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger" CausesValidation="false" OnClick="btndelete_Click" OnClientClick="return checkselector(this)" />
        </div>
        <br />
        <br />
        <div class="col-md-12" style="text-align: right">
            <div style="margin: auto">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <legend>Reference Object</legend>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">*Ref. Object Name :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtr1" runat="server" class="form-control input-md" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtr1" ForeColor="Red" ErrorMessage="Object can not be empty."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                       
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                        <asp:AsyncPostBackTrigger ControlID="btncancel" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnsave_Click" />
            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
        </div>
    </div>
</asp:Content>
