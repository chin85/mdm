﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class RefObjAtt : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                ViewState["RPageIndex"] = 0;
                ViewState["obj_attribute"] = string.Empty;
                bindDLL();
                txtatt.Enabled = false;
                rbl.Enabled = false;
                ddlvalidationfile.Enabled = false;
                bindemptygvrule();
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            int rid = ViewState["ID"].ConvertTo<int>();
            int oid = ddlobj.SelectedValue.ConvertTo<int>();
            string fieldno = string.Empty;
            int colno = 0;
            int rowno = 0;
            //int ruleno = 0;
            //string dbRefTbl = string.Empty;
            //string dbRefField = string.Empty;

            if (oid != 0)
            {
                foreach (GridViewRow row in gvrule.Rows)
                {
                    LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                    HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                    int id = ctl.Value.ConvertTo<int>();

                    if (id > 0)
                    {
                        if (ctl != null && ctl.Checked)
                        {
                            fieldno = row.Cells[1].Text;
                            colno = row.Cells[4].Text.ConvertTo<int>();
                            rowno = row.Cells[5].Text.ConvertTo<int>();
                            //ruleno = row.Cells[6].Text.ConvertTo<int>();
                            //dbRefTbl = row.Cells[7].Text;
                            //dbRefField = row.Cells[8].Text;
                        }
                    }
                }

                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        Ref_Obj_Att orefatt = new Ref_Obj_Att();

                        orefatt = context.Ref_Obj_Att.Where(i => i.obj_id == oid && i.obj_attribute == txtatt.Text).SingleOrDefault();

                        if (orefatt != null)
                        {
                            UserAlert.ShowAlert("Attribute exist. Please select/ key in a new attribute.", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                        else
                        {

                            Ref_Obj_Att srefatt = new Ref_Obj_Att();
                            srefatt.obj_id = oid;
                            srefatt.obj_attribute = txtatt.Text;
                            srefatt.att_desc = txtDesc.Text;
                            srefatt.att_data_mapped = rbl.SelectedValue;
                            srefatt.field_no = fieldno;
                            srefatt.ref_rule_id = ddlvalidationfile.SelectedValue.ConvertTo<int>();
                            srefatt.rule_col = colno;
                            srefatt.rule_row = rowno;
                            if (ddllovatt.SelectedValue.ConvertTo<int>() == 0 || ddllovatt.SelectedValue == "")
                                srefatt.lov_att_id = null;
                            else
                                srefatt.lov_att_id = ddllovatt.SelectedValue.ConvertTo<int>();
                            srefatt.CREATED_BY = this.logonId;
                            srefatt.CREATE_DATETIME = DateTime.Now;
                            srefatt.MODIFIER_ID = this.logonId;
                            srefatt.MODIFY_DATETIME = DateTime.Now;
                            //srefatt.rule_no = ruleno;
                            //srefatt.DB_Ref_Tbl_Col = dbRefField;
                            //srefatt.DB_Ref_Tbl = dbRefTbl;
                            context.Ref_Obj_Att.Add(srefatt);
                            context.SaveChanges();
                            pkid = (int)srefatt.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "obj_attribute", OLD_VALUE = "", NEW_VALUE = txtatt.Text, DESC = "New obj attribute added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var drefatt = context.Ref_Obj_Att.Where(i => i.ID == rid).SingleOrDefault();

                        if (drefatt != null)
                        {
                            if (drefatt.obj_attribute != txtatt.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "obj_attribute", OLD_VALUE = drefatt.obj_attribute, NEW_VALUE = txtatt.Text, DESC = "Ref. obj attribute value has been modified", CREATOR_ID = this.logonId });
                            if (drefatt.att_desc != txtDesc.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "att_desc", OLD_VALUE = drefatt.att_desc, NEW_VALUE = txtDesc.Text, DESC = "Ref. obj attribute desc. has been modified", CREATOR_ID = this.logonId });
                            if (drefatt.att_data_mapped != rbl.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "att_data_mapped", OLD_VALUE = drefatt.att_data_mapped, NEW_VALUE = rbl.SelectedValue, DESC = "Ref. obj attribute's data mapped flag. has been modified", CREATOR_ID = this.logonId });
                            if (drefatt.ref_rule_id != ddlvalidationfile.SelectedValue.ConvertTo<int>())
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "ref_rule_id", OLD_VALUE = drefatt.ref_rule_id.ToString(), NEW_VALUE = ddlvalidationfile.SelectedValue, DESC = "Attribute's validation rule (file) has been modified", CREATOR_ID = this.logonId });
                            if (drefatt.field_no != fieldno)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "field_no", OLD_VALUE = drefatt.field_no, NEW_VALUE = fieldno, DESC = "Attribute's validation rule has been modified", CREATOR_ID = this.logonId });
                            if (drefatt.lov_att_id != ddllovatt.SelectedValue.ConvertTo<int>())
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "lov_at_id", OLD_VALUE = drefatt.lov_att_id.ToString(), NEW_VALUE = ddllovatt.SelectedValue, DESC = "Attribute's LOV has been modified", CREATOR_ID = this.logonId });
                            //if (drefatt.rule_no != ruleno)
                            //    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "rule_no", OLD_VALUE = drefatt.rule_no.ToString(), NEW_VALUE = ruleno.ToString(), DESC = "Attribute's validation rule no has been modified", CREATOR_ID = this.logonId });
                            //if (drefatt.DB_Ref_Tbl != dbRefTbl)
                            //    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "DB_Ref_Tbl", OLD_VALUE = drefatt.DB_Ref_Tbl, NEW_VALUE = dbRefTbl, DESC = "Attribute's validation ref tbl has been modified", CREATOR_ID = this.logonId });
                            //if (drefatt.DB_Ref_Tbl_Col != dbRefField)
                            //    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "DB_Ref_Tbl_Col", OLD_VALUE = drefatt.DB_Ref_Tbl_Col, NEW_VALUE = dbRefField, DESC = "Attribute's validation ref tbl col has been modified", CREATOR_ID = this.logonId });

                            drefatt.ref_rule_id = ddlvalidationfile.SelectedValue.ConvertTo<int>();
                            drefatt.field_no = fieldno;
                            drefatt.rule_col = colno;
                            drefatt.rule_row = rowno;
                            drefatt.obj_attribute = txtatt.Text;
                            drefatt.att_desc = txtDesc.Text;
                            drefatt.att_data_mapped = rbl.SelectedValue;
                            if (ddllovatt.SelectedValue.ConvertTo<int>() == 0 || ddllovatt.SelectedValue == "")
                                drefatt.lov_att_id = null;
                            else
                                drefatt.lov_att_id = ddllovatt.SelectedValue.ConvertTo<int>();
                            drefatt.MODIFIER_ID = this.logonId;
                            drefatt.MODIFY_DATETIME = DateTime.Now;
                            //drefatt.rule_no = ruleno;
                            //drefatt.DB_Ref_Tbl = dbRefTbl;
                            //drefatt.DB_Ref_Tbl_Col = dbRefField;
                        }
                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }
            }

            bindgv(ddlobj.SelectedValue.ConvertTo<int>());
            txtatt.Text = string.Empty;
            txtDesc.Text = string.Empty;
            rbl.SelectedValue = "N";
            ViewState["PageMode"] = "A";
            ddlvalidationfile.SelectedValue = "0";
            ddllovobj.SelectedValue = "0";
            ddllovatt.SelectedValue = "0";
            ViewState["obj_attribute"] = string.Empty;
            bindemptygvrule();
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var oref = context.Ref_Obj_Att.Where(i => i.ID == id).SingleOrDefault();

                    if (oref != null)
                    {
                        int ruleid;
                        ViewState["FieldNo"] = oref.field_no;
                        ViewState["RuleFileId"] = oref.ref_rule_id;
                        ViewState["obj_attribute"] = oref.obj_attribute;
                        ruleid = oref.ref_rule_id == null ? 0 : oref.ref_rule_id.ConvertTo<int>();
                        var lovobj = context.Ref_Obj_Att.Where(j => j.ID == oref.lov_att_id).SingleOrDefault();
                        if (lovobj != null)
                        {
                            ddllovobj.SelectedValue = lovobj.obj_id == null ? "0" : lovobj.obj_id.ToString();
                            var obj = lovobj.obj_id == null ? 0 : lovobj.obj_id.ConvertTo<int>();
                            bindlovatt(obj);
                            ddllovatt.SelectedValue = lovobj.ID == null ? "0" : lovobj.ID.ToString();
                        }
                        else
                        {
                            ddllovobj.SelectedValue = "0";
                            ddllovatt.SelectedValue = oref.lov_att_id == null ? "0" : oref.lov_att_id.ToString();
                        }
                        txtatt.Text = oref.obj_attribute;
                        txtDesc.Text = oref.att_desc;
                        rbl.SelectedValue = oref.att_data_mapped;
                        ddlvalidationfile.SelectedValue = oref.ref_rule_id == null ? "0" : oref.ref_rule_id.ToString();

                        bindgvrule(ruleid);
                    }
                }
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {

                //var role = (from c in db.Ref_Obj
                //            select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();
                //var validation = db.Ref_Rule.ToList();

                //if (role.Count > 0)
                //{
                //    foreach (var item in role)
                //    {
                //        var datavalue = item.ID.ToString();
                //        var datatext = item.objDESC;
                //        bindddlobject(ddlobj, datatext, datavalue);
                //    }
                //    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //}

                //if (validation.Count > 0)
                //{
                //    foreach (var item in validation)
                //    {
                //        var datavalue = item.ID.ToString();
                //        var datatext = item.File_Name;
                //        bindddlobject(ddlvalidationfile, datatext, datavalue);
                //    }
                //    ddlvalidationfile.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //}

                var role = (from c in db.Ref_Proj
                            select new { c.ID, objDESC = c.Project }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void ddlobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtatt.Text = string.Empty;
            txtDesc.Text = string.Empty;
            rbl.SelectedValue = "N";
            ddlvalidationfile.SelectedValue = "0";
            bindemptygvrule();
            txtatt.Enabled = true;
            rbl.Enabled = true;
            ddlvalidationfile.Enabled = true;

            var _obj = ddlobj.SelectedValue.ConvertTo<int>();
            if (_obj != 0)
            {
                bindgv(_obj);
            }
            else
            {
                bindemptygvrule();
            }

            using (MDMEntities db = new MDMEntities())
            {
                var lovobj = (from pd in db.Ref_Obj
                              from sd in db.Ref_Source
                              where pd.Proj_id == sd.Proj_Id && pd.Source_Sys == sd.ID && pd.ID != _obj
                              select new { pd.ID, pd.obj, pd.Proj_id, sd.Source_Sys }).ToList();

                var lovobjproj = (from o in lovobj
                                  join p in db.Ref_Proj on o.Proj_id equals p.ID
                                  select new { o.ID, objDESC = p.Project + "_" + o.Source_Sys + ":" + o.obj }).OrderBy(i => i.objDESC).ToList();

                if (lovobjproj.Count > 0)
                {
                    foreach (var item in lovobjproj)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddllovobj, datatext, datavalue);
                    }
                    ddllovobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindgv(int objid)
        {
            using (MDMEntities db = new MDMEntities())
            {
                var objatt = (from pd in db.Ref_Obj_Att
                              join od in db.Ref_Obj on pd.obj_id equals od.ID
                              join ld in db.Ref_Obj_Att on pd.lov_att_id equals ld.ID
                              into MatchedOrders
                              from mo in MatchedOrders.DefaultIfEmpty()
                              where pd.obj_id == objid
                              select new
                              {
                                  pd.ID,
                                  pd.obj_attribute,
                                  pd.att_desc,
                                  pd.MODIFY_DATETIME,
                                  pd.field_no,
                                  od.obj,
                                  lov_att = mo.obj_attribute,
                              }).ToList();

                gv.DataSource = objatt;
                gv.DataBind();
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv(ddlobj.SelectedValue.ConvertTo<int>());
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();

            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {

                        Ref_Obj_Att _robjatt = context.Ref_Obj_Att.SingleOrDefault(i => i.ID == id);

                        if (_robjatt != null)
                        {
                            context.Ref_Obj_Att.Remove(_robjatt);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Att", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_Att " + _robjatt.obj_attribute + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv(ddlobj.SelectedValue.ConvertTo<int>());
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtatt.Text = string.Empty;
            txtDesc.Text = string.Empty;
            rbl.SelectedValue = "N";
            ddlvalidationfile.SelectedValue = "0";
            bindemptygvrule();
            ViewState["PageMode"] = "A";
        }

        protected void ddlvalidationfile_SelectedIndexChanged(object sender, EventArgs e)
        {

            var _obj = ddlvalidationfile.SelectedValue.ConvertTo<int>();
            if (_obj != 0)
            {
                bindgvrule(_obj);
            }
            else
            {
                bindemptygvrule();
            }
        }

        private void bindgvrule(int objid)
        {
            using (MDMEntities db = new MDMEntities())
            {
                var objvalidation = db.Ref_Rule.Where(i => i.ID == objid).SingleOrDefault();
                if (objvalidation != null)
                {
                    var validation_file_path = objvalidation.File_Path;
                    //readValidationRule(validation_file_path);
                    readValidationRuleDB(objvalidation.ID.ConvertTo<int>());
                }
                else
                {
                    bindemptygvrule();
                }
            }
        }

        private void bindemptygvrule()
        {
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();
            dt.Columns.Add("Field No");
            dt.Columns.Add("Field Name");
            dt.Columns.Add("Rules");
            dt.Columns.Add("Col No");
            dt.Columns.Add("Row No");
            //dt.Columns.Add("Rule No");
            //dt.Columns.Add("DB Ref Table");
            //dt.Columns.Add("DB Ref Field");
            dr["Field No"] = "0";
            dr["Field Name"] = "NA";
            dr["Col No"] = "NA";
            dr["Row No"] = "NA";
            //dr["Rule No"] = "NA";
            //dr["DB Ref Table"] = "NA";
            //dr["DB Ref Field"] = "NA";
            dt.Rows.Add(dr);

            gvrule.DataSource = dt;
            gvrule.DataBind();
            gvrule.Rows[0].Visible = false;
        }

        private void readValidationRuleDB(int Ruleid)
        {
            if (Ruleid > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Field No");
                dt.Columns.Add("Field Name");
                dt.Columns.Add("Rules");
                dt.Columns.Add("Col No");
                dt.Columns.Add("Row No");
                using (MDMEntities db = new MDMEntities())
                {
                    var ruledtl = db.Ref_Rule_Dtl.Where(i => i.Ref_Rule_ID == Ruleid).ToList();

                    foreach (var item in ruledtl)
                    {
                        DataRow dr = dt.NewRow();
                        dr["Field No"] = item.Field_No;
                        dr["Field Name"] = item.Field_Name;
                        dr["Rules"] = item.Rules;
                        dr["Col No"] = item.ID;
                        dr["Row No"] = 0;
                        dt.Rows.Add(dr); // adding Row into DataTable
                        dt.AcceptChanges();
                    }

                    gvrule.DataSource = dt;
                    gvrule.DataBind();
                }
            }
            else
            {
                bindemptygvrule();
            }
        }
        //private void readValidationRule(string filepath)
        //{

        //    if (File.Exists(filepath))
        //    {
        //        string Path = ConfigurationManager.AppSettings["LogDir"].ToString();

        //        if (Directory.Exists(Path) == false)
        //            Directory.CreateDirectory(Path);

        //        System.IO.FileStream fs = new System.IO.FileStream(Path + "ErrorLog.txt", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite);
        //        System.IO.StreamWriter s = new System.IO.StreamWriter(fs);
        //        s.BaseStream.Seek(0, System.IO.SeekOrigin.End);
        //        s.WriteLine("\nRead Validation File DATE: " + System.DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture) + " \nFile Path: " + filepath);
        //        s.WriteLine("-------------------------------------------------------------------------------------------------------------");
        //        s.Close();

        //        Microsoft.Office.Interop.Excel.Application xlexcel;
        //        Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        //        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
        //        Microsoft.Office.Interop.Excel.Range xlRange;
        //        int Cnum = 0;
        //        int Rnum = 0;

        //        object misValue = System.Reflection.Missing.Value;
        //        xlexcel = new Microsoft.Office.Interop.Excel.Application();

        //        xlexcel.Visible = false;

        //        //Open a File
        //        xlWorkBook = xlexcel.Workbooks.Open(filepath, 0, false, 5, "", "", true,
        //        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

        //        //Set Sheet 1 as the sheet you want to work with
        //        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        //        //gives the used cells in sheet
        //        xlRange = xlWorkSheet.UsedRange;

        //        int rowCount = xlRange.Rows.Count;
        //        int colCount = xlRange.Columns.Count;
        //        int colRule = 0;

        //        if (colCount > 0 && rowCount > 0)
        //        {
        //            //Creating datatable to read the containt of the Sheet in File.
        //            DataTable dt = new DataTable();
        //            for (Cnum = 1; Cnum <= colCount; Cnum++)
        //            {
        //                string headear = (xlRange.Cells[4, Cnum] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
        //                dt.Columns.Add(headear);

        //                if (headear == "Visual Basic")
        //                    colRule = Cnum;

        //            }
        //            dt.Columns.Add("Col No");
        //            dt.Columns.Add("Row No");

        //            for (Rnum = 5; Rnum <= rowCount; Rnum++)
        //            {
        //                DataRow dr = dt.NewRow();
        //                //Reading Each Column value From sheet to datatable Colunms                  
        //                for (Cnum = 1; Cnum <= colCount; Cnum++)
        //                {
        //                    if ((xlRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value2 == null)
        //                        dr[Cnum - 1] = string.Empty;
        //                    else
        //                        dr[Cnum - 1] = (xlRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value2.ToString();
        //                }
        //                dr[colCount] = colRule;
        //                dr[colCount + 1] = Rnum;
        //                dt.Rows.Add(dr); // adding Row into DataTable
        //                dt.AcceptChanges();
        //            }

        //            xlWorkBook.Close();
        //            xlexcel.Quit();

        //            gvrule.DataSource = dt;
        //            gvrule.DataBind();
        //        }

        //    }
        //    else
        //    {
        //        bindemptygvrule();
        //    }


        //}

        protected void gvrule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LiteralControl c = e.Row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox chk = c.FindControl("chkid") as HtmlInputCheckBox;
                if (ViewState["FieldNo"] == null)
                    ViewState["FieldNo"] = 0;
                if (ViewState["RuleFileId"] == null)
                    ViewState["RuleFileId"] = 0;

                if (!string.IsNullOrEmpty(ViewState["FieldNo"].ToString()) && !string.IsNullOrEmpty(ViewState["RuleFileId"].ToString()))
                {
                    if ((DataBinder.Eval(e.Row.DataItem, "Field No").ToString() == ViewState["FieldNo"].ToString() && ViewState["RuleFileId"].ToString() == ddlvalidationfile.SelectedValue))
                        chk.Checked = true;
                    else
                        chk.Checked = false;
                }
                else //automate with field name
                {
                    if (DataBinder.Eval(e.Row.DataItem, "Field Name").ToString() == ViewState["obj_attribute"].ToString())
                        chk.Checked = true;
                    else
                        chk.Checked = false;
                }
            }
        }

        protected void gvrule_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["RPageIndex"] = e.NewPageIndex;
            this.gvrule.PageIndex = e.NewPageIndex;
            bindgvrule(ddlvalidationfile.SelectedValue.ConvertTo<int>());
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobj.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj + " - " + pd.Source_Sys }).OrderBy(i => i.objDESC).ToList();
                var validation = db.Ref_Rule.ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }

                if (validation.Count > 0)
                {
                    foreach (var item in validation)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.File_Name;
                        bindddlobject(ddlvalidationfile, datatext, datavalue);
                    }
                    ddlvalidationfile.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindlovatt(int obj)
        {
            ddllovatt.Items.Clear();

            using (MDMEntities db = new MDMEntities())
            {
                var att = (from c in db.Ref_Obj_Att
                           where c.obj_id == obj
                           select new { c.ID, objDESC = c.obj_attribute }).OrderBy(i => i.objDESC).ToList();

                if (att.Count > 0)
                {
                    foreach (var item in att)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddllovatt, datatext, datavalue);
                    }
                    ddllovatt.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void ddllovobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            var obj = ddllovobj.SelectedValue.ConvertTo<int>();
            bindlovatt(obj);
        }
    }
}