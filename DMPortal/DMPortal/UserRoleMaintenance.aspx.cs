﻿using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Data.Entity.Validation;

namespace DMPortal
{
    public partial class UserRoleMaintenance : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                btndelete.Visible = false;
                bindgv();
            }
        }

        private void bindgv()
        {
            using (MDMEntities context = new MDMEntities())
            {
                var roles = (from c in context.TBLROLES
                             select new { c.ID, c.rolename, c.MODIFY_DATETIME }).ToList();

                if (roles.Count > 0)
                {
                    btndelete.Visible = true;
                    gv.DataSource = roles;
                    gv.DataBind();
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {

            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            if (txtr1.Text != null)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        var role = context.TBLROLES.Where(i => i.rolename == txtr1.Text).SingleOrDefault();
                        if (role != null)
                        {
                            UserAlert.ShowAlert("Role exist.", CommonEnum.AlertType.Exclamation);
                            return;

                        }
                        else
                        {

                            TBLROLE srole = new TBLROLE();
                            srole.rolename = txtr1.Text;
                            srole.CREATED_BY = this.logonId;
                            srole.CREATE_DATETIME = DateTime.Now;
                            srole.MODIFIER_ID = this.logonId;
                            srole.MODIFY_DATETIME = DateTime.Now;
                            context.TBLROLES.Add(srole);
                            context.SaveChanges();
                            pkid = (int)srole.ID;

                            var Param = new SqlParameter
                            {
                                ParameterName = "Roleid",
                                Value = pkid,
                            };

                            //var oinsert = context.Database.SqlQuery<ROLE_ACCESS>("exec AddRoleAccess @Roleid ", Param).ToList<ROLE_ACCESS>();
                            context.Database.ExecuteSqlCommand("exec AddRoleAccess @Roleid ", Param);

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "TBLROLE", FIELD_NAME = "rolename", OLD_VALUE = "", NEW_VALUE = txtr1.Text, DESC = "New role name added. Role access added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var role = context.TBLROLES.Where(i => i.ID == id).SingleOrDefault();

                        if (role != null)
                        {
                            if (role.rolename != txtr1.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "TBLROLE", FIELD_NAME = "METHOD", OLD_VALUE = role.rolename, NEW_VALUE = txtr1.Text, DESC = "User role name has been modified", CREATOR_ID = this.logonId });

                            role.rolename = txtr1.Text;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;
                        }
                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }

            }
            bindgv();
            txtr1.Text = "";
            ViewState["PageMode"] = "A";
        }


        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {

                        TBLROLE _role = context.TBLROLES.SingleOrDefault(i => i.ID == id);

                        if (_role != null)
                        {
                            context.TBLROLES.Remove(_role);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "TBLROLE", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Role " + _role.rolename + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv();
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var role = context.TBLROLES.Where(i => i.ID == id).SingleOrDefault();

                    if (role != null)
                    {
                        txtr1.Text = role.rolename;
                    }
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtr1.Text = "";
        }
    }
}