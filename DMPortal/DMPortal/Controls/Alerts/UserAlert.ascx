﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAlert.ascx.cs" Inherits="DMPortal.Controls.Alerts.UserAlert" %>
<link href="../Content/Site.css" rel="stylesheet" type="text/css" />
<div class="<%=strStyle%>" <%=strHideEvent%> >
    <%=strMessage%>
</div>

