﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Object;

namespace DMPortal.Controls.Alerts
{
    public partial class UserAlert : System.Web.UI.UserControl
    {
        protected string strMessage;
        protected CommonEnum.AlertType alertType;
        protected string strStyle;
        protected string strHideEvent;
        protected string strSupportEmailLink;

        public UserAlert()
        {
            this.HideAlert();
        }

        #region Public Properties

        public string Message
        {
            get
            {
                return strMessage;
            }
            set
            {
                strMessage = value;
            }
        }

        public CommonEnum.AlertType AlertType
        {
            get
            {
                return alertType;
            }
            set
            {
                alertType = value;
            }
        }

        #endregion

        #region Public Methods

        public void HideAlert()
        {
            this.Visible = false;
        }

        private void ShowUserAlert(string message, CommonEnum.AlertType alertType, CommonEnum.AlertHideEvent hideEvent)
        {
            this.alertType = alertType;
            this.strMessage = message;
            strStyle = GetStyle(alertType);

            strHideEvent = GetEvent(hideEvent);

            this.Visible = true;
        }

        public void ShowAlert(string message, CommonEnum.AlertType alertType)
        {
            ShowUserAlert(message, alertType, CommonEnum.AlertHideEvent.None);
        }

        public void ShowAlert(string message, CommonEnum.AlertType alertType, CommonEnum.AlertHideEvent hideEvent)
        {
            ShowUserAlert(message, alertType, hideEvent);
        }

        //public void ShowAlert(string message, CommonEnum.AlertType alertType, string errorCode, bool includeContactLink)
        //{
        //    string tempMessage = message;
        //    if (includeContactLink)
        //    {
        //        PageFramework objPage = Page as PageFramework;

        //        strMessage += " <a class='alert_link' href='" + objPage.GetSupportMailString(errorCode) + "'>";
        //        //strMessage += HttpContext.Current.Application["SUPPORT_EMAIL"].ToString() + "</a>";
        //        strMessage += "mail to Support Team" + "</a>";
        //    }
        //    //this.Visible = true;

        //    ShowUserAlert(strMessage, alertType, CommonEnum.AlertHideEvent.None);
        //}

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(strMessage))
            {
                this.Visible = false;
                return;
            }

            strStyle = GetStyle(alertType);
        }

        #endregion

        #region Private Methods

        private string GetStyle(CommonEnum.AlertType alertType)
        {
            switch (alertType)
            {
                case CommonEnum.AlertType.Information:
                    return "alert_info";

                case CommonEnum.AlertType.Exclamation:
                    return "alert_excl";

                case CommonEnum.AlertType.Critical:
                    return "alert_crit";

                default:
                    return "alert_excl";
            }
        }

        private string GetEvent(CommonEnum.AlertHideEvent hideEvent)
        {
            switch (hideEvent)
            {
                case CommonEnum.AlertHideEvent.Click:
                    return "onclick=\"javascript:this.style.display='none'\"";

                case CommonEnum.AlertHideEvent.DoubleClick:
                    return "ondblclick=\"javascript:this.style.display='none'\"";

                case CommonEnum.AlertHideEvent.None:
                    return string.Empty;

                default:
                    return string.Empty;
            }
        }

        #endregion
    }
}