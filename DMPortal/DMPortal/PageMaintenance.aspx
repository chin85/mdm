﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PageMaintenance.aspx.cs" Inherits="DMPortal.PageMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>This website is currently offline for maintenance</h1>
            <p>
                Please contact your administrator.
            </p>
        </div>
    </form>
</body>
</html>
