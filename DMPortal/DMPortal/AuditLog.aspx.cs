﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;

namespace DMPortal
{
    public partial class AuditLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                printer.Visible = false;
                ViewState["PageIndex"] = 0;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            bindreport();
        }

        private void bindreport()
        {
            string fDate = txttxnDate.Value;
            string tDate = txttxntoDate.Value;

            List<AUDIT_TRAIL> oaudit = new List<AUDIT_TRAIL>();

            using (MDMEntities context = new MDMEntities())
            {

                if (!string.IsNullOrEmpty(fDate) && !string.IsNullOrEmpty(tDate))
                {
                    System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");
                    DateTime sdt = Convert.ToDateTime(fDate, enGB);//Convert.ToDateTime(tDate, enGB);
                    DateTime tdt = DateTime.Parse(string.Format("{0} 23:59:59", tDate), enGB);

                    oaudit = context.AUDIT_TRAIL.Where(i => i.CREATE_DATETIME >= sdt && i.CREATE_DATETIME <= tdt).ToList();
                }

                gv.DataSource = oaudit;
                gv.DataBind();

                if (oaudit.Count > 0)
                    printer.Visible = true;


                //try
                //{

                //}
                //catch (DbEntityValidationException ex)
                //{
                //    foreach (var eve in ex.EntityValidationErrors)
                //    {
                //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //        foreach (var ve in eve.ValidationErrors)
                //        {
                //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //                ve.PropertyName, ve.ErrorMessage);
                //        }
                //    }
                //    throw;
                //}

            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindreport();
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
            }
        }
    }
}