﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefObjAtt.aspx.cs" Inherits="DMPortal.RefObjAtt" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/comm.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function SingleSelect(regex, current) {
            re = new RegExp(regex);
            for (i = 0; i < document.forms[0].elements.length; i++) {
                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        elm.checked = false;
                    }
                }
            }
            current.checked = true;
        }

        //$(':checkbox').click(function (e) {
        //    if ($(this).is(':checked')) {
        //        $(':checked').prop('checked', false);
        //        $(this).prop('checked', true);

        //    }
        //    else {
        //        e.preventDefault();
        //    }
        //});

        function checkselector(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to delete?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert runat="server" ID="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Reference Object Attribute</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-md-4 control-label">Project: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. project."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-4 control-label">Object: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlobj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlobj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlobj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. object."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" OnPageIndexChanging="gv_PageIndexChanging" EmptyDataText="No records." OnRowDataBound="gv_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("ID")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Reference Object" DataField="obj"></asp:BoundField>
                            <asp:BoundField HeaderText="Object Attribute" DataField="obj_attribute"></asp:BoundField>
                            <asp:BoundField HeaderText="Desc." DataField="att_desc"></asp:BoundField>
                            <asp:BoundField HeaderText="Validation Rule" DataField="field_no"></asp:BoundField>
                            <asp:BoundField HeaderText="Lov Attribute" DataField="lov_att"></asp:BoundField>
                            <asp:BoundField HeaderText="Last Modified" DataField="MODIFY_DATETIME" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ID")%>'
                                        Font-Underline="false" OnClick="gv_edit" CausesValidation="false">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btndelete" />
                    <asp:AsyncPostBackTrigger ControlID="btnsave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger" CausesValidation="false" OnClick="btndelete_Click" OnClientClick="return checkselector(this)" />
        </div>
        <br />
        <br />
        <div id="divdetail" runat="server" class="col-md-12" style="text-align: right">
            <div style="margin: auto">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <legend>Reference Object Attribute</legend>

                        <div id="ldiv" runat="server">
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">* Attribute Name :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtatt" runat="server" CssClass="form-control input-md" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtatt" ForeColor="Red" ErrorMessage="can not be empty."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div id="Div1" runat="server">
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">*Attribute Desc :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control input-md" Enabled="true"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDesc" ForeColor="Red" ErrorMessage="can not be empty."></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">Data Mapped :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:RadioButtonList ID="rbl" runat="server" ValidationGroup="rbl" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Yes" Value="Y" />
                                    <asp:ListItem Text="No" Value="N" Selected="True" />
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">LOV Object :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddllovobj" runat="server" CssClass="form-control input-md" AutoPostBack="true" OnSelectedIndexChanged="ddllovobj_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">LOV Attribute :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddllovatt" runat="server" CssClass="form-control input-md">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">Data Validation File:</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddlvalidationfile" runat="server" CssClass="form-control input-md" AutoPostBack="true" OnSelectedIndexChanged="ddlvalidationfile_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:GridView ID="gvrule" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="Field No" EmptyDataText="No Data." OnRowDataBound="gvrule_RowDataBound" OnPageIndexChanging="gvrule_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <input id="chkid" runat="server" type="checkbox" value='<%# Eval("Field No")%>' onclick="SingleSelect('chk', this);" />
                                            </ItemTemplate>
                                            <ItemStyle Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Field No" DataField="Field No" />
                                        <asp:BoundField HeaderText="Field Name" DataField="Field Name" />
                                        <asp:BoundField HeaderText="Rules Desc" DataField="Rules" />
                                        <asp:BoundField HeaderText="Col No" DataField="Col No"></asp:BoundField>
                                        <asp:BoundField HeaderText="Row No" DataField="Row No"></asp:BoundField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                        <asp:AsyncPostBackTrigger ControlID="btncancel" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<br />
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlvalidationfile"  />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnsave_Click" />
            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
        </div>
    </div>
</asp:Content>
