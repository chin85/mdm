﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckerDetails.aspx.cs" Inherits="DMPortal.CheckerDetails" %>
<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gvdetail.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_gvdetail INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to proceed?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:useralert runat="server" id="UserAlert1" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Details</h4>
            </div>
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvdetail" runat="server" CssClass="table table-hover table-bordered" EmptyDataText="No Records."
                            DataKeyNames="Row_id"
                            AllowPaging="true" OnRowDataBound="gvdetail_RowDataBound" OnPageIndexChanging="gvdetail_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <input id="chkid" runat="server" type="checkbox" value='<%# Eval("row_id")%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="3%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="3%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnApproved" />
                    <asp:AsyncPostBackTrigger ControlID="btnRejected" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div>
            <table runat="server">
                <tr>
                    <td class="col-5 control-label" style="width: 120px">Reject Reason : </td>
                    <td class="auto-style1" style="width: 300px; text-align: left">
                        <asp:TextBox ID="txtreason" runat="server" class="form-control input-md"></asp:TextBox>
                    </td>
                    <td class="auto-style1" style="width: 100px;">
                        <asp:Button ID="btnRejected" runat="server" Text="Rejected" CssClass="btn btn-danger" OnClientClick="return checkselector(this)" OnClick="btnRejected_Click" /></td>
                    <td class="auto-style1" style="width: 100px;">
                        <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn btn-success" OnClientClick="return checkselector(this)" OnClick="btnApproved_Click" />
                    </td>
                    <td class="auto-style1" style="width: 100px;">
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-inverse" OnClick="btnBack_Click"  />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
