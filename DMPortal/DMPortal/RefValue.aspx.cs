﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using DMPortal.Object;
using System.Web.UI.HtmlControls;
using System.Data.Entity.Validation;
//using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;

namespace DMPortal
{
    public partial class RefValue : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //logonId = "test";
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                bindDLL();
                //ddltype.Enabled = true;
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void ddlobj_SelectedIndexChanged(object sender, EventArgs e)
        {

            txtmin.Text = string.Empty;
            txtmax.Text = string.Empty;
            txtvalue.Text = string.Empty;
            //txtdesc.Text = string.Empty;

            var _obj = ddlobj.SelectedValue.ConvertTo<int>();
            if (_obj != 0)
            {
                bindgv(_obj);
            }
            else
            {
                gv.DataSource = null;
                gv.DataBind();

            }
        }

        private void bindgv(int objid)
        {
            using (MDMEntities db = new MDMEntities())
            {
                var objvalue = (from pd in db.Ref_Obj_Value
                                join od in db.Ref_Obj on pd.obj_id equals od.ID
                                where pd.obj_id == objid
                                orderby pd.obj_value
                                select new
                                {
                                    pd.ID,
                                    pd.obj_value,
                                    //pd.obj_value_desc,
                                    pd.MODIFY_DATETIME,
                                    pd.obj_type,
                                    pd.min,
                                    pd.max,
                                    od.obj,
                                }).ToList();

                var objtype = objvalue.FirstOrDefault();
                if (objtype != null)
                {
                    ddltype.SelectedValue = objtype.obj_type;
                    ddltype.Enabled = false;

                    if (objtype.obj_type == "LIST")
                    {
                        rdiv.Visible = false;
                        ldiv.Visible = true;
                    }
                    else
                    {
                        rdiv.Visible = true;
                        ldiv.Visible = false;
                    }
                }
                gv.DataSource = objvalue;
                gv.DataBind();
            }
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var oref = context.Ref_Obj_Value.Where(i => i.ID == id).SingleOrDefault();

                    if (oref != null && oref.obj_type == "LIST")
                    {
                        txtvalue.Text = oref.obj_value;
                        //txtdesc.Text = oref.obj_value_desc;
                    }
                    else
                    {
                        txtmin.Text = oref.min.ToString();
                        txtmax.Text = oref.max.ToString();
                        //txtdesc.Text = oref.obj_value_desc;
                    }
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            //try
            //{
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            int rid = ViewState["ID"].ConvertTo<int>();
            int oid = ddlobj.SelectedValue.ConvertTo<int>();
            string strtype;

            if (oid != 0)
            {
                strtype = ddltype.SelectedValue;

                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        Ref_Obj_Value oref = new Ref_Obj_Value();

                        if (strtype == "LIST" && !string.IsNullOrEmpty(txtvalue.Text))

                            oref = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.obj_value == txtvalue.Text).SingleOrDefault();
                        else
                        {
                            var min = txtmin.Text.ConvertTo<decimal>();
                            var max = txtmax.Text.ConvertTo<decimal>();

                            var Param = new SqlParameter
                            {
                                ParameterName = "Criteria",
                                Value = "AND obj_id =" + oid + " AND ((" + min + " BETWEEN MIN AND MAX) OR (" + max + " BETWEEN MIN AND MAX))",
                            };

                            oref = context.Database.SqlQuery<Ref_Obj_Value>("exec GetObjVal @Criteria ", Param).FirstOrDefault<Ref_Obj_Value>();
                        }


                        if (oref != null && strtype == "LIST")
                        {
                            UserAlert.ShowAlert("Value exist. Please select/ key in a new value.", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                        else if (oref != null && strtype == "RANGE")
                        {
                            UserAlert.ShowAlert("Min / Max fall in range!. Please key in a new value.", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                        else
                        {
                            Ref_Obj_Value srefobj = new Ref_Obj_Value();
                            srefobj.obj_id = oid;
                            srefobj.obj_value = txtvalue.Text;
                            //srefobj.obj_value_desc = txtdesc.Text;
                            srefobj.CREATED_BY = this.logonId;
                            srefobj.CREATE_DATETIME = DateTime.Now;
                            srefobj.MODIFIER_ID = this.logonId;
                            srefobj.MODIFY_DATETIME = DateTime.Now;
                            srefobj.min = txtmin.Text.ConvertTo<decimal>();
                            srefobj.max = txtmax.Text.ConvertTo<decimal>();
                            srefobj.obj_type = ddltype.SelectedValue;
                            context.Ref_Obj_Value.Add(srefobj);
                            context.SaveChanges();
                            pkid = (int)srefobj.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "obj_value", OLD_VALUE = "", NEW_VALUE = txtvalue.Text, DESC = "New obj value added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var role = context.Ref_Obj_Value.Where(i => i.ID == rid).SingleOrDefault();

                        if (role != null && role.obj_type == "LIST")
                        {
                            if (role.obj_value != txtvalue.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "obj_value", OLD_VALUE = role.obj_value, NEW_VALUE = txtvalue.Text, DESC = "Ref. obj value has been modified", CREATOR_ID = this.logonId });
                            //if (role.obj_value_desc != txtdesc.Text)
                            //    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "obj_value_desc", OLD_VALUE = role.obj_value_desc, NEW_VALUE = txtdesc.Text, DESC = "Ref. obj value's desc. has been modified", CREATOR_ID = this.logonId });

                            role.obj_value = txtvalue.Text;
                            //role.obj_value_desc = txtdesc.Text;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;
                        }
                        else if (role != null && role.obj_type == "RANGE")
                        {
                            var min = txtmin.Text.ConvertTo<decimal>();
                            var max = txtmax.Text.ConvertTo<decimal>();

                            var Param = new SqlParameter
                            {
                                ParameterName = "Criteria",
                                Value = "AND obj_id =" + oid + " AND ID <> " + rid + " AND ((" + min + " BETWEEN MIN AND MAX) OR (" + max + " BETWEEN MIN AND MAX))",
                            };

                            var oref = context.Database.SqlQuery<Ref_Obj_Value>("exec GetObjVal @Criteria ", Param).FirstOrDefault<Ref_Obj_Value>();

                            if (oref != null)
                            {
                                UserAlert.ShowAlert("Min / Max fall in range!. Please key in a new value.", CommonEnum.AlertType.Exclamation);
                                return;
                            }

                            role.min = min;
                            role.max = max;
                            //role.obj_value_desc = txtdesc.Text;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;
                        }
                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }

            }
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}

            bindgv(ddlobj.SelectedValue.ConvertTo<int>());
            txtvalue.Text = string.Empty;
            //txtdesc.Text = string.Empty;
            txtmin.Text = string.Empty;
            txtmax.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtmin.Text = string.Empty;
            txtmax.Text = string.Empty;
            txtvalue.Text = string.Empty;
            //txtdesc.Text = string.Empty;
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv(ddlobj.SelectedValue.ConvertTo<int>());
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {

                        Ref_Obj_Value _rolev = context.Ref_Obj_Value.SingleOrDefault(i => i.ID == id);

                        if (_rolev != null)
                        {
                            context.Ref_Obj_Value.Remove(_rolev);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_Value " + _rolev.obj_value + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv(ddlobj.SelectedValue.ConvertTo<int>());
        }

        protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {

            txtmin.Text = string.Empty;
            txtmax.Text = string.Empty;
            txtvalue.Text = string.Empty;
            //txtdesc.Text = string.Empty;

            var obj_type = ddltype.SelectedValue;
            if (!string.IsNullOrEmpty(obj_type))
            {
                if (obj_type == "LIST")
                {
                    rdiv.Visible = false;
                    ldiv.Visible = true;
                }
                else
                {
                    rdiv.Visible = true;
                    ldiv.Visible = false;
                }
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                var lblmin = e.Row.FindControl("lblMin") as System.Web.UI.WebControls.Label;
                var lblmax = e.Row.FindControl("lblMax") as System.Web.UI.WebControls.Label;
                var lblval = e.Row.FindControl("lblval") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
                var obj_type = DataBinder.Eval(e.Row.DataItem, "obj_type").ToString();
                var obj_value = DataBinder.Eval(e.Row.DataItem, "obj_value").ToString();
                var min = DataBinder.Eval(e.Row.DataItem, "min").ToString();
                var max = DataBinder.Eval(e.Row.DataItem, "max").ToString();

                if (obj_type == "LIST")
                {
                    lblmin.Text = string.Empty;
                    lblmax.Text = string.Empty;
                    lblval.Text = obj_value;
                }
                else
                {
                    lblmin.Text = min;
                    lblmax.Text = max;
                    lblval.Text = string.Empty;
                }

            }
        }
    }
}