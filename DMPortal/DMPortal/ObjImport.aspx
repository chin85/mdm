﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ObjImport.aspx.cs" Inherits="DMPortal.ObjImport" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function SelectAllCheckboxes(chk) {
            $('#<%=extcol.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_extcol INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you publish the table?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Import Table</h4>
            </div>
        </div>
        <div>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <table runat="server">
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Database :</td>
                            <td colspan="3" class="auto-style1" style="width: 200px; text-align: left">
                                <asp:DropDownList ID="ExtDb" runat="server" CssClass="form-control" OnSelectedIndexChanged="ExtDb_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Table :</td>
                            <td colspan="3" class="auto-style1" style="width: 200px; text-align: left">
                                <asp:DropDownList ID="ExtTbl" runat="server" CssClass="form-control" OnSelectedIndexChanged="ExtTbl_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Import Option :</td>
                            <td colspan="3" class="auto-style1" style="width: 200px; text-align: left">
                                <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddltype_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                    <asp:ListItem Value="S">Schema and Data</asp:ListItem>
                                    <asp:ListItem Value="D">Data Only</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Import Data Option :</td>
                            <td colspan="3" class="auto-style1" style="width: 200px; text-align: left">
                                <asp:DropDownList ID="ddldataopt" runat="server" CssClass="form-control" Enabled="false">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                    <asp:ListItem Value="O">Overwrite / Replace</asp:ListItem>
                                    <asp:ListItem Value="A">Append</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <uc1:UserAlert runat="server" ID="UserAlert" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <asp:Label ID="Label1" runat="server" Text="* Remark : Kindly please select desire columns to import or else all columns imported as default." Font-Italic="true"></asp:Label>
                    <asp:GridView ID="extcol" runat="server" AutoGenerateColumns="true" CssClass="table table-hover table-bordered" EmptyDataText="No Records.">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("Column")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <table>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Project : </td>
                            <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" Width="290px" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. Project."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Source Sys. : </td>
                            <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                <asp:DropDownList ID="ddlsource" runat="server" CssClass="form-control" Width="290px" OnSelectedIndexChanged="ddlsource_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlsource" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. Source."></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-4 control-label" style="width: 150px">Obj Name : </td>
                            <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                <asp:DropDownList ID="ddlobj" runat="server" CssClass="form-control" Width="290px" OnSelectedIndexChanged="ddlobj_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>&nbsp;<asp:TextBox ID="txtobj" runat="server" CssClass="form-control input-md"></asp:TextBox>
                            </td>
                            <td colspan="3" class="auto-style1" style="width: 100px; text-align: center">
                                <asp:Button ID="btnImport" runat="server" Text="Import" CssClass="btn btn-warning" OnClick="btnImport_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
