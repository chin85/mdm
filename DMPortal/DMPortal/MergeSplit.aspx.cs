﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class MergeSplit : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageIndex"] = 0;
                ViewState["PageIndexDtl"] = 0;
                bindDLL();
                btnsave.Visible = false;
                btncancel.Visible = false;
                //btnMerge.Visible = false;
                //BindGrid();
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {
                //var role = (from c in db.Ref_Obj
                //            select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                //if (role.Count > 0)
                //{
                //    foreach (var item in role)
                //    {
                //        var datavalue = item.ID.ToString();
                //        var datatext = item.objDESC;
                //        bindddlobject(ddlobj, datatext, datavalue);
                //    }
                //    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //}


                var role = (from c in db.Ref_Proj
                            select new { c.ID, objDESC = c.Project }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }
        private void BindGrid()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetObjAttValMerge", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = ddlobj.SelectedValue.ConvertTo<int>();
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "Obj");
                DataTable dt = ds.Tables["Obj"];
                Session["TaskTable"] = dt;

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables["Obj"].Rows.Count > 0)
                    {
                        gv.DataSource = ds.Tables["Obj"];
                        gv.DataBind();

                        if (string.IsNullOrEmpty(ds.Tables["Obj"].Rows[0]["row_id"].ToString()))
                        {
                            gv.Rows[0].Visible = false;
                        }
                    }
                }
                con.Close();
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void btnMerge_Click(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "M";
            List<int> ids = new List<int>();
            GridViewRow fRow = gv.FooterRow;

            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[1].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;

                int oid = ddlobj.SelectedValue.ConvertTo<int>();
                int mid = ctl.Value.ConvertTo<int>(); //rowid

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        ids.Add(mid);
                        int fir = ids[0];

                        var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id == fir).ToList();
                        if (upd.Count > 0)
                        {
                            for (int i = 0; i < upd.Count; i++)
                            {
                                ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text = upd[i].obj_value;
                            }

                            btnsave.Visible = true;
                            btncancel.Visible = true;
                            btnMerge.Visible = false;

                            UserAlert1.ShowAlert("Please specify a new value or use the default .", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                    }
                }
            }
        }

        protected void gv_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int cellCnt = e.Row.Cells.Count;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                for (int i = 3; i < cellCnt; i++)
                {
                    TextBox txtFooter = new TextBox();
                    txtFooter.ID = "txtFooter" + i.ToString();
                    txtFooter.CssClass = "form-control input-md";
                    e.Row.Cells[i].Controls.Add(txtFooter);
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;

            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            List<MERGE_SPLIT_TRAIL> oMerge = new List<MERGE_SPLIT_TRAIL>();
            List<int> ids = new List<int>();
            List<int> idm = new List<int>();
            GridViewRow fRow = gv.FooterRow;

            if (ViewState["PageMode"].ToString() == "M")
            {
                using (MDMEntities context = new MDMEntities())
                {
                    foreach (GridViewRow row in gv.Rows)
                    {
                        LiteralControl c = row.Cells[1].Controls[0] as LiteralControl;
                        HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;

                        int oid = ddlobj.SelectedValue.ConvertTo<int>();
                        int mid = ctl.Value.ConvertTo<int>(); // rowid

                        if (ctl != null && ctl.Checked)
                        {
                            ids.Add(mid);
                            int fir = ids[0];

                            var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id == fir).ToList();
                            if (upd.Count > 0)
                            {
                                for (int i = 0; i < upd.Count; i++)
                                {
                                    string newVal = ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text;
                                    int updid = upd[i].ID.ConvertTo<int>();
                                    int updattid = upd[i].obj_Att_id.ConvertTo<int>();
                                    idm.Add(updid);

                                    var attribute = context.Ref_Obj_Att.Where(k => k.obj_id == oid && k.ID == updattid).SingleOrDefault();

                                    oMerge.Add(new MERGE_SPLIT_TRAIL() { OBJECT_ID = updid, MERGE_ID = updid, TBL_NAME = "Ref_Obj_Value", OLD_ITEM = attribute.obj_attribute, OLD_VALUE = upd[i].obj_value, OLD_VALUE_DESC = null, NEW_ITEM = null, NEW_VALUE = newVal, NEW_VALUE_DESC = null, CREATOR_ID = this.logonId, CREATE_DATETIME = DateTime.Now, STS = "M", DESC = "Object value has been merged" });

                                    Ref_Obj_Value objval = context.Ref_Obj_Value.SingleOrDefault(j => j.ID == updid);
                                    if (objval != null)
                                    {
                                        if (objval.obj_value != newVal)
                                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = updid, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "obj_value", OLD_VALUE = objval.obj_value, NEW_VALUE = newVal, DESC = "Ref. obj value has been modified", CREATOR_ID = this.logonId });
                                        objval.obj_value = newVal;
                                        objval.MODIFIER_ID = this.logonId;
                                        objval.MODIFY_DATETIME = DateTime.Now;
                                        objval.min = 0;
                                        objval.max = 0;
                                        context.SaveChanges();
                                    }
                                }
                            }

                            var del = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id != fir).ToList();
                            if (del.Count > 0)
                            {
                                for (int i = 0; i < del.Count; i++)
                                {
                                    string newVal = ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text;
                                    int delid = del[i].ID.ConvertTo<int>();
                                    int delattid = del[i].obj_Att_id.ConvertTo<int>();
                                    int idms = idm[i];

                                    var attributesel = context.Ref_Obj_Att.Where(k => k.obj_id == oid && k.ID == delattid).SingleOrDefault();

                                    Ref_Obj_Value _robjatt = context.Ref_Obj_Value.SingleOrDefault(k => k.ID == delid);
                                    if (_robjatt != null)
                                    {
                                        oMerge.Add(new MERGE_SPLIT_TRAIL() { OBJECT_ID = delid, MERGE_ID = idms, TBL_NAME = "Ref_Obj_Value", OLD_ITEM = attributesel.obj_attribute, OLD_VALUE = _robjatt.obj_value, OLD_VALUE_DESC = null, NEW_ITEM = null, NEW_VALUE = newVal, NEW_VALUE_DESC = null, CREATOR_ID = this.logonId, CREATE_DATETIME = DateTime.Now, STS = "M", DESC = "Object value has been merged" });
                                        context.Ref_Obj_Value.Remove(_robjatt);
                                        context.SaveChanges();
                                        oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = delid, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_value " + _robjatt.obj_id + ": " + _robjatt.obj_value + " has been deleted due to merge.", CREATOR_ID = this.logonId });
                                    }
                                }
                            }
                        }
                    }

                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                    oAudittrl.AddMergeTrail(oMerge);
                }

                BindGrid();
                btnsave.Visible = false;
                btncancel.Visible = false;
                btnMerge.Visible = true;
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            BindGrid();
            btnsave.Visible = false;
            btncancel.Visible = false;
            btnMerge.Visible = true;
        }


        protected void gv_unmerge(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";

            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;

                int oid = ddlobj.SelectedValue.ConvertTo<int>();
                int id = linkButton.CommandArgument.ConvertTo<int>(); // rowid
                using (MDMEntities context = new MDMEntities())
                {
                    var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == id).ToList();
                    var rowno = context.Ref_Obj_Value.Where(j => j.obj_id == oid).Max(k => k.row_id);
                    if (upd.Count > 0)
                    {
                        foreach (var item in upd)
                        {
                            var chk = context.MERGE_SPLIT_TRAIL.Where(i => i.MERGE_ID == item.ID && i.STS != "U").ToList();

                            if (chk.Count == 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Value has not been merged');", true);
                                return;
                            }
                            else
                            {
                                var merged = context.MERGE_SPLIT_TRAIL.Where(i => i.MERGE_ID == item.ID && i.STS != "U");
                                int mid = merged.FirstOrDefault().MERGE_ID;
                                var m = merged.SingleOrDefault(i => i.OBJECT_ID == mid);
                                if (m != null)
                                {
                                    m.STS = "U";
                                    m.DESC = "Object value has been unmerged";
                                }

                                string old_val = m.OLD_VALUE;
                                var attid = upd.SingleOrDefault(j => j.ID == mid).obj_Att_id;

                                Ref_Obj_Value objval = context.Ref_Obj_Value.SingleOrDefault(i => i.ID == mid);
                                objval.obj_value = old_val;
                                objval.MODIFIER_ID = this.logonId;
                                objval.MODIFY_DATETIME = DateTime.Now;
                                context.SaveChanges();

                                var del = context.MERGE_SPLIT_TRAIL.Where(i => i.MERGE_ID == mid && i.OBJECT_ID != mid && i.STS != "U");

                                foreach (var d in del)
                                {
                                    int did = d.OBJECT_ID;
                                    var u = del.SingleOrDefault(i => i.OBJECT_ID == did);
                                    if (u != null)
                                    {
                                        u.STS = "U";
                                        u.DESC = "Object value has been unmerged";
                                    }

                                    Ref_Obj_Value refd = new Ref_Obj_Value();
                                    refd.obj_id = oid;
                                    refd.row_id = rowno + 1;
                                    refd.obj_Att_id = attid;
                                    refd.obj_value = d.OLD_VALUE;
                                    refd.CREATED_BY = this.logonId;
                                    refd.CREATE_DATETIME = DateTime.Now;
                                    refd.MODIFIER_ID = this.logonId;
                                    refd.MODIFY_DATETIME = DateTime.Now;
                                    refd.min = 0;
                                    refd.max = 0;
                                    refd.obj_type = "LIST";
                                    refd.status = "A";
                                    context.Ref_Obj_Value.Add(refd);
                                }
                                context.SaveChanges();
                            }
                        }

                        BindGrid();
                        BindMergeDtl("0");
                        btnsave.Visible = false;
                        btncancel.Visible = false;
                        btnMerge.Visible = true;
                    }
                }
            }
        }

        protected void gv_detail(object sender, EventArgs e)
        {
            ViewState["MergeId"] = null;
            List<string> mergeidlst = new List<string>();
            string mergeid = string.Empty;
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;

                int oid = ddlobj.SelectedValue.ConvertTo<int>();
                int id = linkButton.CommandArgument.ConvertTo<int>(); // rowid
                using (MDMEntities context = new MDMEntities())
                {
                    var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == id).ToList();

                    foreach (var item in upd)
                    {
                        mergeidlst.Add(item.ID.ToString());
                    }

                    mergeid = string.Join(",", mergeidlst.ToArray());
                }
            }

            ViewState["MergeId"] = mergeid;
            BindMergeDtl(mergeid);
        }

        private void BindMergeDtl(string mergeid)
        {
            if (mergeid == "0")
            {
                gvDtl.DataSource = null;
                gvDtl.DataBind();
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("GetMergeDetail", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = ddlobj.SelectedValue.ConvertTo<int>();
                    cmd.Parameters.Add("@merge_id", SqlDbType.VarChar, 500).Value = mergeid;
                    SqlDataAdapter sql = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sql.Fill(ds, "MergeDtl");
                    DataTable dt = ds.Tables["MergeDtl"];
                    Session["TaskTable"] = dt;

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables["MergeDtl"].Rows.Count > 0)
                        {
                            gvDtl.DataSource = ds.Tables["MergeDtl"];
                            gvDtl.DataBind();
                        }
                    }
                    con.Close();
                }
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int oid = ddlobj.SelectedValue.ConvertTo<int>();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkMerge = e.Row.FindControl("lnkMerge") as System.Web.UI.WebControls.LinkButton;
                LinkButton lnkDetail = e.Row.FindControl("lnkDetail") as System.Web.UI.WebControls.LinkButton;
                var rowid = gv.DataKeys[e.Row.RowIndex].Values[0].ConvertTo<int>();

                using (MDMEntities context = new MDMEntities())
                {
                    var objv = context.Ref_Obj_Value.Where(j => j.obj_id == oid && j.row_id == rowid).ToList();
                    if (objv.Count > 0)
                    {
                        var robjv = objv.FirstOrDefault();
                        if (robjv != null)
                        {
                            var chk = context.MERGE_SPLIT_TRAIL.Where(i => i.MERGE_ID == robjv.ID && i.STS == "M").ToList();

                            if (chk.Count == 0)
                            {
                                lnkMerge.Visible = false;
                                lnkDetail.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        protected void gvDtl_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndexDtl"].ConvertTo<int>();
            int pagesize = gvDtl.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
            }
        }

        protected void gvDtl_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndexDtl"] = e.NewPageIndex;
            this.gvDtl.PageIndex = e.NewPageIndex;
            BindMergeDtl(ViewState["MergeId"].ToString());
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobj.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj + " - " + pd.Source_Sys }).OrderBy(i => i.objDESC).ToList();
                var validation = db.Ref_Rule.ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }
    }
}