
function disableObj(id) {
    document.getElementById(id).disabled = true;
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function getQS(a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=');
        if (p.length != 2) continue;
        b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
/*
function Cleanup() 
{
window.clearInterval(idTmr);
//“CollectGarbage?fire JScript's garbage collection to release the reference to Excel
CollectGarbage();
} */
function validate(str) {
    if (str == null || str == undefined)
    { return ""; }
    else
    { return str; }
}
function getDateDDMMYYYY(date) {
    var d = date.getDate();
    var day = (d < 10) ? '0' + d : d;
    var m = date.getMonth() + 1;
    var month = (m < 10) ? '0' + m : m;
    var yy = date.getYear();
    var year = (yy < 1000) ? yy + 1900 : yy;
    var theDateStr = year + "-" + month + "-" + day;
    return theDateStr;
}
function getDateDMYHMS(date) {
    var d = date.getDate();
    var day = (d < 10) ? '0' + d : d;
    var m = date.getMonth() + 1;
    var month = (m < 10) ? '0' + m : m;
    var yy = date.getYear();
    var HH = (date.getHours() < 10) ? "0" + date.getHours() : date.getHours();
    var MM = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
    var ss = (date.getSeconds() < 10) ? "0" + date.getSeconds() : date.getSeconds();
    var AmPm = (HH > 11) ? "PM" : "AM";
    var TheTime = HH + ":" + MM + ":" + ss + " " + AmPm;

    var year = (yy < 1000) ? yy + 1900 : yy;
    var theDateStr = year + "-" + month + "-" + day + ' ' + TheTime;
    return theDateStr;
}

function AddAudit(objID, objName, Desc, objType, updType, fromVal, toVal, userID) {
    OpenConnX();
    var SQL = "SELECT ISNULL(MAX(LOG_ID),0) + 1 as NEW_ID FROM SYS_LOG"
    var rsID = new ActiveXObject("ADODB.Recordset");
    var newID = 1;
    rsID.open(SQL, cnX, 1, 3);
    if (!rsID.bof) {
        rsID.MoveFirst();
        newID = rsID.Fields("NEW_ID").value;
    }
    rsID.close();

    var d = new Date();
    var updateDate = getDateDMYHMS(d);

    SQL = "SELECT * FROM SYS_LOG";
    rsX.open(SQL, cnX, 2, 3);
    rsX.AddNew;
    rsX.fields("LOG_ID").value = newID;
    rsX.fields("OBJECT_ID").value = objID;
    rsX.fields("OBJECT_NAME").value = objName;
    rsX.fields("DESCRIPTION").value = Desc;
    rsX.fields("OBJECT_TYPE").value = objType;
    rsX.fields("UPDATE_TYPE").value = updType;
    rsX.fields("ORIGINAL_VALUE").value = fromVal;
    rsX.fields("UPDATE_VALUE").value = toVal;
    rsX.fields("UPDATE_BY").value = userID;
    rsX.fields("UPDATE_DATE").value = updateDate;

    rsX.Update;
    CloseConnX();

}
function DropSP(SC_ID) {
    var NextLine = "\n";
    var objConn = new ActiveXObject("ADODB.Connection");
    var rsFactor = new ActiveXObject("ADODB.Recordset");
    var strConn = "Provider=SQLOLEDB;Data Source=" + SQL_SERVER + ";Initial Catalog=" + SQL_DATABASE + ";User Id=" + SQL_UID + ";Password=" + SQL_PWD + ";";
    objConn.open(strConn, "", "");
    var SQL = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_" + SC_ID + "]') AND type in (N'P', N'PC'))" + NextLine
    SQL = SQL + "DROP PROCEDURE [dbo].[SP_" + SC_ID + "]" + NextLine
    objConn.execute(SQL);
    objConn.close();
}

function GetParamList(SC_ID) {
    var AttributeName, AttributeType, AttributeLen
    var Param;
    var Default;
    var strParam = "";
    var NextLine = "\n";
    //var SQL = "SELECT DISTINCT ATTRIBUTE_NAME, ATTRIBUTE_DATA_TYPE FROM dbo.FACTOR_INFO WHERE SC_ID = '" + SC_ID + "' AND ATTRIBUTE_NAME NOT IN ('ID_NO','ID_TYPE')";
    var SQL = "SELECT SC_ID, ATTRIBUTE_NAME, ATTRIBUTE_DATA_TYPE, ATTRIBUTE_DATA_LENGTH  FROM FACTOR_ATTRIBUTE WHERE SC_ID =  '" + SC_ID + "' AND ATTR_ACTIVE_FLAG = 'Y'"
    OpenConn();
    rs.Open(SQL, cn, 1, 3)
    if (!rs.bof) {
        rs.MoveFirst();
        while (!rs.eof) {
            AttributeName = rs.Fields("ATTRIBUTE_NAME").value
            AttributeType = rs.Fields("ATTRIBUTE_DATA_TYPE").value
            AttributeLen = rs.Fields("ATTRIBUTE_DATA_LENGTH").value

            Param = "@p" + AttributeName.replace(" ", "");
            (AttributeType == "NUM") ? DataType = "NUMERIC(18,2)" : DataType = "VARCHAR(" + AttributeLen + ")";
            strParam = strParam + "	" + Param + " " + DataType + "," + NextLine
            rs.MoveNext();
        }
    }
    CloseConn();
    return strParam;
}

function CreateSP(SC_ID) {

    var FID, FType, FDesc, FWeight, VMin, VMax, VItem, VScore, VWeighted_Score
    var ScoreArea, ScoringMethod
    var DataType, Param, Logic, sFilter, ParamList
    var AttributeName, Attribute, AttributeType, LOVSource, LOVTable, LOVSchema
    var KeyFieldName, KeyFieldType, RetFieldName, RetFieldType
    var RetFieldParam, KeyFieldParam

    var SQL = "SELECT * FROM FACTOR_INFO WHERE SC_ID = '" + SC_ID + "'"
    var strConn = "Provider=SQLOLEDB;Data Source=" + SQL_SERVER + ";Initial Catalog=" + SQL_DATABASE + ";User Id=" + SQL_UID + ";Password=" + SQL_PWD + ";";
    var objConn = new ActiveXObject("ADODB.Connection");
    var rsFactor = new ActiveXObject("ADODB.Recordset");
    var NextLine = "\n";
    var delimiter = '|';


    //Get Parameter List for Create SP Parameter Declaration
    ParamList = GetParamList(SC_ID);
    Logic = ""
    var recordCount = 0;
    var n = 1;

    objConn.open(strConn, "", "");
    rsFactor.open(SQL, objConn, 1, 3);
    if (!rsFactor.bof) {
        recordCount = rsFactor.RecordCount;

        rsFactor.MoveFirst();
        while (!rsFactor.eof) {
            ScoreArea = rsFactor.Fields("SCORE_AREA").value
            FID = rsFactor.Fields("FACTOR_ID").value
            FType = rsFactor.Fields("FACTOR_TYPE").value
            FDesc = rsFactor.Fields("DESCRIPTION").value
            FWeight = rsFactor.Fields("WEIGHT").value
            ScoringMethod = rsFactor.Fields("SCORE_METHOD").value
            AttributeName = rsFactor.Fields("ATTRIBUTE_NAME").value
            AttributeType = rsFactor.Fields("ATTRIBUTE_DATA_TYPE").value
            LOVSource = rsFactor.Fields("LOOKUP_TABLE_SOURCE").value
            LOVSchema = rsFactor.Fields("LOOKUP_TABLE_SCHEMA").value
            LOVTable = rsFactor.Fields("LOOKUP_TABLE_NAME").value
            KeyFieldName = rsFactor.Fields("LOOKUP_FIELD_NAME").value;
            KeyFieldType = rsFactor.Fields("LOOKUP_FIELD_TYPE").value;
            RetFieldName = rsFactor.Fields("RETURN_FIELD_NAME").value;
            RetFieldType = rsFactor.Fields("RETURN_FIELD_TYPE").value;

            //Get internal variable to hold Return Field / Lookup Field
            (RetFieldType == "NUM") ? RetFieldParam = "@LOV_RET_NUM" : RetFieldParam = "@LOV_RET_CHAR";
            (KeyFieldType == "CHAR") ? Attribute = "'" + AttributeName + "'" : Attribute = AttributeName;



            (n == recordCount) ? delimiter = "" : delimiter = "|";
            Param = "@p" + AttributeName.replace(" ", "");

            Logic = Logic + "	----- " + FID + " -----" + NextLine
            if (ScoringMethod == "DIR") {
                //filter when hit Range / List

                sFilter = ((FType == "LIST") ? "LTRIM(RTRIM(ITEM)) = " + Param : Param + " BETWEEN [MIN] AND [MAX]");
            }
            if (ScoringMethod == "LOV") {
                sFilter = ((FType == "LIST") ? "UPPER(LTRIM(RTRIM(ITEM))) = UPPER(LTRIM(RTRIM(" + RetFieldParam + ")))" : RetFieldParam + " BETWEEN [MIN] AND [MAX]");
                if (RetFieldName == "0")//Check is Exists
                {
                    Logic = Logic + "SET " + RetFieldParam + " = (SELECT CASE WHEN COUNT(1) >=1 THEN 'Y' ELSE 'N' END FROM [" + LOVSource + "].[" + LOVSchema + "].[" + LOVTable + "] WHERE [" + KeyFieldName + "] = " + Param + ")" + NextLine;
                }
                else //Lookup Value
                {
                    Logic = Logic + "SET " + RetFieldParam + " = (SELECT [" + RetFieldName + "] FROM [" + LOVSource + "].[" + LOVSchema + "].[" + LOVTable + "] WHERE [" + KeyFieldName + "] = " + Param + ")" + NextLine;
                }
            }

            Logic = Logic + "	SET @SCORE = (SELECT ISNULL(SUM(WEIGHTED_SCORE),0) FROM FACTOR_X_VALUE " + NextLine
            Logic = Logic + "	WHERE SC_ID = '" + SC_ID + "' AND FACTOR_ID = '" + FID + "' AND " + NextLine
            Logic = Logic + "	" + sFilter + ")" + NextLine

            Logic = Logic + "	SET @TOTAL = @TOTAL + @SCORE" + NextLine
            Logic = Logic + "	SET @FactorList = @FactorList + '" + FID + "=' + CAST(@SCORE as VARCHAR(10)) + '" + delimiter + "'" + NextLine
            Logic = Logic + NextLine
            Logic = Logic + "	INSERT INTO CREDIT_SCORING_DET (Application_ID,FACTOR_ID,FACTOR_DESC,SUBMIT_VALUE, RETURN_VALUE, SCORE, WEIGHT, CREDIT_SCORE)"
            Logic = Logic + "	VALUES(@ApplicationID, '" + FID + "', '" + FDesc + "'," + Param + ", " + RetFieldParam + ", (@SCORE/" + FWeight + "), " + FWeight + ", @SCORE)" + NextLine + NextLine;
            rsFactor.MoveNext();
            n = n + 1;
        }



        //------------------------------- DROP EXISTING SP ---------------------

        SQL = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_" + SC_ID + "]') AND type in (N'P', N'PC'))" + NextLine
        SQL = SQL + "DROP PROCEDURE [dbo].[SP_" + SC_ID + "]" + NextLine
        //SQL = SQL + "GO" + NextLine
        objConn.execute(SQL);

        //------------------------------- CREATE NEW SP ------------------------
        SQL = "CREATE PROCEDURE [dbo].[SP_" + SC_ID + "]" + NextLine
        //SQL = SQL + "	@pApplicationID VARCHAR(50)," + NextLine
        SQL = SQL + "	" + ParamList
        SQL = SQL + "	@oTOTAL_SCORE NUMERIC(18,2) OUTPUT," + NextLine
        SQL = SQL + "	@oTHRESHOLD NUMERIC(18,2) OUTPUT," + NextLine
        SQL = SQL + "	@oSTATUS VARCHAR(1) OUTPUT," + NextLine
        SQL = SQL + "	@oFACTOR VARCHAR(1000) OUTPUT" + NextLine
        SQL = SQL + "AS BEGIN" + NextLine
        SQL = SQL + "	DECLARE @ApplicationID varchar(50)" + NextLine
        SQL = SQL + "	DECLARE @LOV_RET_NUM NUMERIC(18,2)" + NextLine
        SQL = SQL + "	DECLARE @LOV_RET_CHAR VARCHAR(50)" + NextLine
        SQL = SQL + "	DECLARE @SCORE NUMERIC(18,2)" + NextLine
        SQL = SQL + "	DECLARE @TOTAL NUMERIC(18,2)" + NextLine
        SQL = SQL + "	DECLARE @FactorList Varchar(1000)" + NextLine
        SQL = SQL + "	SET @TOTAL = 0 " + NextLine
        SQL = SQL + "	SET @FactorList = ''" + NextLine
        SQL = SQL + "	SET @ApplicationID = NewID()" + NextLine
        SQL = SQL + "	-- Get Threshold --" + NextLine
        SQL = SQL + "	SELECT @oTHRESHOLD = THRESHOLD FROM SCORE_CARD WHERE SC_ID = '" + ScoreCardID + "'" + NextLine

        SQL = SQL + "	" + Logic + NextLine
        SQL = SQL + "	IF @TOTAL >= @oTHRESHOLD " + NextLine
        SQL = SQL + "		SET @oSTATUS = 'P'" + NextLine
        SQL = SQL + "	ELSE" + NextLine
        SQL = SQL + "		SET @oSTATUS = 'F'" + NextLine + NextLine
        SQL = SQL + "	SELECT @oTOTAL_SCORE = @TOTAL" + NextLine
        SQL = SQL + "	SELECT @oFACTOR = @FactorList" + NextLine
        SQL = SQL + "	INSERT INTO CREDIT_SCORING ([APPLICATION_ID], [APPLICANT_NAME],[SCORE_AREA],[SC_ID],[THRESHOLD],[SCORE],[STATUS],[PROCESS_DTTM])" + NextLine
        SQL = SQL + "	VALUES(@ApplicationID, '', '" + ScoreArea + "', '" + ScoreCardID + "', @oTHRESHOLD,  @oTOTAL_SCORE, @oSTATUS, getDate())" + NextLine

        SQL = SQL + "END" + NextLine
        objConn.execute(SQL);
        //alert(SQL);
        alert("Score Card Successfully Updated.");
    }
    rsFactor.close();
    objConn.close();
    return SQL;

}

function disableRadio(radioObj, val) {
    if (!radioObj) return;
    var radioLength = radioObj.length;
    if (radioLength == undefined)
        if (radioObj.value == val.toString())
            return radioObj.disabled = true;
        else
            return;
    for (var i = 0; i < radioLength; i++) {
        if (radioObj[i].value == val.toString()) {
            radioObj[i].disabled = true;
        }
    }
}

function getCheckedValue(radioObj) {
    if (!radioObj) return "";
    var radioLength = radioObj.length;
    if (radioLength == undefined)
        if (radioObj.checked)
            return radioObj.value;
        else
            return "";
    for (var i = 0; i < radioLength; i++) {
        if (radioObj[i].checked) {
            return radioObj[i].value;
        }
    }
    return "";
}

function setCheckedValue(radioObj, newValue) {
    if (!radioObj)
        return;
    var radioLength = radioObj.length;
    if (radioLength == undefined) {
        radioObj.checked = (radioObj.value == newValue.toString());
        return;
    }
    for (var i = 0; i < radioLength; i++) {
        radioObj[i].checked = false;
        if (radioObj[i].value == newValue.toString()) {
            radioObj[i].checked = true;
        }
    }
}


function addRow(tableID) {

    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    element1.type = "checkbox";
    cell1.appendChild(element1);

    var cell2 = row.insertCell(1);
    cell2.innerHTML = rowCount + 1;

    var cell3 = row.insertCell(2);
    var element2 = document.createElement("input");
    element2.type = "text";
    cell3.appendChild(element2);

}

function selectListItem(lst, val) {
    if (!lst) return;
    if (!val) return;
    var itemCount = lst.options.length;
    if (itemCount > 0) {
        for (var i = 0; i < itemCount; i++) {
            if (lst.options[i].value == val) {
                lst.selectedIndex = i;
                return;
            }
        }
    }
}

//onkeypress : only allow number with unlimited decimal point
function validateNumber(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);

    var regex = /-|[0-9]|\./;

    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

//onkeypress
function NumAndsixDecimals(e, obj) {
    var event = e || window.event;
    var text = obj.value;
    var id = obj.id

    if ((event.which != 45) && (event.which != 46 || text.indexOf('.') != -1) &&
        ((event.which < 48 || event.which > 57) &&
          (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 6) &&
        (event.which != 0 && event.which != 8) &&
        (obj.selectionStart >= text.length - 6)) {
        event.preventDefault();
    }
}

//for onkeyup
function NumAndTwoDecimals(e, field) {
    var val = field.value;
    var re = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)$/g;
    var re1 = /^([0-9]+[\.]?[0-9]?[0-9]?|[0-9]+)/g;
    if (re.test(val)) {
        //do something here

    } else {
        val = re1.exec(val);
        if (val) {
            field.value = val[0];
        } else {
            field.value = "";
        }
    }
}

function validateSpecialChar(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
           ((evt.which) ? evt.which : 0));

    if (charCode > 31 && (charCode >= 48 && charCode <= 57) ||
                (charCode > 64 && charCode < 91) ||
                (charCode == 46) ||
                (charCode == 8) ||
                (charCode > 94 && charCode < 123) || (charCode == 72) || (charCode == 75) || (charCode == 77) || (charCode == 80) || (charCode == 32)) {
        return true;
    }
    if (charCode == 9) { return true; }
    else { return false; }
}

function validatePasswordSpecialChar(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
           ((evt.which) ? evt.which : 0));

    if ((charCode == 60) || (charCode == 62) || (charCode == 63) || (charCode == 124)) {
        return false;
    }
    else { return true; }


}

function validateNameSpecialChar(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
           ((evt.which) ? evt.which : 0));

    if (charCode > 31 &&
                (charCode >= 64 && charCode < 91) ||
                (charCode == 8) ||
                (charCode > 94 && charCode < 123) || (charCode == 72) || (charCode == 75) || (charCode == 77) || (charCode == 80) || (charCode == 47) || (charCode == 39) || (charCode == 32) || (charCode == 45)) {
        return true;
    }
    if (charCode == 9) { return true; }
    else { return false; }
}

function validateParamNameSpecialChar(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
           ((evt.which) ? evt.which : 0));

    if (charCode > 31 &&
                (charCode >= 64 && charCode < 91) ||
                (charCode == 8) ||
                (charCode > 94 && charCode < 123) || (charCode == 72) || (charCode == 75) || (charCode == 77) || (charCode == 80) || (charCode == 47) || (charCode == 32) || (charCode == 45) || (charCode >= 48 && charCode <= 57)) {
        return true;
    }
    if (charCode == 9) { return true; }
    else { return false; }
}

function validateLoginIDSpecialChar(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
           ((evt.which) ? evt.which : 0));

    if (charCode > 31 && (charCode >= 48 && charCode <= 57) ||
                (charCode > 64 && charCode < 91) ||
                (charCode == 8) ||
                (charCode > 94 && charCode < 123) || (charCode == 72) || (charCode == 75) || (charCode == 77) || (charCode == 80)) {
        return true;
    }
    if (charCode == 9) { return true; }
    else { return false; }
}

function showDateTime() {
    window.history.forward(1);

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var now = new Date();
    var day = ("0" + now.getDate()).substring(("0" + now.getDate().toString()).length - 2)
    var month = months[now.getMonth()]
    var year = now.getFullYear()
    var hour = ("0" + now.getHours()).substring(("0" + now.getHours().toString()).length - 2)
    var minute = ("0" + now.getMinutes()).substring(("0" + now.getMinutes().toString()).length - 2)
    var second = ("0" + now.getSeconds()).substring(("0" + now.getSeconds().toString()).length - 2)

    document.getElementById("ctl00_currentDateTime").innerHTML = day + " " + month + " " + year + " " + hour + ":" + minute + ":" + second;
    setTimeout("showDateTime()", 1000);
};


//$.fn.Watermark = function (text) {
//    if (text == undefined)
//        return this.attr("watermark");
//    this.attr("watermark", text);
//    this.focus(function () {
//        $(this).filter(function () {
//            return $(this).val() == "" || $(this).val() == $(this).attr("watermark");
//        }).removeClass("watermarkOn").val("");
//    });
//    this.blur(function () {
//        if ($(this).val() == "" || $(this).val() == $(this).attr("watermark"))
//            $(this).addClass("watermarkOn").val($(this).attr("watermark"));
//        else
//            $(this).removeClass("watermarkOn");
//    });
//    this.blur();
//}