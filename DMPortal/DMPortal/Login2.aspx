﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login2.aspx.cs" Inherits="DMPortal.Login2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function doLogin(e) {
            // look for window.event in case event isn't passed in
            if (typeof e == 'undefined' && window.event) { e = window.event; }
            //        if (e.keyCode == 13)
            //        {
            //           imgLogin_Click();
            //        }
        }

    </script>
</head>
<body onload="window.history.forward(1);">
    <div class="well container">
        <div class="col-md-offset-4 col-md-12" style="padding-top: 20px; text-align: center;">
            <img src="Resources/DI_logo_full.jpg" alt="ISM" width="300px" height="100px"  class="img-responsive" />
        </div>
        <div class="col-md-12" style="padding-top: 20px">
            <div class="col-md-offset-3 col-md-6">
                <fieldset>
                    <legend style="text-align: center;">Master Data Management Portal</legend>
                    <form class="form-horizontal" id="form_register" role="form" runat="server">
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Username</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                    <input type="text" runat="server" name="txtUID" id='txtUID' class='form-control'
                                        placeholder='Username' size="20" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Passsword</label>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                    <input type="password" runat="server" name="txtPWD" id='txtPWD' class='form-control'
                                        placeholder='Password' onkeydown="doLogin();" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-12">
                                <asp:Label ID="lblmsg" runat="server" ForeColor="red" Text="The username or password you entered is incorrect."
                                    Visible="false"></asp:Label>
                            </div>
                            <div class="col-md-offset-3 col-md-12">
                                <asp:Button ID="btnlogin" runat="server" Text="Login" Style="width: 140px;" CssClass="btn btn-primary" OnClick="btnlogin_Click" />

                            </div>
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</body>
</html>
