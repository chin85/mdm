﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GridViewTest.aspx.cs" Inherits="DMPortal.GridViewTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <%-- <asp:GridView ID="gridData" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False"
        AllowPaging="True" EmptyDataText="No Data" OnRowEditing="gridData_RowEditing" OnRowUpdating="gridData_RowUpdating" OnRowCancelingEdit="gridData_RowCancelingEdit" DataKeyNames="Row_id" >
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton Text="Edit" CssClass="btn btn-success" runat="server" CommandName="Edit" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton Text="Update" CssClass="btn btn-success" runat="server" CommandName="Update" />
                    <asp:LinkButton Text="Cancel" CssClass="btn btn-danger" runat="server" CommandName="Cancel" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>--%>

     <asp:GridView ID="TaskGridView" runat="server" 
        AutoGenerateEditButton="True" 
        AllowPaging="true"
        OnRowEditing="TaskGridView_RowEditing"         
        OnRowCancelingEdit="TaskGridView_RowCancelingEdit" 
        OnRowUpdating="TaskGridView_RowUpdating"
        OnPageIndexChanging="TaskGridView_PageIndexChanging" OnRowDataBound="TaskGridView_RowDataBound" DataKeyNames="Row_id">
      </asp:GridView>

</asp:Content>
