﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using DMPortal;

namespace DMPortal
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AuthConfig.RegisterOpenAuth();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            Response.Clear();
            var ex = Server.GetLastError().GetBaseException();
            var code = (ex is HttpException) ? (ex as HttpException).GetHttpCode() : 500;
            string Path = ConfigurationManager.AppSettings["LogDir"].ToString();

            if (Directory.Exists(Path) == false)
                Directory.CreateDirectory(Path);

            if (code != 404)
            {
                System.IO.FileStream fs = new System.IO.FileStream(Path + "ErrorLog.txt", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite);
                System.IO.StreamWriter s = new System.IO.StreamWriter(fs);
                s.BaseStream.Seek(0, System.IO.SeekOrigin.End);
                s.WriteLine("\nERROR DATE: " + System.DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture) + " \nERROR MESSAGE: " + ex.Message + "\nSOURCE: " + ex.Source + "\nFORM NAME: " + System.Web.HttpContext.Current.Request.Url.ToString() + "\nQUERYSTRING: " + Request.QueryString.ToString() + "\nTARGETSITE: " + ex.TargetSite.ToString() + "\nSTACKTRACE: " + ex.StackTrace + System.Diagnostics.EventLogEntryType.Error, Environment.NewLine);
                s.WriteLine("-------------------------------------------------------------------------------------------------------------");
                s.Close();
            }
        }
    }
}
