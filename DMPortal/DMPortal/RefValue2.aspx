﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefValue2.aspx.cs" Inherits="DMPortal.RefValue2" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/comm.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to delete?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }

        function selected(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length >= 2;

            if (checked) {

                if (confirm("Are you sure you want to merge?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least two.');
                return false;
            }


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert runat="server" ID="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Object Value</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-md-4 control-label" style="width: 150px">Project: </td>
                                    <td >
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. project."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-4 control-label" style="width: 150px">&nbsp;&nbsp;&nbsp; Object: </td>
                                    <td >
                                        <asp:DropDownList ID="ddlobj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlobj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlobj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. object."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-4 control-label" style="width: 150px">&nbsp;&nbsp;&nbsp; Search by: </td>
                                    <td >
                                        <asp:DropDownList ID="ddlatt" runat="server" CssClass="form-control" >
                                        </asp:DropDownList>

                                    </td>
                                    <td class="col-4 control-label" style="width: 150px; text-align:center">Search Value: </td>
                                    <td>
                                        <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td class="auto-style1" style="width: 100px; text-align: center">
                                        <asp:Button ID="ObjBtn" runat="server" OnClick="Obj_Search" Text="Search" CssClass="btn btn-success" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" CssClass="table table-hover table-bordered" EmptyDataText="No Records." AutoGenerateEditButton="True"
                        DataKeyNames="Row_id"
                        AllowPaging="true"
                        OnRowEditing="gv_RowEditing"
                        OnRowCancelingEdit="gv_RowCancelingEdit"
                        OnRowUpdating="gv_RowUpdating"
                        OnPageIndexChanging="gv_PageIndexChanging"
                        OnRowCommand="gv_RowCommand"
                        OnRowCreated="gv_RowCreated"
                        OnRowDataBound="gv_RowDataBound" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("row_id")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <uc1:UserAlert runat="server" ID="UserAlert1" />
                    <asp:Label ID="lblErrormsg" runat="server" ForeColor="Red">fg</asp:Label>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btndelete" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <br />
        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger" CausesValidation="false" OnClick="btndelete_Click" OnClientClick="return checkselector(this)" />

        </div>
    </div>
</asp:Content>
