﻿using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Services;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.Security.Principal;
using System.Web.SessionState;



namespace DMPortal
{
    public partial class Login2 : System.Web.UI.Page
    {
        public string logonId { get; set; }
        public int Uid { get; set; }
        public string sMode { get; set; }
        public bool sRoleManagement = false;
        public bool sUserManagement = false;
        public bool sRoleAccess = false;
        public bool sRefObject = false;
        public bool sRefObjectV = false;
        public bool sRefObjectVMapped = false;
        public bool sRefObjectAtt = false;
        public bool sRefProj = false;
        public bool sRefSource = false;
        public bool sObjView = false;
        public bool sObjImport = false;
        public bool sMergeSplit = false;
        public bool sAuditLog = false;
        public bool sChceker = false;
        public bool sMaker = false;
        public string homeurl { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string erroeurl { get; set; }
        public SessionInfo oDOSession { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.sMode = string.IsNullOrEmpty(Request.QueryString["smode"]) ? "" : Request.QueryString["smode"];
            this.homeurl = ConfigurationManager.AppSettings["HomeURL"].ToString();
            this.erroeurl = ConfigurationManager.AppSettings["ErrorURL"].ToString();
            if (oDOSession != null)
            {
                logonId = oDOSession.LogonId;
                Uid = oDOSession.Id;

                if (sMode == "L")
                {
                    LogoutUser(this.Uid, this.logonId, CommonEnum.LogOffMode.logoff.GetDescription());
                    Session.Abandon();
                    Session.Clear();
                    Session.RemoveAll();
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Redirect(homeurl, true);
                }
                else
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        var userinfo = context.USERs.Where(i => i.ID == Uid).SingleOrDefault();
                        if (userinfo != null)
                        {
                            if (userinfo.LOGON_STATUS == CommonEnum.LogonStatus.Off.GetDescription())
                            {
                                Session.Abandon();
                                System.Web.Security.FormsAuthentication.RedirectToLoginPage();
                                Response.Redirect(homeurl, true);
                            }
                        }
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //var encrypt = new EncryptionManager();

            if (Request.Form["userid"] != null && Request.Form["password"] != null)
            {
                UserId = Request.Form["userid"];
                Password = Request.Form["password"];//encrypt.Decrypt(Request.QueryString["psd"].ToString());

                if (!String.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Password))
                    btnlogin_Click(btnlogin, new EventArgs());
            }

            //int loop1;
            //NameValueCollection coll;

            ////Load Form variables into NameValueCollection variable.
            //coll = Request.Form;
            //// Get names of all forms into a string array.
            //String[] arr1 = coll.AllKeys;
            //for (loop1 = 0; loop1 < arr1.Length; loop1++)
            //{
            //    Response.Write("Form: " + arr1[loop1] + "<br>");
            //}
        }

        private void LogoutUser(int uId, string uLoginId, string logoffmode)
        {
            try
            {
                using (MDMEntities context = new MDMEntities())
                {
                    var oUser = context.USERs.Where(i => i.ID == uId).SingleOrDefault();
                    var oUserLogonLog = context.USER_LOGON_LOG.Where(i => i.OBJECT_ID == uId && i.LOG_OUT_DATETIME == null).OrderByDescending(j => j.LOG_ID).FirstOrDefault();

                    if (oUser != null)
                    {
                        oUser.ID = uId;
                        oUser.LOGON_STATUS = CommonEnum.LogonStatus.Off.GetDescription();
                        oUser.MODIFIER_ID = uLoginId;
                        oUser.MODIFY_DATETIME = DateTime.Now;
                    }

                    if (oUserLogonLog != null)
                    {
                        oUserLogonLog.OBJECT_ID = uId;
                        oUserLogonLog.LOG_OFF_MODE = logoffmode;//CommonEnum.LogOffMode.logoff.GetDescription();
                        oUserLogonLog.LOG_OUT_DATETIME = DateTime.Now;
                    }
                    context.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            EncryptionManager encrypt = new EncryptionManager();
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            SessionIDManager manager = new SessionIDManager();
            string newSessionId = manager.CreateSessionID(HttpContext.Current);

            string loginid = string.Empty;
            string strpassword = string.Empty;
            if (!String.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Password))
            {
                loginid = UserId;
                strpassword = Password;
            }
            else
            {
                loginid = txtUID.Value;
                strpassword = encrypt.EncryptSHA1(txtPWD.Value);
            }

            string strsession = SetupEnum.SESSION_TIMEOUT.ToString();

            if (string.IsNullOrEmpty(loginid) || string.IsNullOrEmpty(strpassword))
            {
                lblmsg.Text = LoginMsgEnum.Enter_username_password.GetDescription();
            }
            else
            {
                //try
                //{

                using (MDMEntities context = new MDMEntities())
                {
                    var userlist = context.USERs.Where(i => i.LOGON_ID == loginid).SingleOrDefault();
                    var otimeout = context.SETUP_SETTING.Where(i => i.PARAM_NAME == strsession).SingleOrDefault();
                    if (userlist == null)
                        lblmsg.Text = LoginMsgEnum.Inv_user_id.GetDescription();
                    else
                    {
                        if (userlist.LOGON_PASSWORD != strpassword)
                        {
                            lblmsg.Text = LoginMsgEnum.Inv_password.GetDescription();
                            return;
                            //Response.Redirect(this.erroeurl, true);
                        }
                        else if (userlist.ACCOUNT_STATUS == CommonEnum.statusEnum.Inactive.GetDescription())
                        {
                            lblmsg.Text = LoginMsgEnum.User_in.GetDescription();
                            return;
                            //Response.Redirect(this.erroeurl, true);
                        }
                        else
                        {
                            var user = new SessionInfo()
                            {
                                LogonId = userlist.LOGON_ID,
                                Id = userlist.ID.ConvertTo<int>(),
                                UserRoleId = userlist.ROLE.ConvertTo<int>(),
                                LogId = 0,
                                isChecker = userlist.CHECKER_FG,
                                isMaker = userlist.MAKER_FG,
                            };

                            this.Session["SessionInfo"] = user;
                            getrole(user.UserRoleId);

                            if (userlist.LOGON_STATUS == CommonEnum.LogonStatus.On.GetDescription())
                            {
                                var oUserLog = context.USER_LOGON_LOG.Where(i => i.OBJECT_ID == user.Id && i.LOG_OUT_DATETIME == null).OrderByDescending(j => j.LOG_ID).FirstOrDefault();

                                if (oUserLog != null)
                                {
                                    if (!oUserLog.ACT_SESSION_ID.Equals(newSessionId))//HttpContext.Current.Session.SessionID
                                    {
                                        LogoutUser(user.Id, user.LogonId, CommonEnum.LogOffMode.forcelogoff.GetDescription());
                                        successLogon(user.Id, newSessionId);
                                    }
                                }
                            }
                            else
                            {
                                successLogon(user.Id, newSessionId);
                            }
                            Response.Redirect("RefObj.aspx");
                        }
                    }
                }
                //}
                //catch (DbEntityValidationException ex)
                //{
                //    foreach (var eve in ex.EntityValidationErrors)
                //    {
                //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //        foreach (var ve in eve.ValidationErrors)
                //        {
                //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //                ve.PropertyName, ve.ErrorMessage);
                //        }
                //    }
                //    throw;
                //}
            }
            lblmsg.Visible = true;
            txtUID.Focus();
        }

        private void getrole(int roleid)
        {
            using (MDMEntities context = new MDMEntities())
            {
                var role = (from pd in context.ROLE_ACCESS
                            join od in context.SCREEN_MAINTENANCE on pd.SCREEN_ID equals od.ID
                            where pd.ROLE_ID == roleid
                            orderby od.SCREEN_NAME
                            select new
                            {
                                pd.ACCESS,
                                od.SCREEN_CODE,
                                od.SCREEN_NAME,
                            }).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        if (item.SCREEN_CODE == "UP003")
                            sRoleManagement = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM002")
                            sRefObject = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM003")
                            sRefObjectV = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "UP002")
                            sUserManagement = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "UP001")
                            sRoleAccess = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM004")
                            sRefObjectVMapped = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM005")
                            sRefObjectAtt = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM001")
                            sRefProj = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM006")
                            sRefSource = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM007")
                            sObjView = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM008")
                            sObjImport = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "PM009")
                            sMergeSplit = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "RP001")
                            sAuditLog = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "DB001")
                            sChceker = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);
                        else if (item.SCREEN_CODE == "DB002")
                            sMaker = (item.ACCESS == CommonEnum.statusEnum.Y.GetDescription() ? true : false);

                    }

                    var roleaccess = new SessionRole()
                    {

                        showRoleAccess = sRoleAccess,
                        showRoleManagement = sRoleManagement,
                        showUserManagement = sUserManagement,
                        showRefObject = sRefObject,
                        showRefObjectV = sRefObjectV,
                        showRefObjectVMapped = sRefObjectVMapped,
                        showRefObjectAtt = sRefObjectAtt,
                        showRefProj = sRefProj,
                        showRefSource = sRefSource,
                        showObjView = sObjView,
                        showObjImport = sObjImport,
                        showMergeSplit = sMergeSplit,
                        showAuditLog = sAuditLog,
                        showChecker = sChceker,
                        showMaker = sMaker,

                    };

                    this.Session["SessionRole"] = roleaccess;
                }
            }
        }

        private void successLogon(int Uid, string newSessionId)
        {
            int pkid = 0;
            using (MDMEntities context = new MDMEntities())
            {
                var ouser = context.USERs.Where(i => i.ID == Uid).SingleOrDefault();

                if (ouser != null)
                {
                    ouser.LOGON_STATUS = CommonEnum.LogonStatus.On.GetDescription();
                    ouser.MODIFY_DATETIME = DateTime.Now;
                    ouser.FIRST_LOGON_FLAG = CommonEnum.statusEnum.N.GetDescription();
                }

                USER_LOGON_LOG ouser_log = new USER_LOGON_LOG();
                ouser_log.OBJECT_ID = Uid;
                ouser_log.LOG_IN_DATETIME = DateTime.Now;
                ouser_log.ACT_SESSION_ID = newSessionId;//HttpContext.Current.Session.SessionID; //active session
                context.USER_LOGON_LOG.Add(ouser_log);

                context.SaveChanges();
                pkid = (int)ouser_log.LOG_ID;
            }

            oDOSession = (SessionInfo)Session["SessionInfo"];
            oDOSession.LogId = pkid;

        }
    }
}