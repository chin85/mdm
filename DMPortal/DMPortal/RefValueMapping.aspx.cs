﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using DMPortal.Object;

namespace DMPortal
{
    public partial class RefValueMapping : System.Web.UI.Page
    {
        public string logonId { get; set; }
        public class Sources
        {
            public string Source { get; set; }
        }
        public class Project
        {
            public string Proj { get; set; }
            public long Projid { get; set; }
        }
        public class mapobject
        {
            public int ID { get; set; }
            public string strobj_type { get; set; }
            public string objvalDESC { get; set; }
            public string objrngDESC { get; set; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                BindDDL();
            }
        }

        private void BindDDL()
        {
            using (MDMEntities db = new MDMEntities())
            {
               
                //var obj = db.Ref_Obj // now we have in-memory query
                //             .Select(c => new Sources()
                //             {
                //                 Source = c.Source_Sys,
                //             }).Distinct().ToList();

                //if (obj.Count > 0)
                //{
                //    foreach (var item in obj)
                //    {
                //        var datavalue = item.Source;
                //        var datatext = item.Source;
                //        bindddlobject(ddlsource, datatext, datavalue);
                //        bindddlobject(ddlmapsource, datatext, datavalue);
                //    }
                //    ddlsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //    ddlmapsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //}

            var proj = db.Ref_Proj
                      .Select(c => new Project()
                      {
                          Proj = c.Project,
                          Projid = c.ID,
                      }).Distinct().ToList();

                if (proj.Count > 0)
                {
                    foreach (var item in proj)
                    {
                        var datavalue = item.Projid.ToString();
                        var datatext = item.Proj;
                        bindddlobject(ddlproj, datatext, datavalue);
                        bindddlobject(ddlmapproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    ddlmapproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void ddlsource_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlrefobj.Items.Clear();
            var _source = ddlsource.SelectedValue.ConvertTo<int>();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            if (_source > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    //var obj = db.Ref_Obj.Where(i => i.Source_Sys == _source).ToList();

                    var obj = (from od in db.Ref_Obj
                               join pd in db.Ref_Obj_Att on od.ID equals pd.obj_id
                               where od.Source_Sys == _source && od.Proj_id == _proj
                               orderby od.obj
                               select new
                               {
                                   od.obj,
                                   od.ID
                               }).Distinct().ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.obj;
                            bindddlobject(ddlrefobj, datatext, datavalue);
                        }
                        ddlrefobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }

        protected void ddlmapsource_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlmaprefobj.Items.Clear();
            var _source = ddlmapsource.SelectedValue.ConvertTo<int>();
            var _proj = ddlmapproj.SelectedValue.ConvertTo<int>();
            if (_source > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    //var obj = db.Ref_Obj.Where(i => i.Source_Sys == _source).ToList();

                    var obj = (from od in db.Ref_Obj
                               join pd in db.Ref_Obj_Att on od.ID equals pd.obj_id
                               where od.Source_Sys == _source && od.Proj_id == _proj
                               orderby od.obj
                               select new
                               {
                                   od.obj,
                                   od.ID
                               }).Distinct().ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.obj;
                            bindddlobject(ddlmaprefobj, datatext, datavalue);
                        }
                        ddlmaprefobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int obj = ddlrefobj.SelectedValue.ConvertTo<int>();
            int objmap = ddlmaprefobj.SelectedValue.ConvertTo<int>();
            int att = ddlatt.SelectedValue.ConvertTo<int>();

            using (MDMEntities db = new MDMEntities())
            {
                //var objV = db.Ref_Obj_Value.Where(i => i.obj_id == obj).ToList();

                var objV = (from od in db.Ref_Obj_Value
                            join pd in db.Ref_Obj_Att on od.obj_Att_id equals pd.ID
                            where pd.att_data_mapped == "Y" && pd.ID == att && od.obj_id == obj
                            select od
                           ).ToList();

                gv.DataSource = objV;
                gv.DataBind();
                btnsave.Enabled = true;
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;
            int objmap = ddlmaprefobj.SelectedValue.ConvertTo<int>();
            int objattmap = ddlattmap.SelectedValue.ConvertTo<int>();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlmapvalue = (e.Row.FindControl("ddlmapvalue") as DropDownList);
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                var lblmin = e.Row.FindControl("lblMin") as System.Web.UI.WebControls.Label;
                var lblmax = e.Row.FindControl("lblMax") as System.Web.UI.WebControls.Label;
                var lblval = e.Row.FindControl("lblval") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
                var obj_value_id = DataBinder.Eval(e.Row.DataItem, "id").ConvertTo<int>();
                var obj_type = DataBinder.Eval(e.Row.DataItem, "obj_type").ToString();
                var obj_value = DataBinder.Eval(e.Row.DataItem, "obj_value").ToString();
                var min = DataBinder.Eval(e.Row.DataItem, "min").ToString();
                var max = DataBinder.Eval(e.Row.DataItem, "max").ToString();

                if (obj_type.ToUpper() == "LIST")
                {
                    lblmin.Text = string.Empty;
                    lblmax.Text = string.Empty;
                    lblval.Text = obj_value;
                }
                else
                {
                    lblmin.Text = min;
                    lblmax.Text = max;
                    lblval.Text = string.Empty;
                }

                using (MDMEntities db = new MDMEntities())
                {
                    Ref_Obj_Mapped objmapped = new Ref_Obj_Mapped();
                    string strobjtype = string.Empty;
                    var objV = (from c in db.Ref_Obj_Value
                                join pd in db.Ref_Obj_Att on c.obj_Att_id equals pd.ID
                                where c.obj_id == objmap && pd.att_data_mapped == "Y" && pd.ID == objattmap
                                select new { c.ID, c.min, c.max, c.obj_type, c.obj_value }).OrderBy(i => i.obj_value);

                    var objVlist = objV.ToList()
                                  .Select(c => new mapobject()
                                  {
                                      ID = Convert.ToInt32(c.ID),
                                      strobj_type = c.obj_type,
                                      objvalDESC = c.obj_value, // + " : " + c.obj_value_desc,
                                      objrngDESC = Convert.ToString(c.min) //+ " - " + Convert.ToString(c.max) + ":" + c.obj_value_desc
                                  }).ToList();

                    var objtype = objVlist.FirstOrDefault();

                    if (objtype != null)
                    {
                        strobjtype = objtype.strobj_type.ToUpper();
                    }

                    ddlmapvalue.DataSource = objVlist;
                    if (strobjtype == "LIST")
                        ddlmapvalue.DataTextField = "objvalDESC";
                    else
                        ddlmapvalue.DataTextField = "objrngDESC";
                    ddlmapvalue.DataValueField = "ID";
                    ddlmapvalue.DataBind();
                    ddlmapvalue.Items.Insert(0, new ListItem("--Select--", "0"));

                    objmapped = db.Ref_Obj_Mapped.Where(i => i.obj_value_id == obj_value_id && i.m_obj_id == objmap).FirstOrDefault();

                    if (objmapped != null)
                    {
                        if (objmapped.m_obj_value_id != null)
                        {
                            ddlmapvalue.SelectedValue = objmapped.m_obj_value_id.ToString();
                        }
                    }
                }
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            MDMEntities db = new MDMEntities();
            int maprefobj = ddlmaprefobj.SelectedValue.ConvertTo<int>();
            int obj = ddlrefobj.SelectedValue.ConvertTo<int>();

            foreach (GridViewRow row in gv.Rows)
            {
                Ref_Obj_Value oobjv = new Ref_Obj_Value();
                List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
                int ID = Convert.ToInt32(gv.DataKeys[row.RowIndex].Value);
                string strvalue = row.Cells[3].Text;
                DropDownList ddlmapvalue = (DropDownList)row.FindControl("ddlmapvalue");
                string refvId = ddlmapvalue.SelectedValue;
                string strold = string.Empty;

                Ref_Obj_Mapped objv = (from c in db.Ref_Obj_Mapped
                                       where c.obj_value_id == ID && c.m_obj_id == maprefobj
                                       select c).FirstOrDefault();

                if (objv != null)
                {
                    if (objv.m_obj_value_id != null)
                    {
                        oobjv = (from c in db.Ref_Obj_Value
                                 where c.ID == objv.m_obj_value_id
                                 select c).FirstOrDefault();

                        if (oobjv != null)
                            strold = oobjv.obj_value; //+ " - " + oobjv.obj_value_desc;
                    }

                    if (objv.m_obj_value_id != refvId.ConvertTo<int>())
                        oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = ID, TBL_NAME = "Ref_Obj_Mapped", FIELD_NAME = "m_obj_value_id", OLD_VALUE = strold, NEW_VALUE = ddlmapvalue.SelectedItem.Text, DESC = "Mapped object value from : " + ddlsource.SelectedItem.Text + " | " + ddlrefobj.SelectedItem.Text + " | " + strvalue + " to : " + ddlmapsource.SelectedItem.Text + " | " + ddlmaprefobj.SelectedItem.Text + " | " + ddlmapvalue.SelectedItem.Text, CREATOR_ID = this.logonId });

                    if (refvId.ConvertTo<int>() == 0)
                    {
                        objv.m_obj_value_id = null;
                    }
                    else
                    {
                        objv.m_obj_value_id = refvId.ConvertTo<int>();
                    }

                    objv.MODIFIER_ID = this.logonId;
                    objv.MODIFY_DATETIME = DateTime.Now;
                }
                else
                {
                    if (refvId.ConvertTo<int>() != 0)
                    {
                        Ref_Obj_Mapped _objmapped = new Ref_Obj_Mapped();
                        _objmapped.obj_id = obj;
                        _objmapped.obj_value_id = ID;
                        _objmapped.m_obj_id = maprefobj;
                        _objmapped.m_obj_value_id = refvId.ConvertTo<int>();
                        _objmapped.CREATED_BY = this.logonId;
                        _objmapped.CREATE_DATETIME = DateTime.Now;
                        _objmapped.MODIFIER_ID = this.logonId;
                        _objmapped.MODIFY_DATETIME = DateTime.Now;
                        db.Ref_Obj_Mapped.Add(_objmapped);
                    }

                    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = ID, TBL_NAME = "Ref_Obj_Mapped", FIELD_NAME = "m_obj_value_id", OLD_VALUE = "", NEW_VALUE = ddlmapvalue.SelectedItem.Text, DESC = "Mapped object value from : " + ddlsource.SelectedItem.Text + " | " + ddlrefobj.SelectedItem.Text + " | " + strvalue + " to : " + ddlmapsource.SelectedItem.Text + " | " + ddlmaprefobj.SelectedItem.Text + " | " + ddlmapvalue.SelectedItem.Text, CREATOR_ID = this.logonId });
                }

                db.SaveChanges();
                AuditTrail oAudittrl = new AuditTrail();
                oAudittrl.AddAuditTrail(oAudit);
            }

            UserAlert1.ShowAlert("Mapping saved.", CommonEnum.AlertType.Information);
        }

        protected void ddlmaprefobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlattmap.Items.Clear();
            var _obj = ddlmaprefobj.SelectedValue.ConvertTo<int>();
            if (_obj > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    var obj = db.Ref_Obj_Att.Where(i => i.obj_id == _obj && i.att_data_mapped == "Y").ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.obj_attribute;
                            bindddlobject(ddlattmap, datatext, datavalue);
                        }
                        ddlattmap.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }

        protected void ddlrefobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlatt.Items.Clear();
            var _obj = ddlrefobj.SelectedValue.ConvertTo<int>();
            if (_obj > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    var obj = db.Ref_Obj_Att.Where(i => i.obj_id == _obj && i.att_data_mapped == "Y").ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.obj_attribute;
                            bindddlobject(ddlatt, datatext, datavalue);
                        }
                        ddlatt.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }

        protected void ddlattmap_SelectedIndexChanged(object sender, EventArgs e)
        {
            int obj = ddlrefobj.SelectedValue.ConvertTo<int>();
            int att = ddlatt.SelectedValue.ConvertTo<int>();
            var _refobj = ddlmaprefobj.SelectedValue.ConvertTo<int>();
            if (_refobj != 0 && obj != 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    //var objV = db.Ref_Obj_Value.Where(i => i.obj_id == obj).ToList();

                    var objV = (from od in db.Ref_Obj_Value
                                join pd in db.Ref_Obj_Att on od.obj_Att_id equals pd.ID
                                where pd.ID == att && od.obj_id == obj
                                select od
                         ).ToList();

                    gv.DataSource = objV;
                    gv.DataBind();
                    btnsave.Enabled = true;
                }
            }
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlsource.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            if (_proj > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    var obj = db.Ref_Source.Where(i => i.Proj_Id == _proj).ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.Source_Sys;
                            bindddlobject(ddlsource, datatext, datavalue);
                        }
                        ddlsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }

        protected void ddlmapproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlmapsource.Items.Clear();
            var _proj = ddlmapproj.SelectedValue.ConvertTo<int>();
            if (_proj > 0)
            {
                using (MDMEntities db = new MDMEntities())
                {
                    var obj = db.Ref_Source.Where(i => i.Proj_Id == _proj).ToList();

                    if (obj.Count > 0)
                    {
                        foreach (var item in obj)
                        {
                            var datavalue = item.ID.ToString();
                            var datatext = item.Source_Sys;
                            bindddlobject(ddlmapsource, datatext, datavalue);
                        }
                        ddlmapsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                    }
                }
            }
        }
    }
}