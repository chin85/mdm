﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RefValueMapping.aspx.cs" Inherits="DMPortal.RefValueMapping" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">

        function saveselector(obj) {
            if (confirm("Are you sure you want to save?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert ID="UserAlert1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Reference Mapping</h4>
            </div>
        </div>
    </div>
    <div class="container">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table runat="server" style="width: 100%" class="table table-hover table-bordered">
                    <tr class="panel-heading">
                        <td style="width: 50%; text-align: center" colspan="2">
                            <label class="control-label" for="textinput">To Map</label>
                        </td>
                        <td style="width: 50%; text-align: center" colspan="2">
                            <label class="control-label" for="textinput">Map To</label>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">Project:</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">Project:</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlmapproj" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlmapproj_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">Source:</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlsource" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlsource_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">Source:</label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlmapsource" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlmapsource_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">*Ref. Object:</label></td>
                        <td>
                            <asp:DropDownList ID="ddlrefobj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlrefobj_SelectedIndexChanged"></asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlrefobj" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">*Ref. Object:</label></td>
                        <td>
                            <asp:DropDownList ID="ddlmaprefobj" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlmaprefobj_SelectedIndexChanged"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlmaprefobj" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                      <tr>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">*Ref. Object Attribute:</label></td>
                        <td>
                            <asp:DropDownList ID="ddlatt" runat="server" CssClass="form-control"></asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="ddlatt" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>
                        </td>
                        <td style="width: 25%; text-align: right">
                            <label class="control-label" for="textinput">*Ref. Object Attribute:</label></td>
                        <td>
                            <asp:DropDownList ID="ddlattmap" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlattmap_SelectedIndexChanged" ></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlattmap" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
                <div class="col-md-12" style="text-align: right">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-success" OnClick="btnSearch_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <legend>Mapping Detail</legend>
                <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" EmptyDataText="No records." OnRowDataBound="gv_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="No">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="4%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Min Value">
                            <ItemTemplate>
                                <asp:Label ID="lblMin" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Max Value">
                            <ItemTemplate>
                                <asp:Label ID="lblMax" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblval" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblequal" runat="server" Text="="></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mapped To" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlmapvalue" runat="server" CssClass="form-control"></asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle Width="45%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div class="col-md-12" style="text-align: right">
                    <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" Enabled="false" OnClick="btnsave_Click" OnClientClick="return saveselector(this)" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnsave" />
                <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
