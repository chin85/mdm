﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditLog.aspx.cs" Inherits="DMPortal.AuditLog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" lang="javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd-mm-yy'
                //showOn: "button",
                //buttonImage: "Resources/calendar.jpg",
                //buttonImageOnly: true
            }).keyup(function (e) {
                if (e.keyCode == 8 || e.keyCode == 46) {
                    $.datepicker._clearDate(this);
                }
            });
        });

    </script>
    <script type="text/javascript">
        function PrintGridData() {
            var prtGrid = document.getElementById('<%=gv.ClientID %>');
            prtGrid.border = 2;
            var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=650,tollbar=0,scrollbars=1,status=0,resizable=1');
            prtwin.document.write(prtGrid.outerHTML);
            prtwin.document.close();
            prtwin.focus();
            prtwin.print();
            prtwin.close();
        }

    </script>
    <style type="text/css">
        /* This is the style for the trigger icon. The margin-bottom value causes the icon to shift down to center it. */
        .ui-datepicker-trigger {
            margin-left: 5px;
            margin-top: 8px;
            margin-bottom: -3px;
            width: 20px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Audit Log</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <table runat="server">


                    <tr>
                        <td class="col-md-4 control-label">Log Date: (From)</td>
                        <td colspan="5" class="col-md-8">
                            <input name="txttxnDate" runat="server" id="txttxnDate" style="height: 30px; width: 280px" class="date-picker input-md"
                                readonly="readonly" />
                        </td>
                        <td class="col-md-4 control-label">(To)</td>
                        <td>
                            <input name="txttxntoDate" runat="server" id="txttxntoDate" style="height: 30px; width: 280px" class="date-picker input-md"
                                readonly="readonly" /></td>
                    </tr>

                </table>
                <div class="col-md-12" style="text-align: right">
                    <asp:Label ID="Label1" runat="server" Text="" ForeColor="red" CssClass="control-label"></asp:Label>
                </div>

            </fieldset>
        </div>
        <div class="col-md-12" style="text-align: right">
            <asp:Button ID="btnSearch" runat="server" USeSubmitBehaviour="false" CausesValidation="false" Text="Search" CssClass="btn btn-success" OnClick="btnSearch_Click" />
        </div>
        <br />
        <div class="panel panel-primary">
            <div class="panel-heading" style="height: 40px">
                <div style="float: left">Audit Log Listings </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="printer" runat="server" style="float: right">
                            <img src="~/Resources/printer.ico" alt="Print" id="imgPrint" runat="server" width="25"
                                height="25" hspace="0" vspace="0" border="0" align="absmiddle" onclick="PrintGridData()"
                                style="cursor: pointer">
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" On
                            ="gv_RowDataBound" OnPageIndexChanging="gv_PageIndexChanging" EmptyDataText="No records.">
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="3%" />
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Log Date" DataField="CREATE_DATETIME" DataFormatString="{0:dd/MM/yyyy HH:MM:ss:sss}"></asp:BoundField>
                                <asp:BoundField HeaderText="User" DataField="CREATOR_ID"></asp:BoundField>
                                <asp:BoundField HeaderText="Old Value" DataField="OLD_VALUE"></asp:BoundField>
                                <asp:BoundField HeaderText="New Value" DataField="NEW_VALUE"></asp:BoundField>
                                <asp:BoundField HeaderText="Action Desc." DataField="DESC"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <br />
    </div>

</asp:Content>
