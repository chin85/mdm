﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class EditPasword : System.Web.UI.Page
    {
        public string logonId { get; set; }
        public int ID { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;
            this.ID = oDOSession.Id;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            EncryptionManager encrypt = new EncryptionManager();
            string newPassword = encrypt.EncryptSHA1(txtPassword.Text);
            using (MDMEntities context = new MDMEntities())
            {
                var oUser = context.USERs.Where(i => i.ID == this.ID).SingleOrDefault();

                if (oUser != null)
                {
                    if (newPassword == oUser.LOGON_PASSWORD)
                    {
                        UserAlert.ShowAlert("Current Password and New Password must not be equal.", CommonEnum.AlertType.Exclamation);
                        return;
                    }
                    else
                    {
                        oUser.LOGON_PASSWORD = newPassword;
                        oUser.MODIFIER_ID = this.logonId;
                        oUser.MODIFY_DATETIME = DateTime.Now;
                    }
                    context.SaveChanges();
                    UserAlert.ShowAlert("Password has been changed successfully.", CommonEnum.AlertType.Exclamation);
                }
            }
        }


        protected void btncancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("RefObj.aspx");
        }
    }
}