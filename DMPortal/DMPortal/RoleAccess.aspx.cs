﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using DMPortal.Object;
using System.Data.Entity.Validation;

namespace DMPortal
{
    public partial class RoleAccess : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageIndex"] = 0;
                bindDLL();
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {
                var role = db.TBLROLES.ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.rolename;
                        bindddlobject(ddlrole, datatext, datavalue);
                    }
                    ddlrole.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }

        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        private void bindgv(int roleid)
        {
            using (MDMEntities db = new MDMEntities())
            {

                var roleaccess = (from pd in db.ROLE_ACCESS
                                  join od in db.SCREEN_MAINTENANCE on pd.SCREEN_ID equals od.ID
                                  where pd.ROLE_ID == roleid
                                  orderby od.SCREEN_NAME
                                  select new
                                  {
                                      pd.ID,
                                      pd.ACCESS,
                                      od.SCREEN_NAME,
                                  }).ToList();

                //var roleaccess = (from pd in db.ROLE_ACCESS
                //                  join od in db.SCREEN_MAINTENANCE on pd.SCREEN_ID equals od.ID into screen
                //                  from x in screen.DefaultIfEmpty()
                //                  where pd.ROLE_ID == roleid
                //                  select new
                //                  {
                //                       pd.ID,
                //                       ACCESS = pd.ACCESS == null ? "N" : pd.ACCESS,
                //                       x.SCREEN_NAME,
                //                  }).ToList();


                if (roleaccess.Count > 0)
                {
                    gv.DataSource = roleaccess;
                    gv.DataBind();
                }
            }
        }

        protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
        {
            var _role = ddlrole.SelectedValue.ConvertTo<int>();
            if (_role != 0)
            {
                bindgv(_role);
            }
            else
            {
                gv.DataSource = null;
                gv.DataBind();

            }


        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RadioButtonList rbl = e.Row.FindControl("rbl") as RadioButtonList;
                var ac = DataBinder.Eval(e.Row.DataItem, "ACCESS").ToString();

                if (rbl != null)
                {
                    rbl.SelectedValue = ac;
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //try
            //{

                List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
                foreach (GridViewRow row in gv.Rows)
                {
                    int id = (int)gv.DataKeys[row.RowIndex].Values[0];
                    RadioButtonList cb = (RadioButtonList)row.Cells[0].FindControl("rbl");
                    using (MDMEntities db = new MDMEntities())
                    {
                        var roleaccess = db.ROLE_ACCESS.Where(i => i.ID == id).SingleOrDefault();

                        if (roleaccess != null)
                        {
                            if (roleaccess.ACCESS != cb.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "ROLE_ACCESS", FIELD_NAME = "ACCESS", OLD_VALUE = roleaccess.ACCESS, NEW_VALUE = cb.SelectedValue, DESC = "Role acess for screen id " + roleaccess.SCREEN_ID + " has been modified for role id " + roleaccess.ROLE_ID, CREATOR_ID = this.logonId });

                            roleaccess.ACCESS = cb.SelectedValue;
                            roleaccess.MODIFIER_ID = this.logonId;
                            roleaccess.MODIFIED_DATETIME = DateTime.Now;
                        }
                        db.SaveChanges();
                    }
                }

                AuditTrail oAudittrl = new AuditTrail();
                oAudittrl.AddAuditTrail(oAudit);

                UserAlert.ShowAlert("Role access save.", CommonEnum.AlertType.Information);
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}

        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            gv.PageIndex = e.NewPageIndex;
            var _role = ddlrole.SelectedValue.ConvertTo<int>();
            if (_role != 0)
            {
                bindgv(_role);
            }
            else
            {
                gv.DataSource = null;
                gv.DataBind();

            }
        }
    }
}