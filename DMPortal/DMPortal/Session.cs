﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal
{
    public class SessionInfo
    {
        public string LogonId { get; set; }
        public int Id { get; set; }
        public int UserRoleId { get; set; }
        public int LogId { get; set; }
        public string reportType { get; set; }
        public string isChecker { get; set; }
        public string isMaker { get; set; }
    }

    public class SessionRole
    {
        public bool showRefObject { get; set; }
        public bool showRefObjectV { get; set; }
        public bool showRoleManagement { get; set; }
        public bool showUserManagement { get; set; }
        public bool showRoleAccess { get; set; }
        public bool showRefObjectVMapped { get; set; }
        public bool showRefObjectAtt { get; set; }
        public bool showRefProj { get; set; }
        public bool showRefSource { get; set; }
        public bool showObjView { get; set; }
        public bool showObjImport { get; set; }
        public bool showMergeSplit { get; set; }
        public bool showAuditLog { get; set; }
        public bool showChecker { get; set; }
        public bool showMaker { get; set; }

    }
    public class SessionFlag
    {
        public int InsertFlag { get; set; }
    }
}