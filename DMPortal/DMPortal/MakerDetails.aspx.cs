﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class MakerDetails : System.Web.UI.Page
    {
        public int objid { get; set; }
        public string logonId { get; set; }
        public int userId { get; set; }
        static string prevPage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            var encrypt = new EncryptionManager();
            this.objid = string.IsNullOrEmpty(Request.QueryString["d0"]) ? 0 : encrypt.Decrypt(Request.QueryString["d0"].Replace(" ", "+")).ConvertTo<int>();

            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;
            this.userId = oDOSession.Id;

            if (!Page.IsPostBack)
            {
                Uri url = new Uri(Request.ServerVariables["HTTP_REFERER"]);
                prevPage = String.Format("{0}{1}{2}{3}", url.Scheme,
                   Uri.SchemeDelimiter, url.Authority, url.AbsolutePath);
                ViewState["PageIndex"] = 0;
                ViewState["DetailPageIndex"] = 0;
                bindgvdetail(objid);
            }
        }
        private void bindgvdetail(int id)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetMakerObjView", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = id;
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "Obj");
                gvdetail.DataSource = ds.Tables["Obj"];
                gvdetail.DataBind();
                con.Close();
            }
        }

        protected void gvdetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["DetailPageIndex"] = e.NewPageIndex;
            this.gvdetail.PageIndex = e.NewPageIndex;
            this.bindgvdetail(this.objid);

        }

        protected void gvdetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["DetailPageIndex"].ConvertTo<int>();
            int pagesize = gvdetail.PageSize;

            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[2].Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[2].Visible = false;
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();

            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            var encrypt = new EncryptionManager();
            string url = prevPage + "?d0=" + encrypt.Encrypt(this.objid.ToString()); //for .net 4.5
            Response.Redirect(url);
        }
    }
}