﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using DMPortal.Object;
using System.Text;
using DMPortal.Object;
using System.Web.UI.HtmlControls;

namespace DMPortal
{
    public partial class UserManagement : System.Web.UI.Page
    {
        public string logonId { get; set; }
        public string complexitycheck = "N";
        public int min_length = 8;

        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                ViewState["filrole"] = -1;
                ViewState["filstatus"] = string.Empty;
                ViewState["filuname"] = string.Empty;
                BindDLL();
                bindgv();
            }

            using (MDMEntities context = new MDMEntities())
            {
                var passwordcomplexity = context.SETUP_SETTING.Where(i => i.PARAM_NAME == "PASSWORD_COMPLEXITY_REQUIREMENT").SingleOrDefault();
                if (passwordcomplexity != null)
                {
                    min_length = passwordcomplexity.PARAM_VALUE.ConvertTo<int>();
                    complexitycheck = passwordcomplexity.SUB_VALUE;
                }
            }
        }

        protected void btn_register_Click(object sender, EventArgs e)
        {
            EncryptionManager encrypt = new EncryptionManager();
            PasswordManager passwordgen = new PasswordManager();
            USER _user = new USER();
            PASSWORD_LOG _passwordlog = new PASSWORD_LOG();
            List<USER_LOG> _userlog = new List<USER_LOG>();
            var password = "";
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;

            if (Page.IsValid)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        var oUser = context.USERs.Where(i => i.LOGON_ID == txtID.Value).SingleOrDefault();

                        if (oUser != null)
                        {
                            lbl_warning.Visible = true;
                            lbl_warning.Text = "Same logon ID existed, Please verify that the ID given is unique.";
                            return;
                        }
                        else
                        {
                            password = passwordgen.GENERATE_PASSWORD(min_length);
                            _user.LOGON_ID = txtID.Value;
                            _user.USER_FULL_NM = txtname.Text;
                            _user.LOGON_PASSWORD = encrypt.EncryptSHA1(password);//encrypt.EncryptMD5(password);
                            _user.LOGON_EMAIL = txtmail.Text;
                            _user.ROLE = ddl.SelectedValue.ConvertTo<int>();
                            _user.ACCOUNT_STATUS = ddluserstatus.SelectedValue;
                            _user.ACCOUNT_VALITITY = ddlaccValidity.SelectedValue.ConvertTo<int>();
                            _user.LOGON_ATTEMPT = 0;
                            _user.LOGON_COUNT = 0;
                            _user.PASSWORD_UPDATE_DATETIME = DateTime.Now;
                            _user.ACCOUNT_VALIDATE_DATETIME = DateTime.Now;
                            _user.FIRST_LOGON_FLAG = CommonEnum.statusEnum.Y.GetDescription();
                            _user.LOGON_STATUS = CommonEnum.LogonStatus.Off.GetDescription();
                            _user.NODE_ID = "";
                            _user.CHECKER_FG = rbl.SelectedValue;
                            _user.MAKER_FG = rblm.SelectedValue;
                            _user.CREATOR_ID = this.logonId;
                            _user.CREATE_DATETIME = DateTime.Now;
                            _user.MODIFIER_ID = this.logonId;
                            _user.MODIFY_DATETIME = DateTime.Now;
                            context.USERs.Add(_user);

                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var user = context.USERs.Where(i => i.ID == id).SingleOrDefault();

                        if (user != null)
                        {
                            if (user.LOGON_ID != txtID.Value)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "LOGON_ID", OLD_VALUE = user.LOGON_ID, NEW_VALUE = txtID.Value, DESC = "User login Id has been modified", CREATOR_ID = this.logonId });
                            if (user.USER_FULL_NM != txtname.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "USER_FULL_NM", OLD_VALUE = user.USER_FULL_NM, NEW_VALUE = txtname.Text, DESC = "User full name has been modified", CREATOR_ID = this.logonId });
                            if (user.LOGON_EMAIL != txtmail.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "LOGON_EMAIL", OLD_VALUE = user.LOGON_EMAIL, NEW_VALUE = txtmail.Text, DESC = "User email has been modified", CREATOR_ID = this.logonId });
                            if (user.ROLE.ToString() != ddl.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "ROLE", OLD_VALUE = user.ROLE.ToString(), NEW_VALUE = ddl.SelectedValue, DESC = "User role has been modified", CREATOR_ID = this.logonId });
                            if (user.ACCOUNT_STATUS != ddluserstatus.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "ACCOUNT_STATUS", OLD_VALUE = user.ACCOUNT_STATUS, NEW_VALUE = ddluserstatus.SelectedValue, DESC = "User account status has been modified", CREATOR_ID = this.logonId });
                            if (user.ACCOUNT_VALITITY.ToString() != ddlaccValidity.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "ACCOUNT_VALITITY", OLD_VALUE = user.ACCOUNT_VALITITY.ToString(), NEW_VALUE = ddlaccValidity.SelectedValue, DESC = "User acount validity period has been modified", CREATOR_ID = this.logonId });
                            if (user.LOGON_STATUS != ddlaccValidity.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "LOGON_STATUS", OLD_VALUE = user.LOGON_STATUS, NEW_VALUE = ddlaccValidity.SelectedValue, DESC = "User logon status has been modified", CREATOR_ID = this.logonId });
                            if (user.CHECKER_FG != rbl.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "CHECKER_FG", OLD_VALUE = user.CHECKER_FG, NEW_VALUE = rbl.SelectedValue, DESC = "Checker fg has been modified", CREATOR_ID = this.logonId });
                            if (user.MAKER_FG != rblm.SelectedValue)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "USER", FIELD_NAME = "MAKER_FG", OLD_VALUE = user.MAKER_FG, NEW_VALUE = rblm.SelectedValue, DESC = "Maker has been modified", CREATOR_ID = this.logonId });

                            user.LOGON_ID = txtID.Value;
                            user.USER_FULL_NM = txtname.Text;
                            user.LOGON_EMAIL = txtmail.Text;
                            user.ROLE = ddl.SelectedValue.ConvertTo<int>();
                            user.ACCOUNT_STATUS = ddluserstatus.SelectedValue;
                            user.ACCOUNT_VALITITY = ddlaccValidity.SelectedValue.ConvertTo<int>();
                            user.LOGON_STATUS = ddllogon.SelectedValue;
                            user.CHECKER_FG = rbl.SelectedValue;
                            user.MAKER_FG = rblm.SelectedValue;
                            user.MODIFIER_ID = this.logonId;
                            user.MODIFY_DATETIME = DateTime.Now;

                        }
                    }

                    context.SaveChanges();
                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);

                    pkid = _user.ID.ConvertTo<int>();

                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "USER", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = UserLogEnum.LogDescEnum.user_account_create.GetDescription(), CREATOR_ID = this.logonId });
                        AuditTrail Audit = new AuditTrail();
                        Audit.AddAuditTrail(oAudit);

                        _passwordlog.USER_ID = _user.ID.ConvertTo<int>();
                        _passwordlog.PASSWORD = encrypt.EncryptMD5(password);
                        _passwordlog.CREATOR_ID = this.logonId;
                        _passwordlog.CREATE_DATETIME = DateTime.Now;
                        context.PASSWORD_LOG.Add(_passwordlog);
                        context.SaveChanges();

                        SendPassword(password);
                    }
                }

                bindgv();
                lbl_warning.Text = "Data save.";
                lbl_warning.ForeColor = System.Drawing.Color.Green;
                txtID.Value = string.Empty;
                txtname.Text = string.Empty;
                txtmail.Text = string.Empty;
                ddl.SelectedValue = "-1";
                ddluserstatus.SelectedValue = "A";
                ddlaccValidity.SelectedValue = "30";
                ddllogon.SelectedValue = "OFF";
                ViewState["PageMode"] = "A";
                rbl.SelectedValue = "N";
                rblm.SelectedValue = "N";
            }
        }

        private void bindgv()
        {
            var active = CommonEnum.statusEnum.Active.GetDescription();
            string status = ViewState["filstatus"].ToString();
            decimal rolename = ViewState["filrole"].ConvertTo<decimal>();
            string username = ViewState["filuname"].ToString();

            if (ddlStatus.SelectedValue == "INACTIVE")
                status = "I";
            else if (ddlStatus.SelectedValue == "ACTIVE")
                status = "A";
            else
                status = "";


            using (MDMEntities context = new MDMEntities())
            {
                //IQueryable<USER> oUsers = context.USERs.WhereLike(u => u.LOGON_ID, username, '%');

                if (string.IsNullOrEmpty(username) && rolename == -1 && string.IsNullOrEmpty(status))
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }


                else if (!string.IsNullOrEmpty(username) && rolename > 0 && !string.IsNullOrEmpty(status))
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ACCOUNT_STATUS == status && pd.ROLE == rolename
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).Where(i => i.LOGON_ID.Contains(username)).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }

                else if (!string.IsNullOrEmpty(username) && rolename > 0 && string.IsNullOrEmpty(status))
                {

                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ROLE == rolename
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).Where(i => i.LOGON_ID.Contains(username)).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }


                else if (string.IsNullOrEmpty(username) && rolename > 0 && !string.IsNullOrEmpty(status))
                {

                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ACCOUNT_STATUS == status && pd.ROLE == rolename
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }

                else if (!string.IsNullOrEmpty(username) && rolename == -1 && !string.IsNullOrEmpty(status))
                {

                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ACCOUNT_STATUS == status
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).Where(i => i.LOGON_ID.Contains(username)).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }

                else if (!string.IsNullOrEmpty(status))
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ACCOUNT_STATUS == status
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }
                else if (!string.IsNullOrEmpty(username))
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).Where(i => i.LOGON_ID.Contains(username)).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }
                else
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ROLE == rolename
                                orderby pd.LOGON_ID
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.ACCOUNT_STATUS,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).ToList();

                    if (user.Count < 0)
                    {
                        lblerrormsg.Visible = true;
                    }
                    else
                    {
                        btndelete.Visible = true;
                        gv.DataSource = user;
                        gv.DataBind();
                    }
                }
            }
        }

        public void SendPassword(string password)
        {
            using (MDMEntities context = new MDMEntities())
            {
                var mail_server_setup = context.MAIL_SERVER_SETTING.SingleOrDefault();

                EmailManager oEmailer = new EmailManager();
                StringBuilder Body = new StringBuilder();
                string msg = " Welcome to MDM Portal. Kindly refer your login credential at below and change your password accordingly after first login.";
                Body = new StringBuilder();
                Body.Append("DateTime: " + DateTime.Now.ToString() + "<br>");
                Body.Append("Message: " + msg + "<br>");
                Body.Append("Logon Credential:- <br>");
                Body.Append("LoginID: " + txtID.Value + "<br>");
                Body.Append("Password: " + password + "<br>");

                oEmailer.ToAdd = txtmail.Text;
                oEmailer.From = "support@sturngroup.com";
                oEmailer.Subject = "New user added on " + DateTime.Now.ToString();
                oEmailer.Body = Body.ToString();
                oEmailer.BodyType = EmailManager.oBodyType.HTML;
                if (mail_server_setup != null)
                {
                    oEmailer.From = mail_server_setup.SMTP_CREDENTIAL;
                    oEmailer.SMTPServerName = mail_server_setup.SMTP_SERVER_NAME;
                    oEmailer.SMTPServerPort = mail_server_setup.SMTP_SERVER_PORT.ConvertTo<int>();
                    oEmailer.SMTPSSL = mail_server_setup.SMTP_SSL == CommonEnum.statusEnum.Y.GetDescription() ? true : false;
                    oEmailer.SMTPCredential = mail_server_setup.SMTP_CREDENTIAL;
                    oEmailer.SMTPPassword = mail_server_setup.SMTP_PASSWORD;
                }
                oEmailer.SendMail();
            }
        }

        private void BindDLL()
        {
            /*nat*/
            using (MDMEntities context = new MDMEntities())
            {
                var orole = (from c in context.TBLROLES
                             select new { c.ID, CODEWDESC = c.rolename }).OrderBy(i => i.CODEWDESC).Distinct().ToList();


                if (orole.Count > 0)
                {
                    ddl.DataTextField = "CODEWDESC";
                    ddl.DataValueField = "ID";
                    ddl.DataSource = orole;
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("SELECT", "-1"));

                    ddlRole.DataTextField = "CODEWDESC";
                    ddlRole.DataValueField = "ID";
                    ddlRole.DataSource = orole;
                    ddlRole.DataBind();
                    ddlRole.Items.Insert(0, new ListItem("SELECT", "-1"));
                }
            }
        }

        protected void btnSearchF_Click(object sender, EventArgs e)
        {
            ViewState["filrole"] = Convert.ToDecimal(ddlRole.SelectedValue.ToString());
            ViewState["filstatus"] = ddlStatus.SelectedValue.ToString();
            ViewState["filuname"] = txtusername.Text;
            bindgv();
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            lbl_warning.Text = string.Empty;
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var user = (from pd in context.USERs
                                join od in context.TBLROLES on pd.ROLE equals od.ID
                                where pd.ID == id
                                select new
                                {
                                    pd.ID,
                                    pd.LOGON_ID,
                                    pd.USER_FULL_NM,
                                    pd.LOGON_EMAIL,
                                    pd.ACCOUNT_STATUS,
                                    pd.ACCOUNT_VALITITY,
                                    od.rolename,
                                    pd.ROLE,
                                    pd.LOGON_STATUS,
                                    pd.MAKER_FG,
                                    pd.CHECKER_FG,
                                }).SingleOrDefault();

                    if (user != null)
                    {
                        txtID.Value = user.LOGON_ID;
                        txtname.Text = user.USER_FULL_NM;
                        txtmail.Text = user.LOGON_EMAIL;
                        ddl.SelectedValue = user.ROLE.ToString();
                        ddluserstatus.SelectedValue = user.ACCOUNT_STATUS;
                        ddlaccValidity.SelectedValue = user.ACCOUNT_VALITITY.ToString();
                        ddllogon.SelectedValue = user.LOGON_STATUS;
                        rbl.SelectedValue = user.CHECKER_FG;
                        rblm.SelectedValue = user.MAKER_FG;
                    }
                }
            }
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtID.Value = string.Empty;
            txtname.Text = string.Empty;
            txtmail.Text = string.Empty;
            ddl.SelectedValue = "-1";
            ddluserstatus.SelectedValue = "A";
            ddlaccValidity.SelectedValue = "30";
            ddllogon.SelectedValue = "OFF";
            ViewState["PageMode"] = "A";
            rbl.SelectedValue = "N";
            rblm.SelectedValue = "N";
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        USER _user = context.USERs.SingleOrDefault(i => i.ID == id);

                        if (_user != null)
                        {
                            context.USERs.Remove(_user);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "User", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "User " + _user.LOGON_ID + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv();
        }

    }
}