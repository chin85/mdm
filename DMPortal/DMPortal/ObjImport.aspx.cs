﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class ObjImport : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                BindDDL();
            }
        }

        protected void BindDDL()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand sql = new SqlCommand("select name from sys.sysdatabases where name not in ('master', 'model', 'msdb', 'tempdb')", con);
                using (SqlDataReader dr = sql.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ExtDb.Items.Add(dr.GetString(0));
                    }
                }
            }

            using (MDMEntities db = new MDMEntities())
            {

                var oproj = (from c in db.Ref_Proj
                             select new { c.ID, objDESC = c.Project + " - " + c.Description }).OrderBy(i => i.objDESC).ToList();

                if (oproj.Count > 0)
                {
                    foreach (var item in oproj)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void Bindextcol()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                string sql = "SELECT COLUMN_NAME [Column], DATA_TYPE [Type], IS_NULLABLE [Nullable] FROM @db.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '@tbl'";
                sql = sql.Replace("@db", ExtDb.SelectedValue.ToString()).Replace("@tbl", ExtTbl.SelectedValue.ToString());
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "Columns");
                extcol.DataSource = ds.Tables["Columns"];
                extcol.DataBind();
            }
        }

        protected void ExtDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExtTbl.Items.Clear();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                string sql = "SELECT DISTINCT TABLE_NAME FROM " + "@db" + ".INFORMATION_SCHEMA.COLUMNS ORDER BY TABLE_NAME";
                sql = sql.Replace("@db", ExtDb.SelectedValue.ToString());
                SqlCommand cmd = new SqlCommand(sql, con);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ExtTbl.Items.Add(dr.GetString(0));
                    }
                }
            }

            ExtTbl.Items.Insert(0, new ListItem("- SELECT -", "0"));
        }

        protected void ExtTbl_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bindextcol();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            int sourceid = ddlsource.SelectedValue.ConvertTo<int>();

            using (MDMEntities context = new MDMEntities())
            {
                var role = context.Ref_Obj.Where(i => i.obj == txtobj.Text && i.Source_Sys == sourceid && i.Proj_id == projid).SingleOrDefault();

                if (role != null)
                {
                    if (ddltype.SelectedValue == "S")
                    {
                        UserAlert.ShowAlert("Object exist for same source.", CommonEnum.AlertType.Exclamation);
                        return;
                    }
                }
                else
                {
                    if (ddltype.SelectedValue == "D")
                    {
                        UserAlert.ShowAlert("Object does not exist. Kindly please import with Schema and Data.", CommonEnum.AlertType.Exclamation);
                        return;
                    }
                }
            }

            int col = 0;
            List<string> sel = new List<string>();
            foreach (GridViewRow row in extcol.Rows)
            {
                CheckBox chkall = (CheckBox)extcol.HeaderRow.FindControl("ChkAll");

                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;

                if (ctl.Checked)
                {
                    sel.Add(ctl.Value.ToString());
                    col += 1;
                }
            }
            string cols = string.Join(",", sel.ToArray());

            //if (ddltype.SelectedValue == "D" && col == 0)
            //{
            //    string sResult = "FALSE";
            //    DataSet ds = new DataSet();
            //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            //    {
            //        con.Open();
            //        SqlCommand cmd = new SqlCommand("CheckSvrColVsMDMCol", con);
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.Add("@obj_id", SqlDbType.Int).Value = ddlobj.SelectedValue.ConvertTo<int>();
            //        cmd.Parameters.Add("@db", SqlDbType.VarChar, 128).Value = ExtDb.SelectedValue.ToString();
            //        cmd.Parameters.Add("@tbl", SqlDbType.VarChar, 128).Value = ExtTbl.SelectedValue.ToString();
            //        SqlDataAdapter sql = new SqlDataAdapter(cmd);
            //        sql.Fill(ds, "Obj");
            //        con.Close();
            //    }

            //    DataTable dt = ds.Tables["Obj"];

            //    if (ds.Tables["Obj"].Rows.Count > 0)
            //        sResult = ds.Tables["Obj"].Rows[0]["ISCOLMATCH"].ToString();

            //    if (sResult == "FALSE")
            //    {
            //        UserAlert.ShowAlert("Source table column doesn't match with MDM table column. Kindly please import with Schema and Data.", CommonEnum.AlertType.Exclamation);
            //        return;
            //    }
            //}

            if (ddltype.SelectedValue == "S")
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("ImportObj", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@projid", SqlDbType.Int).Value = ddlproj.SelectedValue.ConvertTo<int>();
                    cmd.Parameters.Add("@db", SqlDbType.VarChar, 128).Value = ExtDb.SelectedValue.ToString();
                    cmd.Parameters.Add("@tbl", SqlDbType.VarChar, 128).Value = ExtTbl.SelectedValue.ToString();
                    cmd.Parameters.Add("@obj", SqlDbType.VarChar, 50).Value = txtobj.Text.ToString();
                    cmd.Parameters.Add("@constraint", SqlDbType.VarChar, 4000).Value = col.ToString();
                    cmd.Parameters.Add("@coll", SqlDbType.VarChar, 4000).Value = cols;
                    cmd.Parameters.Add("@sys", SqlDbType.Int).Value = ddlsource.SelectedValue.ConvertTo<int>(); //ExtDb.SelectedValue.ToString();
                    cmd.Parameters.Add("@login", SqlDbType.VarChar, 50).Value = this.logonId;
                    SqlDataAdapter sql = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sql.Fill(ds, "Obj");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", "alert('Table Imported Successfully')", true);
                    con.Close();
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("ImportData", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@obj_id", SqlDbType.Int).Value = ddlobj.SelectedValue.ConvertTo<int>();
                    cmd.Parameters.Add("@db", SqlDbType.VarChar, 128).Value = ExtDb.SelectedValue.ToString();
                    cmd.Parameters.Add("@tbl", SqlDbType.VarChar, 128).Value = ExtTbl.SelectedValue.ToString();
                    cmd.Parameters.Add("@constraint", SqlDbType.VarChar, 4000).Value = col.ToString();
                    cmd.Parameters.Add("@coll", SqlDbType.VarChar, 4000).Value = cols;
                    cmd.Parameters.Add("@ImportOpt", SqlDbType.VarChar, 1).Value = ddldataopt.SelectedValue.ToString();
                    SqlDataAdapter sql = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sql.Fill(ds, "Obj");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", "alert('Data Imported Successfully')", true);
                    con.Close();
                }
            }
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlsource.Items.Clear();
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var osource = (from c in db.Ref_Source
                               where c.Proj_Id == projid
                               select new { c.ID, objDESC = c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                if (osource.Count > 0)
                {
                    foreach (var item in osource)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlsource, datatext, datavalue);
                    }
                    ddlsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobj.SelectedValue = "0";
            ddldataopt.SelectedValue = "0";
            if (ddltype.SelectedValue != "0")
            {
                if (ddltype.SelectedValue == "D")
                {
                    ddldataopt.Enabled = true;
                    ddlobj.Enabled = true;
                    txtobj.Enabled = false;
                }
                else
                {
                    txtobj.Text = ExtTbl.SelectedValue.ToString();
                    ddldataopt.Enabled = false;
                    ddlobj.Enabled = false;
                    txtobj.Enabled = true;
                }
            }
        }

        protected void ddlsource_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobj.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {
                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void ddlobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlobj.SelectedValue != "0")
            {
                txtobj.Text = ddlobj.SelectedItem.ToString();
            }
        }
    }
}