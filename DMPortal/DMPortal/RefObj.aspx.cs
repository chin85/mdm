﻿using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using System.Web.UI.HtmlControls;
using System.Data.Entity.Validation;
using System.Configuration;
using System.Data;

namespace DMPortal
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //logonId = "test";
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                ViewState["obj"] = string.Empty;
                ViewState["source"] = string.Empty;
                //btndelete.Enabled = false;
                bindgv();
                bindDLL();
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {

                var oproj = (from c in db.Ref_Proj
                             select new { c.ID, objDESC = c.Project + " - " + c.Description }).OrderBy(i => i.objDESC).ToList();

                if (oproj.Count > 0)
                {
                    foreach (var item in oproj)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        private void bindgv()
        {
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            string obj = ViewState["obj"].ToString();
            string source = ViewState["source"].ToString();

            using (MDMEntities db = new MDMEntities())
            {
                var oObj = (from pd in db.Ref_Obj
                            join od in db.Ref_Source on pd.Source_Sys equals od.ID
                            where pd.Proj_id == projid
                            orderby od.Source_Sys
                            select new
                            {
                                pd.ID,
                                od.Source_Sys,
                                pd.obj,
                                pd.MODIFY_DATETIME
                            }).ToList();

                if (!string.IsNullOrEmpty(source) && source != "- SELECT -")
                    oObj = oObj.Where(x => x.Source_Sys == source).ToList();
                if (!string.IsNullOrEmpty(obj))
                    oObj = oObj.Where(y => y.obj.ToLower().Contains(obj.ToLower())).ToList();

                gv.DataSource = oObj;
                gv.DataBind();
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            //try
            //{
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            int sourceid = ddlsource.SelectedValue.ConvertTo<int>();
            if (txtr1.Text != null)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        var role = context.Ref_Obj.Where(i => i.obj == txtr1.Text && i.Source_Sys == sourceid && i.Proj_id == projid).SingleOrDefault();
                        if (role != null)
                        {
                            UserAlert.ShowAlert("Object exist for same source.", CommonEnum.AlertType.Exclamation);
                            return;

                        }
                        else
                        {
                            Ref_Obj srefobj = new Ref_Obj();
                            srefobj.obj = txtr1.Text;
                            srefobj.Source_Sys = sourceid;
                            srefobj.Proj_id = projid;
                            srefobj.CREATED_BY = this.logonId;
                            srefobj.CREATE_DATETIME = DateTime.Now;
                            srefobj.MODIFIER_ID = this.logonId;
                            srefobj.MODIFY_DATETIME = DateTime.Now;
                            context.Ref_Obj.Add(srefobj);
                            context.SaveChanges();
                            pkid = (int)srefobj.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Obj", FIELD_NAME = "Obj", OLD_VALUE = "", NEW_VALUE = txtr1.Text, DESC = "New obj name added.", CREATOR_ID = this.logonId });
                            //oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Obj", FIELD_NAME = "Source_Sys", OLD_VALUE = "", NEW_VALUE = ddlsource.SelectedItem.Text, DESC = "New obj source system added.", CREATOR_ID = this.logonId });
                        }
                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var role = context.Ref_Obj.Where(i => i.ID == id).SingleOrDefault();

                        if (role != null)
                        {
                            if (role.obj != txtr1.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj", FIELD_NAME = "Obj", OLD_VALUE = role.obj, NEW_VALUE = txtr1.Text, DESC = "Ref. obj name has been modified", CREATOR_ID = this.logonId });
                            if (role.Source_Sys != sourceid)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj", FIELD_NAME = "Source_Sys", OLD_VALUE = role.Source_Sys.ToString(), NEW_VALUE = sourceid.ToString(), DESC = "Ref. obj source system has been modified", CREATOR_ID = this.logonId });

                            role.obj = txtr1.Text;
                            role.Source_Sys = sourceid;
                            role.Proj_id = projid;
                            role.MODIFIER_ID = this.logonId;
                            role.MODIFY_DATETIME = DateTime.Now;
                        }
                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }

            }
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var eve in ex.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }
            //    }
            //    throw;
            //}

            bindgv();
            txtr1.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        Ref_Obj _role = context.Ref_Obj.SingleOrDefault(i => i.ID == id);

                        if (_role != null)
                        {
                            //context.Ref_Obj.Remove(_role);
                            //context.SaveChanges();

                            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                            {
                                con.Open();
                                SqlCommand cmd = new SqlCommand("DeleteObj", con);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = id;
                                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                                DataSet ds = new DataSet();
                                sql.Fill(ds, "Obj");
                                con.Close();
                            }

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj " + _role.obj + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv();
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var role = context.Ref_Obj.Where(i => i.ID == id).SingleOrDefault();

                    if (role != null)
                    {
                        txtr1.Text = role.obj;
                    }
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv();
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtr1.Text = "";
            ViewState["PageMode"] = "A";
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlsource.Items.Clear();
            int projid = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var osource = (from c in db.Ref_Source
                               where c.Proj_Id == projid
                               select new { c.ID, objDESC = c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                if (osource.Count > 0)
                {
                    foreach (var item in osource)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlsource, datatext, datavalue);
                    }
                    ddlsource.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            ViewState["obj"] = txtobj.Text;
            ViewState["source"] = ddlsource.SelectedItem.Text;
            bindgv();
        }
    }
}