﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Manager;
using System.Web.UI.HtmlControls;
using DMPortal.Object;

namespace DMPortal
{
    public partial class RefInfo : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                bindDLL();
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void ddlobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddltype.Items.Clear();
            txtitem.Text = string.Empty;
            txtvalue.Text = string.Empty;
            txtdesc.Text = string.Empty;
            var _obj = ddlobj.SelectedValue.ConvertTo<int>();

            using (MDMEntities db = new MDMEntities())
            {
                var objvalue = (from pd in db.Ref_Obj_Value
                                join od in db.Ref_Obj on pd.obj_id equals od.ID
                                where pd.obj_id == _obj
                                orderby pd.obj_value
                                select new
                                {
                                    pd.ID,
                                    pd.obj_value,
                                    //pd.obj_value_desc,
                                    pd.MODIFY_DATETIME,
                                    //pd.obj_type,
                                    //pd.min,
                                    //pd.max,
                                    od.obj,
                                }).ToList();


                if (objvalue.Count > 0)
                {
                    foreach (var item in objvalue)
                    {
                        var datavalue = item.ID.ToString();
                        var datatextlst = item.obj_value; //+ ":" + item.obj_value_desc;
                        //var datatextrng = item.min + " - " + item.max; // + " : " + item.obj_value_desc;


                        //if (item.obj_type == "LIST")
                            bindddlobject(ddltype, datatextlst, datavalue);
                        //else
                        //    bindddlobject(ddltype, datatextrng, datavalue);
                    }
                    ddltype.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void ddltype_SelectedIndexChanged(object sender, EventArgs e)
        {

            var _obj = ddltype.SelectedValue.ConvertTo<int>();
            if (_obj != 0)
            {
                bindgv(_obj);
            }
            else
            {
                gv.DataSource = null;
                gv.DataBind();
            }
        }

        private void bindgv(int objid)
        {
            using (MDMEntities db = new MDMEntities())
            {
                var objvalue = (from pd in db.Ref_Obj_Info
                                join od in db.Ref_Obj_Value on pd.obj_value_id equals od.ID
                                where pd.obj_value_id == objid
                                orderby pd.info_item
                                select new
                                {
                                    pd.ID,
                                    pd.info_item,
                                    pd.info_desc,
                                    pd.MODIFY_DATETIME,
                                    pd.info_value,
                                    //od.min,
                                    //od.max,
                                    od.obj_value,
                                    //od.obj_value_desc,
                                    //od.obj_type,
                                }).ToList();

                gv.DataSource = objvalue;
                gv.DataBind();
            }
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;
                using (MDMEntities context = new MDMEntities())
                {
                    var oref = context.Ref_Obj_Info.Where(i => i.ID == id).SingleOrDefault();

                    txtitem.Text = oref.info_item.ToString();
                    txtvalue.Text = oref.info_value.ToString();
                    txtdesc.Text = oref.info_desc;
                }
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                //var lblmin = e.Row.FindControl("lblMin") as System.Web.UI.WebControls.Label;
                //var lblmax = e.Row.FindControl("lblMax") as System.Web.UI.WebControls.Label;
                var lblval = e.Row.FindControl("lblval") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
                //var obj_type = DataBinder.Eval(e.Row.DataItem, "obj_type").ToString();
                var obj_value = DataBinder.Eval(e.Row.DataItem, "obj_value").ToString();
                //var min = DataBinder.Eval(e.Row.DataItem, "min").ToString();
                //var max = DataBinder.Eval(e.Row.DataItem, "max").ToString();

                //if (obj_type == "LIST")
                //{
                //    lblmin.Text = string.Empty;
                //    lblmax.Text = string.Empty;
                    lblval.Text = obj_value;
                //}
                //else
                //{
                //    lblmin.Text = min;
                //    lblmax.Text = max;
                //    lblval.Text = string.Empty;
                //}

            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv(ddltype.SelectedValue.ConvertTo<int>());
        }

        protected void btncancel_Click(object sender, EventArgs e)
        {
            txtitem.Text = string.Empty;
            txtvalue.Text = string.Empty;
            txtdesc.Text = string.Empty;
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int pkid = 0;
            int rid = ViewState["ID"].ConvertTo<int>();
            int oid = ddlobj.SelectedValue.ConvertTo<int>();
            int vid = ddltype.SelectedValue.ConvertTo<int>();

            if (oid != 0)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    if (ViewState["PageMode"].ToString() == "A")
                    {
                        Ref_Obj_Info oref = new Ref_Obj_Info();

                        if (vid != 0 && !string.IsNullOrEmpty(txtitem.Text))

                            oref = context.Ref_Obj_Info.Where(i => i.obj_id == oid && i.obj_value_id == vid && i.info_item == txtitem.Text).SingleOrDefault();

                        if (oref != null)
                        {
                            UserAlert.ShowAlert("Info item exist. Please select/ key in a new item.", CommonEnum.AlertType.Exclamation);
                            return;
                        }
                        else
                        {
                            Ref_Obj_Info srefobj = new Ref_Obj_Info();
                            srefobj.obj_id = oid;
                            srefobj.obj_value_id = vid;
                            srefobj.info_item = txtitem.Text;
                            srefobj.info_value = txtvalue.Text;
                            srefobj.info_desc = txtdesc.Text;
                            srefobj.CREATED_BY = this.logonId;
                            srefobj.CREATE_DATETIME = DateTime.Now;
                            srefobj.MODIFIER_ID = this.logonId;
                            srefobj.MODIFY_DATETIME = DateTime.Now;
                            context.Ref_Obj_Info.Add(srefobj);
                            context.SaveChanges();
                            pkid = (int)srefobj.ID;

                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = pkid, TBL_NAME = "Ref_Obj_Info", FIELD_NAME = "info_item", OLD_VALUE = "", NEW_VALUE = txtvalue.Text, DESC = "New obj info added.", CREATOR_ID = this.logonId });
                        }

                    }
                    else
                    {
                        int id = ViewState["ID"].ConvertTo<int>();
                        var info = context.Ref_Obj_Info.Where(i => i.ID == rid).SingleOrDefault();

                        if (info != null)
                        {
                            if (info.info_item != txtitem.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Info", FIELD_NAME = "info_item", OLD_VALUE = info.info_item, NEW_VALUE = txtitem.Text, DESC = "Info item has been modified", CREATOR_ID = this.logonId });
                            if (info.info_value != txtvalue.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Info", FIELD_NAME = "info_value", OLD_VALUE = info.info_value, NEW_VALUE = txtvalue.Text, DESC = "Info value has been modified", CREATOR_ID = this.logonId });
                            if (info.info_desc != txtdesc.Text)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Info", FIELD_NAME = "info_desc", OLD_VALUE = info.info_desc, NEW_VALUE = txtdesc.Text, DESC = "Info value's desc. has been modified", CREATOR_ID = this.logonId });

                            info.info_item = txtitem.Text;
                            info.info_value = txtvalue.Text;
                            info.info_desc = txtdesc.Text;
                            info.MODIFIER_ID = this.logonId;
                            info.MODIFY_DATETIME = DateTime.Now;
                        }
                    }
                    context.SaveChanges();

                    AuditTrail oAudittrl = new AuditTrail();
                    oAudittrl.AddAuditTrail(oAudit);
                }
            }

            bindgv(ddltype.SelectedValue.ConvertTo<int>());
            txtvalue.Text = string.Empty;
            txtdesc.Text = string.Empty;
            txtitem.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[0].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = ctl.Value.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {

                        Ref_Obj_Info _rolev = context.Ref_Obj_Info.SingleOrDefault(i => i.ID == id);

                        if (_rolev != null)
                        {
                            context.Ref_Obj_Info.Remove(_rolev);
                            context.SaveChanges();
                            oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Info", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_Info " + _rolev.info_item + " has been deleted.", CREATOR_ID = this.logonId });
                        }

                        AuditTrail oAudittrl = new AuditTrail();
                        oAudittrl.AddAuditTrail(oAudit);
                    }
                }
            }
            bindgv(ddltype.SelectedValue.ConvertTo<int>());
        }
    }
}