//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DMPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class VALIDATION_SETTING
    {
        public long ID { get; set; }
        public long obj_id { get; set; }
        public Nullable<long> obj_value_id { get; set; }
        public Nullable<long> info_id { get; set; }
        public string report { get; set; }
        public string item { get; set; }
        public long field_no { get; set; }
        public long rule_no { get; set; }
        public string CREATED_BY { get; set; }
        public System.DateTime CREATE_DATETIME { get; set; }
        public string MODIFIER_ID { get; set; }
        public System.DateTime MODIFY_DATETIME { get; set; }
    }
}
