﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RoleAccess.aspx.cs" Inherits="DMPortal.RoleAccess" %>
<%@ Register Src="~/Controls/Alerts/UserAlert.ascx" TagPrefix="uc1" TagName="UserAlert" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UserAlert runat="server" id="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Role Access Editor</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <table runat="server">
                        <tr>
                            <td class="col-md-4 control-label">Role: </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddlrole" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <legend>Role Access Settings</legend>
            <div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" Width="100%"
                            AllowPaging="True" EmptyDataText="No records has been added." OnRowDataBound="gv_RowDataBound" OnPageIndexChanging="gv_PageIndexChanging">
                            <Columns>
                                <asp:BoundField HeaderText="Screen" DataField="SCREEN_NAME"></asp:BoundField>
                                <asp:TemplateField HeaderText="Access">
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="rbl" runat="server" CssClass="form-control" ValidationGroup="rbl" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="Y" />
                                            <asp:ListItem Text="No" Value="N" Selected="True" />
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlrole" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <br />

        <div class="col-md-12" style="text-align: right">
            <asp:Label ID="lblerrormsg" runat="server" Text="" ForeColor="red" CssClass="control-label"></asp:Label>
            <asp:Button ID="btnAdd" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnAdd_Click" />
        </div>
    </div>


</asp:Content>
