﻿using DMPortal.Manager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class MakerDashboard : System.Web.UI.Page
    {
        public int objid { get; set; }
        public string logonId { get; set; }
        public int userId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            var encrypt = new EncryptionManager();
            this.objid = string.IsNullOrEmpty(Request.QueryString["d0"]) ? 0 : encrypt.Decrypt(Request.QueryString["d0"].Replace(" ", "+")).ConvertTo<int>();

            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;
            this.userId = oDOSession.Id;

            if (!Page.IsPostBack)
            {
                //ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                bindProj();
                BindView(objid);
                if (objid > 0)
                    AssignProjObj();
            }
        }

        private void AssignProjObj()
        {
            using (MDMEntities db = new MDMEntities())
            {
                var oObj = db.Ref_Obj.Where(i => i.ID == this.objid).SingleOrDefault();

                if (oObj != null)
                {
                    ddlproj.SelectedValue = oObj.Proj_id.ToString();
                    ddlproj_SelectedIndexChanged(ddlproj, new EventArgs());
                    ddlobjid.SelectedValue = objid.ToString();
                }
            }
        }

        private void bindProj()
        {
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Proj
                            select new { c.ID, objDESC = c.Project }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindDLL(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void BindView(int tbl)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetMakerSummary", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = tbl;
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "Obj");
                gv.DataSource = ds.Tables["Obj"];
                gv.DataBind();
                con.Close();
            }
        }

        private void bindDLL(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            int tbl = ddlobjid.SelectedValue.ConvertTo<int>();
            BindView(tbl);
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobjid.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj + " - " + pd.Source_Sys }).OrderBy(i => i.objDESC).ToList();
                var validation = db.Ref_Rule.ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindDLL(ddlobjid, datatext, datavalue);
                    }
                    ddlobjid.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void gv_edit(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
            var encrypt = new EncryptionManager();
            if (sender is LinkButton)
            {
                LinkButton linkButton = (LinkButton)sender;
                int id = linkButton.CommandArgument.ConvertTo<int>();
                ViewState["ID"] = id;

                string url = "MakerDetails.aspx?d0=" + encrypt.Encrypt(linkButton.CommandArgument);
                Response.Redirect(url);
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.BindView(ddlobjid.SelectedValue.ConvertTo<int>());
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblNo = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                lblNo.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();

            }
        }
    }
}