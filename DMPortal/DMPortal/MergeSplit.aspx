﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MergeSplit.aspx.cs" Inherits="DMPortal.MergeSplit" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/comm.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function selected(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length >= 2;

            if (checked) {

                if (confirm("Are you sure you want to merge?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least two.');
                return false;
            }


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Object View</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-4 control-label">Project: </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlproj" InitialValue="0" ForeColor="Red" ErrorMessage="Please select 1 ref. project."></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-4 control-label" style="width: 120px">Object : </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlobj" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="3" class="auto-style1" style="width: 100px; text-align: center">
                                        <asp:Button ID="ObjBtn" runat="server" OnClick="Obj_Search" Text="Search" CssClass="btn btn-success" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" CssClass="table table-hover table-bordered" EmptyDataText="No Records." AllowPaging="True" OnPageIndexChanging="gv_PageIndexChanging" OnRowCreated="gv_RowCreated" DataKeyNames="Row_id" ShowFooter="true" OnRowDataBound="gv_RowDataBound">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkMerge" runat="server" CommandArgument='<%# Eval("row_id")%>' Font-Underline="false" OnClick="gv_unmerge" CausesValidation="false">Unmerge</asp:LinkButton>
                                    <br />
                                    <asp:LinkButton ID="lnkDetail" runat="server" CommandArgument='<%# Eval("row_id")%>' Font-Underline="false" OnClick="gv_detail" CausesValidation="false">Details</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("row_id")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <uc1:UserAlert runat="server" ID="UserAlert1" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <br />
        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btnMerge" runat="server" Text="Merge" CssClass="btn btn-warning" CausesValidation="false" OnClientClick="return selected(this)" OnClick="btnMerge_Click" />
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnsave_Click" />
            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <legend>Merge Details</legend>
                    <asp:GridView ID="gvDtl" runat="server" CssClass="table table-hover table-bordered" EmptyDataText="No Records." AllowPaging="True" OnPageIndexChanging="gvDtl_PageIndexChanging" OnRowDataBound="gvDtl_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
