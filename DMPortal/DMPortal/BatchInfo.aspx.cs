﻿using DMPortal.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DMPortal.Object;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace DMPortal
{
    public partial class BatchInfo : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                bindDDL();
                bindgv();
            }
        }

        private void bindDDL()
        {
            using (MDMEntities db = new MDMEntities())
            {
                //var src = (from s in db.Ref_Obj select new { s.Source_Sys }).Distinct().OrderBy(i => i.Source_Sys).ToList();
                var src = (from s in db.Ref_Source select new { s.Source_Sys }).Distinct().OrderBy(i => i.Source_Sys).ToList();

                if (src.Count > 0)
                {
                    foreach (var item in src)
                    {
                        var datavalue = item.Source_Sys;
                        var datatext = item.Source_Sys;
                        ddlsource.Items.Add(new ListItem(datatext, datavalue));
                    }
                    ddlsource.Items.Insert(0, new ListItem("-SELECT-", "0"));
                }
            }
        }

        private void bindgv()
        {
            using (MDMEntities db = new MDMEntities())
            {
                string src = ddlsource.SelectedItem.ToString();

                var batchinfo = (from b in db.Batch_Info
                                    where b.Source_Sys == src
                                    select b
                                    ).ToList();
                gv.DataSource = batchinfo;
                gv.DataBind();
            }
        }

        protected void ddlsource_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindgv();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int pkid = 0;
            int id = ViewState["ID"].ConvertTo<int>();
            string src = ddlsource.SelectedValue.ToString();
            string RptDT = TxtDate.Text.Replace("-", "");
            decimal Report_DT = Convert.ToDecimal(RptDT);

            using (MDMEntities context = new MDMEntities())
            {
                if (ViewState["PageMode"].ToString() == "A")
                {
                    var myInClause = new string[] { "Pending", "Processing" };
                    Batch_Info batch = new Batch_Info();
                    batch = context.Batch_Info.Where(i => i.Source_Sys == src && i.Report_DT == Report_DT && myInClause.Contains(i.Batch_Status)).SingleOrDefault();

                    if (batch != null)
                    {
                        UserAlert.ShowAlert("Date exist. Please select a new date.", CommonEnum.AlertType.Exclamation);
                        return;
                    }
                    else
                    {
                        Batch_Info refbatch = new Batch_Info();
                        refbatch.ID = id;
                        refbatch.Source_Sys = ddlsource.SelectedItem.ToString();
                        refbatch.CREATED_BY = this.logonId;
                        refbatch.CREATED_DT = DateTime.Now;
                        refbatch.Batch_Status = "Pending";
                        refbatch.Trigger_Type = "Manual";
                        refbatch.Report_DT = Convert.ToDecimal(RptDT);
                        context.Batch_Info.Add(refbatch);
                        context.SaveChanges();
                        pkid = (int)refbatch.ID;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gv.Rows)
                    {
                        CheckBox chk = (CheckBox)row.FindControl("chkid");
                        int bid = chk.Text.ConvertTo<int>();

                        if (chk != null && chk.Checked)
                        {
                            var refbatch = context.Batch_Info.Where(i => i.ID == bid).SingleOrDefault();
                            refbatch.Report_DT = Report_DT;
                            refbatch.CREATED_BY = this.logonId;
                            refbatch.CREATED_DT = DateTime.Now;
                            context.SaveChanges();
                        }
                    }
                }
            }

            bindgv();
            TxtDate.Text = string.Empty;
            ViewState["PageMode"] = "A";
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            int page = ViewState["PageIndex"].ConvertTo<int>();
            int pagesize = gv.PageSize;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var No = e.Row.FindControl("lblNo") as System.Web.UI.WebControls.Label;
                var RptDT = e.Row.FindControl("RptDT") as System.Web.UI.WebControls.Label;
                No.Text = ((page * pagesize) + (e.Row.RowIndex + 1)).ToString();
                var lblSts = e.Row.FindControl("lblSts") as System.Web.UI.WebControls.Label;

                RptDT.Text = DataBinder.Eval(e.Row.DataItem, "Report_DT").ToString();
                lblSts.Text = DataBinder.Eval(e.Row.DataItem, "Batch_Status").ToString();

                if (lblSts.Text == "Failed")
                {
                    lblSts.ForeColor = Color.Red;
                }
                else if (lblSts.Text == "Successful")
                {
                    lblSts.ForeColor = Color.Green;
                }
                else if (lblSts.Text == "Processing")
                {
                    lblSts.ForeColor = Color.Orange;
                }

                CheckBox chk = (CheckBox)e.Row.FindControl("chkid");
                int id = chk.Text.ConvertTo<int>();
                ViewState["ID"] = id;

                var Strt_DT = DataBinder.Eval(e.Row.DataItem, "Batch_Start_DT");
                if (Strt_DT != null)
                {
                    chk.Enabled = false;
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            this.bindgv();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkid");
                int id = chk.Text.ConvertTo<int>();

                if (chk != null && chk.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        Batch_Info _rolev = context.Batch_Info.SingleOrDefault(i => i.ID == id);

                        if (_rolev != null)
                        {
                            context.Batch_Info.Remove(_rolev);
                            context.SaveChanges();
                        }

                       bindgv();
                    }
                }
            }
        }

        protected void gv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void chkid_CheckedChanged(object sender, EventArgs e)
        {
            ViewState["PageMode"] = "E";
        }
    }
}