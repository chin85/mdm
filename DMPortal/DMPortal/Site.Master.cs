﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;
        public string logonId { get; set; }
        public bool isRoleManagement { get; set; }
        public bool isRoleAccess { get; set; }
        public bool isUserManagement { get; set; }
        public bool isRefObj { get; set; }
        public bool isRefObjV { get; set; }
        public bool isRefObjVMapped { get; set; }
        public bool isRefObjAtt { get; set; }
        public bool isRefProj { get; set; }
        public bool isRefSource { get; set; }
        public bool isObjView { get; set; }
        public bool isObjImport { get; set; }
        public bool isMergeSplit { get; set; }
        public bool isAuditLog { get; set; }
        public bool isChecker { get; set; }
        public bool isMaker { get; set; }
        public string BaseURL { get; set; }
        public string HomeURL { get; set; }
        protected void Page_Init(object sender, EventArgs e)
        {
            BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            this.HomeURL = ConfigurationManager.AppSettings["HomeURL"].ToString();
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];

            if (oDOSession == null)
            {
                Response.Redirect(HomeURL, true);
            }
            else
            {
                using (MDMEntities context = new MDMEntities())
                {
                    var userlogonlog = context.USER_LOGON_LOG.Where(i => i.LOG_ID == oDOSession.LogId).SingleOrDefault();
                    if (userlogonlog != null)
                    {
                        if (userlogonlog.LOG_OFF_MODE != null)
                        {
                            Session.Abandon();
                            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
                            Response.Redirect(HomeURL, true);
                        }
                    }
                }
            }

            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            SessionRole oDOSessionRole = new SessionRole();
            oDOSessionRole = (SessionRole)Session["SessionRole"];
            logonId = oDOSession.LogonId;
            lblUsername.InnerText = "Welcome " + logonId;
            isRoleManagement = oDOSessionRole.showRoleManagement;
            isRoleAccess = oDOSessionRole.showRoleAccess;
            isUserManagement = oDOSessionRole.showUserManagement;
            isRefObj = oDOSessionRole.showRefObject;
            isRefObjV = oDOSessionRole.showRefObjectV;
            isRefObjVMapped = oDOSessionRole.showRefObjectVMapped;
            isRefObjAtt = oDOSessionRole.showRefObjectAtt;
            isRefProj = oDOSessionRole.showRefProj;
            isRefSource = oDOSessionRole.showRefSource;
            isObjView = oDOSessionRole.showObjView;
            isObjImport = oDOSessionRole.showObjImport;
            isMergeSplit = oDOSessionRole.showMergeSplit;
            isAuditLog = oDOSessionRole.showAuditLog;
            isChecker = oDOSessionRole.showChecker;
            isMaker = oDOSessionRole.showMaker;

            if (!IsPostBack)
            {
                Session["Reset"] = true;
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);

                if (isRefObj == false && isRefObjV == false && isRefObjVMapped == false && isRefObjAtt == false && isRefProj == false && isRefSource == false && isObjView == false && isObjImport == false && isMergeSplit == false)
                    Ulparam.Visible = false;
                if (isRoleManagement == false && isRoleAccess == false && isUserManagement == false)
                    Ulprofile.Visible = false;
                if (isAuditLog == false)
                    Ulrpt.Visible = false;
                if (isChecker == false && isMaker == false )
                    Uldb.Visible = false;

                lnkroleaccess.Visible = isRoleAccess;
                lnkuurolemgmt.Visible = isUserManagement;
                lnkurole.Visible = isRoleManagement;
                lnkrefobj.Visible = isRefObj;
                lnkrefobjv.Visible = isRefObjV;
                lnkmapvalue.Visible = isRefObjVMapped;
                linkrefobjatt.Visible = isRefObjAtt;
                lnkrefproj.Visible = isRefProj;
                lnkrefsource.Visible = isRefSource;
                lnkobjview.Visible = isObjView;
                lnkobjimport.Visible = isObjImport;
                lnkmerge.Visible = isMergeSplit;
                lnkaudit.Visible = isAuditLog;
                lnkchecker.Visible = isChecker;
                lnkmaker.Visible = isMaker;

                lnklogout.HRef = this.BaseURL + "login2.aspx?smode=L";
            }
        }
    }
}