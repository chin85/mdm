﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BatchInfo.aspx.cs" Inherits="DMPortal.BatchInfo" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" lang="javascript">
        $(document).ready(function () {
            $('.date-picker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'yy-mm-dd',
                beforeShowDay: function (date) {
                    //getDate() returns the day (0-31)
                    if (date.getDate() == LastDayOfMonth(date.getFullYear(), date.getMonth())) {
                        return [true, ''];
                    }
                    return [false, ''];
                }
                //showOn: "button",
                //buttonImage: "Resources/calendar.jpg",
                //buttonImageOnly: true
            }).keyup(function (e) {
                if (e.keyCode == 8 || e.keyCode == 46) {
                    $.datepicker._clearDate(this);
                }
            });
        });

        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function LastDayOfMonth(Year, Month) {
            return (new Date((new Date(Year, Month + 1, 1)) - 1)).getDate();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Batch Info</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>

                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td class="col-4 control-label">Source System: </td>
                                    <td colspan="5" class="col-8">
                                        <asp:DropDownList ID="ddlsource" runat="server" Width="200px" OnSelectedIndexChanged="ddlsource_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <uc1:UserAlert runat="server" ID="UserAlert" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div>
                    <table>
                        <tr>
                            <td class="col-4 control-label">Reporting Date: </td>
                            <td colspan="5" class="col-8">
                                <asp:TextBox ID="TxtDate" runat="server" Width="200px" class="form-control date-picker input-md" ClientIDMode="Static"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div>
        <br />
        <div class="col-12" style="text-align: right">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnSave_Click" />
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered" AllowPaging="True" DataKeyNames="ID" OnPageIndexChanging="gv_PageIndexChanging" EmptyDataText="No Records." OnRowDataBound="gv_RowDataBound" OnSelectedIndexChanged="gv_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkid" runat="server" Text='<%# Eval("ID")%>' OnCheckedChanged="chkid_CheckedChanged" />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Source_Sys" DataField="Source_Sys" />
                            <asp:TemplateField HeaderText="Report_DT">
                                <ItemTemplate>
                                    <asp:Label ID="RptDT" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Batch_Start_DT" DataField="Batch_Start_DT" DataFormatString="{0:dd/MM/yyyy HH:MM:ss:sss}" />
                            <asp:BoundField HeaderText="Batch_End_DT" DataField="Batch_End_DT" DataFormatString="{0:dd/MM/yyyy HH:MM:ss:sss}" />
                            <asp:BoundField HeaderText="CREATED_BY" DataField="CREATED_BY" />
                            <asp:BoundField HeaderText="CREATED_DT" DataField="CREATED_DT" />
                            <asp:TemplateField HeaderText="Batch_Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblSts" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Trigger_Type" DataField="Trigger_Type" />
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSave" />
                    <asp:AsyncPostBackTrigger ControlID="btnDelete" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="col-12" style="text-align: right">
        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClick="btnDelete_Click" />
    </div>
    <br />
</asp:Content>
