﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPasword.aspx.cs" Inherits="DMPortal.EditPasword" %>
<%@ Register Src="~/Controls/Alerts/UserAlert.ascx" TagPrefix="uc1" TagName="UserAlert" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:useralert runat="server" id="UserAlert" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Manage Password</h4>
            </div>
        </div>
        <div class="col-md-12" style="text-align: right">
            <div style="margin: auto">

                <div>
                    <div class="col-md-4" style="text-align: right">
                        <label class="control-label" style="text-align: right" for="textinput">*New Password :</label>
                    </div>

                    <div class="col-md-8" style="text-align: left">
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" class="form-control input-md"></asp:TextBox>
                        <ajaxToolkit:PasswordStrength StrengthIndicatorType="Text" ID="txtPassword_PasswordStrength" runat="server" BehaviorID="txtPassword_PasswordStrength" TargetControlID="txtPassword" MinimumLowerCaseCharacters="2" MinimumNumericCharacters="2" MinimumUpperCaseCharacters="2" PreferredPasswordLength="10" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword" ForeColor="Red" ErrorMessage="Object can not be empty."></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div>
                    <div class="col-md-4" style="text-align: right">
                        <label class="control-label" style="text-align: right" for="textinput">*Confirm Password :</label>
                    </div>

                    <div class="col-md-8" style="text-align: left">
                        <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" class="form-control input-md" Enabled="true"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ForeColor="Red" ErrorMessage="New Password dose not match Confirm Password." ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"></asp:CompareValidator><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfirmPassword" ForeColor="Red" ErrorMessage="Object can not be empty."></asp:RequiredFieldValidator>
                    </div>
                </div>


            </div>
            <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnsave_Click" />
            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
        </div>
    </div>
</asp:Content>
