﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{
    public class UserLogEnum
    {
        public enum ObjectTypeEnum
        {
            [Description("USER ACCOUNT")]
            user_account,
        }

        public enum LogDescEnum
        {
            [Description("New user account has been created.")]
            user_account_create = 1,
            [Description("user info has been updated.")]
            user_account_update = 2,
            [Description("user account has been set to inactive.")]
            user_account_delete = 3,
        }

        public enum LogDetailObjectNMEnum
        {
            [Description("LogOn ID")]
            LOGON_ID,
            [Description("Full Name")]
            LOGON_NM,
            [Description("Email")]
            LOGON_EMAIL,
            [Description("Role")]
            ROLE,
            [Description("Account Status")]
            ACCOUNT_STATUS,
            [Description("Account Validity")]
            ACCOUNT_VALIDITY,
            [Description("Password Reset")]
            PASSWORD_RESET,
            [Description("Password Change")]
            PASSWORD_CHANGE,
            [Description("Logon Status")]
            LOGON_STATUS,
        }
    }
}