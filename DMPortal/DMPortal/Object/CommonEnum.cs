﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace DMPortal.Object
{
    public class CommonEnum
    {
        public enum statusEnum
        {
            [Description("YES")]
            Yes,
            [Description("NO")]
            No,
            [Description("Y")]
            Y,
            [Description("N")]
            N,
            [Description("A")]
            Active,
            [Description("I")]
            Inactive,
            [Description("S")]
            Suspend,
            [Description("L")]
            Locked,


        }
        public enum factorTypeEnum
        {
            [Description("RANGE")]
            Range,
            [Description("LIST")]
            List,
        }
        public enum ModeEnum
        {
            [Description("VIEW")]
            VIEW,
            [Description("ADD")]
            ADD,
        }



        public enum SortOrder
        {
            [Description("ASCENDING")]
            Asc,
            [Description("DESCENDING")]
            Desc,
        }

        public enum SortedCol
        {
            [Description("ID")]
            ID,
            [Description("Insurer_Agent")]
            InsurerAgent,
            [Description("SUBMISSION_DATE")]
            SubmissionDate,
            [Description("OVERDUE_DAY")]
            OverdueDay,
            [Description("OVERDUE_COUNT")]
            OverdueCount,
            [Description("TEMPLETE_NAME")]
            TempleteName,
            [Description("TEMPLETE")]
            Templete,
            [Description("MODIFIED_BY")]
            ModifiedBy,
            [Description("MODIFY_DATETIME")]
            ModifyDateTime,

        }


        public enum UserRole
        {
            [Description("SYS_ADMIN")]
            Sysadmin,
            [Description("IT_ADMIN")]
            Itadmin,
            [Description("INSURER")]
            Insurer,
            [Description("REPORT_VIEWER")]
            Reportviwer,
        }

        public enum LogonStatus
        {
            [Description("OFF")]
            Off,
            [Description("ON")]
            On,
        }

        public enum LogOffMode
        {
            [Description("TIME OUT")]
            timeout,
            [Description("LOG OFF")]
            logoff,
            [Description("FORCE LOG OFF")]
            forcelogoff,
        }

        public enum BillingRptEnum
        {
            [Description("Company Summary")]
            CS,
            [Description("Transaction Detail")]
            TD,
            [Description("Transaction Summary")]
            TS,
        }

        public enum RecordStatusEnum
        {
            [Description("Active")]
            A,
            [Description("Inactive")]
            I,
            [Description("Open")]
            O,
            [Description("Rejected")]
            R,
        }

        #region AlertType
        /// <summary>
        /// Summary description for AlertType
        /// </summary>
        public enum AlertType
        {
            /// <summary>
            /// Information given to the user
            /// </summary>
            Information = 1,
            /// <summary>
            /// Alert shown to the user
            /// </summary>
            Exclamation = 2,
            /// <summary>
            /// Critical. Normally used for error messagess
            /// </summary>
            Critical = 3
        }
        #endregion

        #region AlertHideEvent
        /// <summary>
        /// Client-side event to be used to hide a <code>UserAlert</code> control
        /// </summary>
        public enum AlertHideEvent
        {
            /// <summary>
            /// Do not hide the UserAlert control
            /// </summary>
            None = 1,
            /// <summary>
            /// Hide the UserAlert once the user clicks it
            /// </summary>
            Click = 2,
            /// <summary>
            /// Hide the UserAlert once the user double-clicks it
            /// </summary>
            DoubleClick = 3,
        }
        #endregion

    }
}