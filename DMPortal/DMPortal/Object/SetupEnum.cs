﻿using System.ComponentModel;


namespace DMPortal.Object
{
    
        public enum SetupEnum
        {
            ENFORCE_PASSWORD_HISTORY,
            PASSWORD_AGE,
            PASSWORD_COMPLEXITY_REQUIREMENT,
            FAILED_ATTEMPTS,
            SESSION_TIMEOUT,
        }
    
}