﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{
    public enum LoginMsgEnum
    {
        [Description("Please enter username and password.")]
        Enter_username_password,
        [Description("Invalid username.")]
        Inv_user_id,
        [Description("Invalid password.")]
        Inv_password,
        [Description("Account has been inactivated. Please contact administrator to activate account.")]
        User_in,
        [Description("Account has been locked. Please contact administrator to activate account.")]
        User_lo,
        [Description("Account has been suspended. Please contact administrator to activate account")]
        User_su,
        [Description("Concurrent logon is not allowed.")]
        User_con_logon,
        [Description(" logon attempt is left before locked out.")]
        User_failed_attempt,
        [Description("Password Expiried. Kindly change the password.")]
        Password_expiry,
        [Description("First Log On. Kindly change the password.")]
        First_logon,
    }
}