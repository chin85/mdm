﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{
    public class UploadObject
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileSubmissionType { get; set; }

    }
}