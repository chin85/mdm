﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{

        public class Time : IComparable, IComparable<Time>, IEquatable<Time>
        {
            private DateTime _dateTime;
            public int Hours { get { return _dateTime.Hour; } set { _dateTime = new DateTime(0, 0, 0, value, this.Minutes, this.Seconds); } }
            public int Minutes { get { return _dateTime.Minute; } set { _dateTime = new DateTime(0, 0, 0, this.Hours, value, this.Seconds); } }
            public int Seconds { get { return _dateTime.Second; } set { _dateTime = new DateTime(0, 0, 0, this.Hours, this.Minutes, value); } }

            public Time() : this(new DateTime()) { }
            public Time(DateTime dateTime)
            {
                _dateTime = dateTime;
            }
            public int CompareTo(object obj)
            {
                Time time = obj as Time;

                if (time == null)
                    return 1;

                var compareHours = this.Hours.CompareTo(time.Hours);
                if (compareHours != 0) return compareHours;
                var compareMinutes = this.Minutes.CompareTo(time.Minutes);
                if (compareMinutes != 0) return compareMinutes;
                var compareSeconds = this.Seconds.CompareTo(time.Seconds);
                if (compareSeconds != 0) return compareSeconds;

                return 0;
            }

            public int CompareTo(Time other)
            {
                return CompareTo(other);
            }

            public bool Equals(Time other)
            {
                return this.CompareTo(other) == 0;
            }

            public override string ToString()
            {
                return _dateTime.ToString("HH:mm");
            }
        }
    
}