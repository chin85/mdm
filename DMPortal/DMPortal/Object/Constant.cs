﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{
    public class Constant
    {
        public const string PARAM_NATTYP = "NATTYP";
        public const string PARAM_CNTRY = "CNTRY";
        public const string PARAM_STATE = "STATE";
        public const string PARAM_TRANTYP = "TRANTYP";
        public const string PARAM_BRANCH = "BRANCH";
        public const string PARAM_IDTYP = "IDTYP";
        public const string PARAM_PROTYP = "PROTYP";
        public const string PARAM_OCCP = "OCCP";
        public const string PARAM_CURRENCY = "CURRENCY";
        public const string PARAM_BIZTYP = "BIZTYP";
        public const string PARAM_GENDER = "GENDER";

    }
}