﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Object
{
    internal enum MachineKeyValidationMode
    {
        SHA1,
        MD5,
        TripleDES,
    }
}