﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace DMPortal.Object
//{
//    public class GridViewTemplate
//    {
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal.Object
{
    public class GridViewTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private string columnNameFriendly;
        private string columnNameData;
        private Control control;

        public GridViewTemplate(DataControlRowType type, string colNameFr, string colNameDt, Control con)
        {
            templateType = type;
            columnNameFriendly = colNameFr;
            columnNameData = colNameDt;
            control = con;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    {
                        Literal lc = new Literal();
                        lc.Text = columnNameFriendly;
                        container.Controls.Add(lc);
                        break;
                    }
                case DataControlRowType.DataRow:
                    {
                        Control field = control;
                        if (field.GetType() == typeof(Label))
                        {
                            Label lbl = new Label();
                            lbl.ID = "lbl" + columnNameFriendly;
                            lbl.DataBinding += new EventHandler(this.lbl_DataBind);
                            container.Controls.Add(lbl);
                        }
                        else if (field.GetType() == typeof(TextBox))
                        {
                            TextBox txt = new TextBox();
                            txt.ID = "txt" + columnNameFriendly;
                            txt.DataBinding += new EventHandler(this.txt_DataBind);
                            container.Controls.Add(txt);
                        }
                        else if (field.GetType() == typeof(DropDownList))
                        {
                            DropDownList ddl = (DropDownList)field;
                            ddl.ID = "ddl" + columnNameFriendly;
                            ddl.DataBinding += new EventHandler(this.ddl_DataBind);
                            container.Controls.Add(ddl);
                        }
                        else if (field.GetType() == typeof(CheckBox))
                        {
                            CheckBox cbx = new CheckBox();
                            cbx.ID = "cbx" + columnNameFriendly;
                            cbx.DataBinding += new EventHandler(this.cbx_DataBind);
                            container.Controls.Add(cbx);
                        }
                        break;
                    }
            }
        }

        private void txt_DataBind(object sender, EventArgs e)
        {
            TextBox txt = (TextBox)sender;
            GridViewRow row = (GridViewRow)txt.NamingContainer;
            txt.Text = DataBinder.Eval(row.DataItem, columnNameData).ToString();
        }

        private void lbl_DataBind(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            GridViewRow row = (GridViewRow)lbl.NamingContainer;
            lbl.Text = DataBinder.Eval(row.DataItem, columnNameData).ToString();
        }

        private void ddl_DataBind(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.NamingContainer;
            ddl.SelectedValue = DataBinder.Eval(row.DataItem, columnNameData).ToString();
        }

        private void cbx_DataBind(object sender, EventArgs e)
        {
            CheckBox cbx = (CheckBox)sender;
            GridViewRow row = (GridViewRow)cbx.NamingContainer;
            cbx.Checked = (bool)DataBinder.Eval(row.DataItem, columnNameData);
        }
    }
}

