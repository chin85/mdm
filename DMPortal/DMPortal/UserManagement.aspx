﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="DMPortal.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" lang="" src="Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function SelectAllCheckboxes(chk) {
            $('#<%=gv.ClientID %>').find("input:checkbox").each(function () {
                if (this != chk) {
                    this.checked = chk.checked;
                }
            });
        }

        function checkselector(obj) {

            var checked = $("#MainContent_gv INPUT[type='checkbox']:checked").length > 0;

            if (checked) {

                if (confirm("Are you sure you want to delete?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                alert('Please select at least one.');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>User Management</h4>
            </div>
        </div>
        <div class="container">
            <h4>Filter</h4>
            <fieldset>
                <table runat="server">

                    <tr>
                        <td class="col-md-4 control-label">Username: </td>
                        <td colspan="3" class="col-md-8">
                            <asp:TextBox ID="txtusername" runat="server" class="form-control input-md" Enabled="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4 control-label">Role: </td>
                        <td colspan="3" class="col-md-8">
                            <asp:DropDownList ID="ddlRole" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-md-4 control-label">Status: </td>
                        <td colspan="3" class="col-md-8">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="" Selected="True"> SELECT </asp:ListItem>
                                <asp:ListItem Value="ACTIVE"> Active </asp:ListItem>
                                <asp:ListItem Value="INACTIVE"> Inactive </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <div class="col-md-12" style="text-align: right">
                    <asp:Label ID="lblerrormsg" runat="server" Text="Unavailable" ForeColor="Red" Visible="False" CssClass="control-label"></asp:Label>
                    <asp:Button ID="btnSearchF" runat="server" Text="Search" CausesValidation="false" CssClass="btn btn-success" OnClick="btnSearchF_Click" />
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>

                    <asp:GridView ID="gv" runat="server" AutoGenerateColumns="False" AllowPaging="true" DataKeyNames="ID" CssClass="table table-hover table-bordered"
                        EmptyDataText="No records have been added.">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="chkid" runat="server" type="checkbox" value='<%# Eval("ID")%>' />
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="3%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User">
                                <ItemTemplate>
                                    <asp:Label ID="lblu" runat="server" Text='<%# Eval("LOGON_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbls" runat="server" Text='<%# Eval("ACCOUNT_STATUS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("rolename") %>'></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="250px" />
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Is Maker" DataField="MAKER_FG"></asp:BoundField>
                            <asp:BoundField HeaderText="Is Checker" DataField="CHECKER_FG"></asp:BoundField>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%# Eval("ID")%>'
                                        Font-Underline="false" OnClick="gv_edit" CausesValidation="false">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btndelete" />
                    <asp:AsyncPostBackTrigger ControlID="btn_register" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="col-md-12" style="text-align: left">
            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger" CausesValidation="false" OnClick="btndelete_Click" OnClientClick="return checkselector(this)" />
        </div>
    </div>
    <div class="container">
        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12" style="padding-top: 20px; padding-bottom: 10px;">
                        <div class="col-md-12">
                            <fieldset>
                                <!-- Form Name -->
                                <legend>User Profile</legend>
                            </fieldset>
                            <fieldset>
                                <div id="divinfo" runat="server">
                                    <fieldset>
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                * Username :</label>
                                            <div class="col-md-6">
                                                <input id="txtID" name="txtID" type="text" runat="server" class="form-control input-md" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required."
                                                    ControlToValidate="txtID" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <!-- Text input-->
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                Full Name :</label>
                                            <div class="col-md-6">
                                                <asp:TextBox runat="server" ID="txtname" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                * Email :</label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtmail" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required."
                                                    ControlToValidate="txtmail" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="gadd" runat="server" ErrorMessage="Invalid email. eg. abc@alrajhi.com.my"
                                                    ControlToValidate="txtmail" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                * Role :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddl" runat="server" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required."
                                                    ControlToValidate="ddl" InitialValue="-1" ForeColor="Red"></asp:RequiredFieldValidator>

                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                Status :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddluserstatus" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                                    <asp:ListItem Value="I">Inactive</asp:ListItem>
                                                    <asp:ListItem Value="S">Suspend</asp:ListItem>
                                                    <asp:ListItem Value="L">Locked</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                Account Validity :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlaccValidity" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="30" Selected="True">30</asp:ListItem>
                                                    <asp:ListItem Value="60">60</asp:ListItem>
                                                    <asp:ListItem Value="90">90</asp:ListItem>
                                                    <asp:ListItem Value="120">120</asp:ListItem>
                                                    <asp:ListItem Value="0">Never</asp:ListItem>
                                                </asp:DropDownList>
                                                <span class="content">Days (To force account suspended) </span>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                LogOn Status :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddllogon" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="OFF" Selected="True">Off</asp:ListItem>
                                                    <asp:ListItem Value="ON">On</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <legend>Additional Role</legend>
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                Maker Role :</label>
                                            <div class="col-md-4">
                                                <asp:RadioButtonList  ID="rblm" runat="server" ValidationGroup="rblm" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Y" />
                                                    <asp:ListItem Text="No" Value="N" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="form-group">
                                            <label class="col-md-6 control-label" for="textinput" style="text-align: right">
                                                Checker Role :</label>
                                            <div class="col-md-4">
                                                <asp:RadioButtonList ID="rbl"   runat="server" ValidationGroup="rbl" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="Y" />
                                                    <asp:ListItem Text="No" Value="N" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                         
                                    </fieldset>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: right">
                            <asp:Label ID="lbl_warning" runat="server" Text="" CssClass="control-label" ForeColor="Red"></asp:Label>
                            <asp:Button ID="btn_register" Text="Add" runat="server" CssClass="btn btn-success" OnClick="btn_register_Click" />
                            <asp:Button ID="btncancel" runat="server" Text="Clear" CssClass="btn btn-inverse" CausesValidation="false" OnClick="btncancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btn_register" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
