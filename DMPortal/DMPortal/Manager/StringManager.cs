﻿

namespace DMPortal.Manager
{
    public static class StringManager
    {
        public static string Substring(this string value, int startIndex, int length, bool ignoreException)
        {
            if (ignoreException)
            {
                string result = "";
                if (length < 0)
                {
                    return result;
                }
                if (value.Length > startIndex + length && startIndex > -1)
                {
                    result = value.Substring(startIndex, length).Trim();
                }
                else if (value.Length > startIndex && startIndex > -1)
                {
                    result = value.Substring(startIndex, value.Length - startIndex).Trim();
                }
                else
                {
                    result = "";
                }
                return result;
            }
            else
                return value.Substring(startIndex, length);
        }

        public static string DisplayLimitedstring(this string value, int displayLength)
        {
            var result = value.Substring(0, displayLength, true);
                //value.Substring(0, displayLength, true);

            if (value.Length > displayLength)
                return result + " ...";
            else
                return result;
        }
    }
}