﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;


namespace DMPortal.Manager
{
    public class EncryptionManager
    {
        private byte[] key = { };
        private byte[] IV = {
		    0x12,
		    0x34,
		    0x56,
		    0x78,
		    0x90,
		    0xab,
		    0xcd,
		    0xef
	    };

        public string Decrypt(string stringToDecrypt)
        {
            return Decrypt(stringToDecrypt, true);
        }
        public string Decrypt(string stringToDecrypt, bool isDecoded)
        {
            return Decrypt(stringToDecrypt, "12345678", isDecoded);
        }
        public string Decrypt(string stringToDecrypt, string sEncryptionKey)
        {
            return Decrypt(stringToDecrypt, sEncryptionKey, true);
        }
        public string Decrypt(string stringToDecrypt, string sEncryptionKey, bool isDecoded)
        {
            if (isDecoded)
                stringToDecrypt = HttpUtility.HtmlDecode(stringToDecrypt);

            byte[] inputByteArray = new byte[stringToDecrypt.Length + 1];
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Encrypt(string stringToEncrypt)
        {
            return Encrypt(stringToEncrypt, true);
        }
        public string Encrypt(string stringToEncrypt, bool isEncoded)
        {
            return Encrypt(stringToEncrypt, "12345678", isEncoded);
        }
        public string Encrypt(string stringToEncrypt, string sEncryptionKey)
        {
            return Encrypt(stringToEncrypt, sEncryptionKey, true);
        }
        public string Encrypt(string stringToEncrypt, string sEncryptionKey, bool isEncoded)
        {
            try
            {
                key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();

                if (isEncoded)
                    return HttpUtility.UrlEncode(Convert.ToBase64String(ms.ToArray()));
                else
                    return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string EncryptMD5(string stringToEncrypt)
        {
            try
            {
                MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
                UTF8Encoding uTF8Encoding = new UTF8Encoding();
                byte[] hashedbyte = mD5CryptoServiceProvider.ComputeHash(uTF8Encoding.GetBytes(stringToEncrypt));

                return Convert.ToBase64String(hashedbyte);
            }

            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string EncryptSHA1(string stringToEncrypt)
        {
            try
            {

                UTF8Encoding uTF8Encoding = new UTF8Encoding();
                byte[] hashedbyte = SHA1.Create().ComputeHash(uTF8Encoding.GetBytes(stringToEncrypt));

                return MachineKey.ByteArrayToHexString(hashedbyte, 0);

            }

            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}