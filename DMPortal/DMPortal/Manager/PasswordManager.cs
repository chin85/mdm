﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DMPortal.Manager
{
    public class PasswordManager
    {
        public int max_length = 15;
        public string new_password;
        private const string upper_case_rule = "[A-Z]";
        private const string lower_case_rule = "[a-z]";
        private const string number_rule = "[0-9]";
        private const string special_rule = "[^a-zA-Z0-9]";
        private const string logon_id_msg = "Password cannot be same as logon ID.";
        private const string password_length_msg = "Minimum password length required is ";
        private const string password_combination_msg = "Password combination is not meet the requirement.";


        public string GENERATE_PASSWORD(int min_length)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            int length = random.Next(min_length, max_length + 1);
            List<int> listNumber = new List<int>() { 1, 2, 3, 4 };
            int n = 0;
            char[] password = new char[length];

            for (int i = 0; i < length; i++)
            {
                n = random.Next(0, listNumber.Count);
                int item = listNumber[n];

                //NUMBER
                if (item == 1)
                {
                    password[i] = (char)(random.Next(48, 58));
                }

                //UPPERCASE
                if (item == 2)
                {
                    int[] upperCaseList = { random.Next(65, 73), random.Next(74, 76), random.Next(77, 79), random.Next(80, 91) };
                    int upperCasepicker = random.Next(0, 4);
                    password[i] = (char)upperCaseList[upperCasepicker];
                }

                //LOWERCASE
                if (item == 3)
                {
                    int[] lowerCaseList = { random.Next(97, 105), random.Next(106, 108), random.Next(109, 111), random.Next(112, 123) };
                    int lowerCasepicker = random.Next(0, 4);
                    password[i] = (char)lowerCaseList[lowerCasepicker];
                }

                //SYMBOL
                if (item == 4)
                {
                    int[] symbolList = { random.Next(33, 48), random.Next(58, 65), random.Next(91, 97), random.Next(123, 127) };
                    int symbolpicker = random.Next(0, 4);
                    password[i] = (char)symbolList[symbolpicker];
                }

                //3 OF 4 COMBINATION
                if (listNumber.Count >= 2)
                {
                    listNumber.RemoveAt(n);
                }
                else
                {
                    listNumber = new System.Collections.Generic.List<int>() { 1, 2, 3, 4 };
                }
            }

            for (int i = 0; i < length; i++)
            {
                n = random.Next(0, length);
                char temp = password[i];
                password[i] = password[n];
                password[n] = temp;
            }

            new_password = new string(password);
            return new_password;
        }

        public string VALIDATE_PASSWORD_RULE(string password, string logonID, int minimumLength, int upperCaseMinimumCount
           , int lowerCaseMinimumCount, int numberMinimumCount, int specialMinimumCount, int matchMinimumCount)
        {
            Regex upperCaseRegex = new Regex(upper_case_rule);
            Regex lowerCaseRegex = new Regex(lower_case_rule);
            Regex numberRegex = new Regex(number_rule);
            Regex specialRegex = new Regex(special_rule);

            // VALIDATE LOGON ID
            if (password.ToUpper() == logonID.ToUpper())
            {
                return logon_id_msg;
            }

            // VALIDATE LENGTH
            if (password.Length < minimumLength)
            {
                return (password_length_msg + minimumLength.ToString() + ".");
            }

            // VALIDATE COMBINATION
            int upperCaseCount = upperCaseRegex.Matches(password).Count;
            int lowerCaseCount = lowerCaseRegex.Matches(password).Count;
            int numberCount = numberRegex.Matches(password).Count;
            int specialCount = specialRegex.Matches(password).Count;

            int upperCaseMatch = (upperCaseMinimumCount > upperCaseCount) ? 0 : 1;
            int lowerCaseMatch = (lowerCaseMinimumCount > lowerCaseCount) ? 0 : 1;
            int numberMatch = (numberMinimumCount > numberCount) ? 0 : 1;
            int specialMatch = (specialMinimumCount > specialCount) ? 0 : 1;
            int matchCount = upperCaseMatch + lowerCaseMatch + numberMatch + specialMatch;

            if (matchMinimumCount > matchCount)
            {
                return (password_combination_msg);
            }

            return String.Empty;
        }
    }

}