﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Manager
{
    public class AuditTrail
    {
        public void AddAuditTrail(List<AUDIT_TRAIL> oAudit)
        {
            if (oAudit.Count > 0)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    foreach (var item in oAudit)
                    {
                        AUDIT_TRAIL _trail = new AUDIT_TRAIL();
                        _trail.OBJECT_ID = item.OBJECT_ID;
                        _trail.TBL_NAME = item.TBL_NAME;
                        _trail.FIELD_NAME = item.FIELD_NAME;
                        _trail.OLD_VALUE = item.OLD_VALUE;
                        _trail.NEW_VALUE = item.NEW_VALUE;
                        _trail.DESC = item.DESC;
                        _trail.CREATOR_ID = item.CREATOR_ID;
                        _trail.CREATE_DATETIME = DateTime.Now;
                        context.AUDIT_TRAIL.Add(_trail);
                    }
                    context.SaveChanges();
                }
            }
        }

        public void AddMergeTrail(List<MERGE_SPLIT_TRAIL> oMerge)
        {
            if (oMerge.Count > 0)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    foreach (var item in oMerge)
                    {
                        MERGE_SPLIT_TRAIL _mtrail = new MERGE_SPLIT_TRAIL();
                        _mtrail.OBJECT_ID = item.OBJECT_ID;
                        _mtrail.MERGE_ID = item.MERGE_ID;
                        _mtrail.TBL_NAME = item.TBL_NAME;
                        _mtrail.OLD_ITEM = item.OLD_ITEM;
                        _mtrail.OLD_VALUE = item.OLD_VALUE;
                        _mtrail.OLD_VALUE_DESC = item.OLD_VALUE_DESC;
                        _mtrail.NEW_ITEM = item.NEW_ITEM;
                        _mtrail.NEW_VALUE = item.NEW_VALUE;
                        _mtrail.NEW_VALUE_DESC = item.NEW_VALUE_DESC;
                        _mtrail.CREATOR_ID = item.CREATOR_ID;
                        _mtrail.CREATE_DATETIME = DateTime.Now;
                        _mtrail.STS = item.STS;
                        _mtrail.DESC = item.DESC;
                        context.MERGE_SPLIT_TRAIL.Add(_mtrail);
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}