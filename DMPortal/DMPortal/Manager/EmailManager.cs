﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;

namespace DMPortal.Manager
{
    public class EmailManager
    {
        #region "Declaration"
        public enum oBodyType
        {
            TEXT = 0,
            HTML = 1
        }
        string sToAdd;
        string sFromAdd;
        string sCCAdd;
        string sBCCAdd;
        string sBody;
        string sSubject;
        oBodyType sBodyType;
        string sSMTPServerName;
        int sSMTPServerPort;
        bool sSMTPSSL;
        string sSMTPCredential;
        string sSMTPPassword;
        #endregion
        ArrayList FilePaths = new ArrayList();

        #region "Properties"

        public oBodyType BodyType
        {
            get { return sBodyType; }
            set { sBodyType = value; }
        }

        public string ToAdd
        {
            get { return sToAdd; }
            set { sToAdd = value; }
        }

        public string From
        {
            get { return sFromAdd; }
            set { sFromAdd = value; }
        }

        public string CC
        {
            get { return sCCAdd; }
            set { sCCAdd = value; }
        }

        public string BCCAdd
        {
            get { return sBCCAdd; }
            set { sBCCAdd = value; }
        }

        public string Body
        {
            get { return sBody; }
            set { sBody = value; }
        }

        public string Subject
        {
            get { return sSubject; }
            set { sSubject = value; }
        }

        public string SMTPServerName
        {
            get { return sSMTPServerName; }
            set { sSMTPServerName = value; }
        }
        public int SMTPServerPort
        {
            get { return sSMTPServerPort; }
            set { sSMTPServerPort = value; }
        }
        public bool SMTPSSL
        {
            get { return sSMTPSSL; }
            set { sSMTPSSL = value; }
        }
        public string SMTPCredential
        {
            get { return sSMTPCredential; }
            set { sSMTPCredential = value; }
        }
        public string SMTPPassword
        {
            get { return sSMTPPassword; }
            set { sSMTPPassword = value; }
        }

        public void AddFilePaths(string value)
        {
            FilePaths.Add(value);
        }
        #endregion

        public void SendMail()
        {
            try
            {
                MailMessage oEmail = new MailMessage();

                if (!string.IsNullOrEmpty(sFromAdd))
                    oEmail.From = new MailAddress(sFromAdd);

                if (!string.IsNullOrEmpty(sToAdd))
                {
                    string[] strToArray = null;
                    strToArray = sToAdd.Split(new char[] { ';' });

                    int ToInt = strToArray.Length;
                    int Tocount = 0;

                    while (Tocount < ToInt)
                    {
                        oEmail.To.Add(new MailAddress(strToArray[Tocount]));
                        Tocount = Tocount + 1;
                    }
                }


                if (!string.IsNullOrEmpty(sCCAdd))
                {
                    string[] strccArray = null;
                    strccArray = sCCAdd.Split(new char[] { ';' });

                    int ccInt = strccArray.Length;
                    int ccCount = 0;

                    while (ccCount < ccInt)
                    {
                        oEmail.CC.Add(new MailAddress(strccArray[ccCount]));
                        ccCount = ccCount + 1;
                    }
                }

                if (!string.IsNullOrEmpty(sBCCAdd))
                {
                    string[] strBccArray = null;
                    strBccArray = sBCCAdd.Split(new char[] { ';' });

                    int BccInt = strBccArray.Length;
                    int BccCount = 0;

                    while (BccCount < BccInt)
                    {
                        oEmail.Bcc.Add(new MailAddress(strBccArray[BccCount]));
                        BccCount = BccCount + 1;
                    }
                }

                if (!string.IsNullOrEmpty(sBody))
                    oEmail.Subject = sSubject;
                if (!string.IsNullOrEmpty(sSubject))
                    oEmail.Body = sBody;

                if (!string.IsNullOrEmpty(sBodyType.ToString()))
                {
                    switch (sBodyType)
                    {
                        case oBodyType.HTML:
                            oEmail.IsBodyHtml = true;
                            break;
                        case oBodyType.TEXT:
                            oEmail.IsBodyHtml = false;
                            break;
                    }
                }

                if (FilePaths.Count > 0)
                {
                    foreach (string FilePath in FilePaths)
                    {
                        oEmail.Attachments.Add(new Attachment(FilePath));
                    }
                }

                string MailServerName = "smtp.gmail.com";
                SmtpClient mSmtpClient = new SmtpClient();
                mSmtpClient.Host = MailServerName;
                mSmtpClient.Port = 587;
                mSmtpClient.EnableSsl = true;
                //mSmtpClient.UseDefaultCredentials = false;
                mSmtpClient.Credentials = new NetworkCredential("c.chin85@gmail.com", "056915775");

                //string MailServerName = "192.168.1.4";
                //SmtpClient mSmtpClient = new SmtpClient();
                //mSmtpClient.Host = MailServerName;
                ////mSmtpClient.Port = sSMTPServerPort;
                //mSmtpClient.EnableSsl = sSMTPSSL;
                //mSmtpClient.Credentials = new NetworkCredential("los.tst@toyotacapital.com.my","");

                //string MailServerName = sSMTPServerName;
                //SmtpClient mSmtpClient = new SmtpClient();
                //mSmtpClient.Host = MailServerName;
                //mSmtpClient.Port = sSMTPServerPort;
                ////mSmtpClient.EnableSsl = sSMTPSSL;
                //mSmtpClient.UseDefaultCredentials = true;
                ////mSmtpClient.Credentials = new NetworkCredential(sSMTPCredential, sSMTPPassword);

                try
                {
                    mSmtpClient.Send(oEmail);
                }
                catch (Exception excep)
                {
                    throw excep;
                }

                mSmtpClient = null;

                foreach (Attachment aAttach in oEmail.Attachments)
                {
                    aAttach.Dispose();
                }

                oEmail.Attachments.Dispose();
                oEmail.Dispose();
                oEmail = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
