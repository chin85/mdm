﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPortal.Manager
{
    public class UserLog
    {
        public void AddUserLog(List<USER_LOG> oLog)
        {
            if (oLog.Count > 0)
            {
                using (MDMEntities context = new MDMEntities())
                {
                    foreach (var item in oLog)
                    {
                        USER_LOG _trail = new USER_LOG();
                        _trail.OBJECT_ID = item.OBJECT_ID;
                        _trail.OBJECT_NM = item.OBJECT_NM;
                        _trail.OBJECT_TYPE = item.OBJECT_TYPE;
                        _trail.LOG_DESC = item.LOG_DESC;
                        _trail.CREATOR_ID = item.CREATOR_ID;
                        _trail.LOG_DATETIME = DateTime.Now;
                        context.USER_LOG.Add(_trail);
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}