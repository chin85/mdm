﻿//using System;
//using System.Collections.Generic;
//using System.EnterpriseServices;
//using System.Linq;
//using System.Reflection;
//using System.Web;
//using System.ComponentModel;

//namespace ISMCUE.Manager
//{
//    public class EnumManager
//    {
//        public static class EnumManager
//        {
//            public static string GetDescription(this Enum value)
//            {
//                Type type = value.GetType();

//                FieldInfo fieldInfo = type.GetField(value.ToString());
//                if (fieldInfo != null)
//                {
//                    var descriptionAttribute = fieldInfo
//                        .GetCustomAttributes(typeof(DescriptionAttribute), false)
//                        .OfType<DescriptionAttribute>()
//                        .FirstOrDefault();

//                    if (descriptionAttribute != null)
//                        return descriptionAttribute.Description;

//                    return fieldInfo.Name;
//                }

//                return "";
//            }

//            public static List<CommonDataBoundItem> ConvertToDataBoundItems(Type itemType)
//            {
//                var list = new List<CommonDataBoundItem>();
//                foreach (var item in itemType.GetFields().Where(field => itemType.IsAssignableFrom(field.FieldType)))
//                {
//                    string description;
//                    var descriptionAtt = item.GetCustomAttributes(typeof(DescriptionAttribute), true).OfType<DescriptionAttribute>().FirstOrDefault();
//                    if (descriptionAtt == null)
//                        description = item.Name;
//                    else
//                        description = descriptionAtt.Description;

//                    list.Add(new CommonDataBoundItem(item.GetValue(null).ConvertTo<int>(), description));
//                }

//                return list;
//            }
//        }
//    }
//}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using DBO.Data.Objects;

namespace DMPortal.Manager
{
    public static class EnumManager
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();

            FieldInfo fieldInfo = type.GetField(value.ToString());
            if (fieldInfo != null)
            {
                var descriptionAttribute = fieldInfo
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .OfType<DescriptionAttribute>()
                    .FirstOrDefault();

                if (descriptionAttribute != null)
                    return descriptionAttribute.Description;

                return fieldInfo.Name;
            }

            return "";
        }

        public static List<CommonDataBoundItem> ConvertToDataBoundItems(Type itemType)
        {
            var list = new List<CommonDataBoundItem>();
            foreach (var item in itemType.GetFields().Where(field => itemType.IsAssignableFrom(field.FieldType)))
            {
                string description;
                var descriptionAtt = item.GetCustomAttributes(typeof(DescriptionAttribute), true).OfType<DescriptionAttribute>().FirstOrDefault();
                if (descriptionAtt == null)
                    description = item.Name;
                else
                    description = descriptionAtt.Description;

                list.Add(new CommonDataBoundItem(item.GetValue(null).ConvertTo<int>(), description));
            }

            return list;
        }
    }

}
