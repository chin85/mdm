﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Util;
using System.Xml;
using DMPortal.Object;

namespace DMPortal.Manager
{
    public class MachineKey
    {
        private static object s_initLock = new object();
        private const int HASH_SIZE = 20;
        //private static MachineKey.MachineKeyConfig s_config;
        private static SymmetricAlgorithm s_oDes;
        private static Stack s_oEncryptorStack;
        private static Stack s_oDecryptorStack;
        private static byte[] s_validationKey;
        private static byte[] s_ahexval;
        private static char[] s_acharval;

        //private static MachineKeyValidationMode ValidationMode
        //{
        //    get
        //    {
        //        MachineKey.EnsureConfig();
        //        return MachineKey.s_config.ValidationMode;
        //    }
        //}

        //internal static int ValidationKeyHashCode
        //{
        //    get
        //    {
        //        MachineKey.EnsureConfig();
        //        return MachineKey.ByteArrayToHexString(MachineKey.s_validationKey, MachineKey.s_validationKey.Length).GetHashCode();
        //    }
        //}

        private MachineKey()
        {
        }

        //internal static byte[] GetEncodedData(byte[] buf, byte[] modifier, int start, ref int length)
        //{
        //    MachineKey.EnsureConfig();
        //    if (MachineKey.s_config.ValidationMode != MachineKeyValidationMode.TripleDES)
        //    {
        //        byte[] numArray1 = MachineKey.HashData(buf, modifier, start, length);
        //        if (buf.Length - (start + length) >= numArray1.Length)
        //        {
        //            System.Buffer.BlockCopy((Array)numArray1, 0, (Array)buf, start + length, numArray1.Length);
        //            length += numArray1.Length;
        //            return buf;
        //        }
        //        byte[] numArray2 = new byte[(int)checked((uint)unchecked(length + numArray1.Length))];
        //        System.Buffer.BlockCopy((Array)buf, start, (Array)numArray2, 0, length);
        //        System.Buffer.BlockCopy((Array)numArray1, 0, (Array)numArray2, length, numArray1.Length);
        //        length += numArray1.Length;
        //        return numArray2;
        //    }
        //    byte[] numArray = MachineKey.EncryptOrDecryptData(true, buf, modifier, start, length);
        //    length = numArray.Length;
        //    return numArray;
        //}

        //internal static byte[] GetDecodedData(byte[] buf, byte[] modifier, int start, int length, ref int dataLength)
        //{
        //    MachineKey.EnsureConfig();
        //    if (MachineKey.s_config.ValidationMode != MachineKeyValidationMode.TripleDES)
        //    {
        //        byte[] numArray = MachineKey.HashData(buf, modifier, start, length - 20);
        //        for (int index = 0; index < numArray.Length; ++index)
        //        {
        //            if ((int)numArray[index] != (int)buf[start + length - 20 + index])
        //                throw new HttpException("Unable_to_validate_data");
        //        }
        //        dataLength = length - 20;
        //        return buf;
        //    }
        //    dataLength = -1;
        //    return MachineKey.EncryptOrDecryptData(false, buf, modifier, start, length);
        //}

        //internal static byte[] HashData(byte[] buf, byte[] modifier, int start, int length)
        //{
        //    MachineKey.EnsureConfig();
        //    byte[] numArray1 = MachineKey.s_config.ValidationMode != MachineKeyValidationMode.MD5 ? MachineKey.HMACSHA1HashForData(buf, modifier, start, length) : MachineKey.MD5HashForData(buf, modifier, start, length);
        //    if (numArray1.Length < 20)
        //    {
        //        byte[] numArray2 = new byte[20];
        //        System.Buffer.BlockCopy((Array)numArray1, 0, (Array)numArray2, 0, numArray1.Length);
        //        numArray1 = numArray2;
        //    }
        //    return numArray1;
        //}

        //internal static byte[] EncryptOrDecryptData(bool fEncrypt, byte[] buf, byte[] modifier, int start, int length)
        //{
        //    MachineKey.EnsureConfig();
        //    MemoryStream memoryStream = new MemoryStream();
        //    ICryptoTransform cryptoTransform = MachineKey.GetCryptoTransform(fEncrypt);
        //    CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, cryptoTransform, CryptoStreamMode.Write);
        //    cryptoStream.Write(buf, start, length);
        //    if (fEncrypt && modifier != null)
        //        cryptoStream.Write(modifier, 0, modifier.Length);
        //    cryptoStream.FlushFinalBlock();
        //    byte[] numArray1 = memoryStream.ToArray();
        //    cryptoStream.Close();
        //    MachineKey.ReturnCryptoTransform(fEncrypt, cryptoTransform);
        //    if (!fEncrypt && modifier != null)
        //    {
        //        byte[] numArray2 = new byte[(int)checked((uint)unchecked(numArray1.Length - modifier.Length))];
        //        System.Buffer.BlockCopy((Array)numArray1, 0, (Array)numArray2, 0, numArray2.Length);
        //        numArray1 = numArray2;
        //    }
        //    return numArray1;
        //}

        private static byte[] MD5HashForData(byte[] buf, byte[] modifier, int start, int length)
        {
            MD5 md5 = MD5.Create();
            int num = length + MachineKey.s_validationKey.Length;
            if (modifier != null)
                num += modifier.Length;
            else if (buf.Length - (start + length) >= MachineKey.s_validationKey.Length)
            {
                System.Buffer.BlockCopy((Array)MachineKey.s_validationKey, 0, (Array)buf, start + length, MachineKey.s_validationKey.Length);
                byte[] hash = md5.ComputeHash(buf);
                for (int index = start + length; index < start + length + MachineKey.s_validationKey.Length; ++index)
                    buf[index] = (byte)0;
                return hash;
            }
            byte[] buffer = new byte[(int)checked((uint)num)];
            System.Buffer.BlockCopy((Array)buf, start, (Array)buffer, 0, length);
            if (modifier != null)
            {
                System.Buffer.BlockCopy((Array)modifier, 0, (Array)buffer, length, modifier.Length);
                length += modifier.Length;
            }
            System.Buffer.BlockCopy((Array)MachineKey.s_validationKey, 0, (Array)buffer, length, MachineKey.s_validationKey.Length);
            return md5.ComputeHash(buffer);
        }

        //private static byte[] HMACSHA1HashForData(byte[] buf, byte[] modifier, int start, int length)
        //{
        //    //ExtendedHMACSHA1 hmacshA1 = ExtendedHMACSHA1.GetHMACSHA1();
            //HMACBuffer[] buffers;
            //if (modifier != null)
            //{
            //    buffers = new HMACBuffer[2];
            //    buffers[1].buffer = modifier;
            //    buffers[1].start = 0;
            //    buffers[1].length = modifier.Length;
            //}
            //else
            //    buffers = new HMACBuffer[1];
            //buffers[0].buffer = buf;
            //buffers[0].start = start;
            //buffers[0].length = length;
            //byte[] hash = hmacshA1.ComputeHash(buffers);
            //ExtendedHMACSHA1.ReturnHMACSHA1(hmacshA1);
            //return hash;
        //}

        //internal static string HashAndBase64EncodeString(string s)
        //{
        //    byte[] bytes = Encoding.Unicode.GetBytes(s);
        //    return Convert.ToBase64String(MachineKey.HashData(bytes, (byte[])null, 0, bytes.Length));
        //}

        internal static void DestroyByteArray(byte[] buf)
        {
            if (buf == null || buf.Length < 1)
                return;
            for (int index = 0; index < buf.Length; ++index)
                buf[index] = (byte)0;
        }

        internal static byte[] HexStringToByteArray(string str)
        {
            if ((str.Length & 1) == 1)
                return (byte[])null;
            byte[] numArray1 = MachineKey.s_ahexval;
            if (numArray1 == null)
            {
                numArray1 = new byte[103];
                int length = numArray1.Length;
                while (--length >= 0)
                {
                    if (48 <= length && length <= 57)
                        numArray1[length] = (byte)(length - 48);
                    else if (97 <= length && length <= 102)
                        numArray1[length] = (byte)(length - 97 + 10);
                    else if (65 <= length && length <= 70)
                        numArray1[length] = (byte)(length - 65 + 10);
                }
                MachineKey.s_ahexval = numArray1;
            }
            byte[] numArray2 = new byte[(int)checked((uint)unchecked(str.Length / 2))];
            int num1 = 0;
            int num2 = 0;
            int length1 = numArray2.Length;
            while (--length1 >= 0)
            {
                int num3;
                int num4;
                try
                {
                    byte[] numArray3 = numArray1;
                    string str1 = str;
                    int index1 = num1;
                    int num5 = 1;
                    num3 = index1 + num5;
                    int index2 = (int)str1[index1];
                    num4 = (int)numArray3[index2];
                }
                catch
                {
                    return (byte[])null;
                }
                int num6;
                try
                {
                    byte[] numArray3 = numArray1;
                    string str1 = str;
                    int index1 = num3;
                    int num5 = 1;
                    num1 = index1 + num5;
                    int index2 = (int)str1[index1];
                    num6 = (int)numArray3[index2];
                }
                catch
                {
                    return (byte[])null;
                }
                numArray2[num2++] = (byte)((num4 << 4) + num6);
            }
            return numArray2;
        }

        internal static unsafe string ByteArrayToHexString(byte[] buf, int iLen)
        {
            char[] chArray1 = MachineKey.s_acharval;
            if (chArray1 == null)
            {
                chArray1 = new char[16];
                int length = chArray1.Length;
                while (--length >= 0)
                    chArray1[length] = length >= 10 ? (char)(65 + (length - 10)) : (char)(48 + length);
                MachineKey.s_acharval = chArray1;
            }
            if (buf == null)
                return (string)null;
            if (iLen == 0)
                iLen = buf.Length;
            char[] chArray2 = new char[(int)checked((uint)unchecked(iLen * 2))];
            fixed (char* chPtr1 = &chArray2[0])
            fixed (char* chPtr2 = &chArray1[0])
            fixed (byte* numPtr1 = &buf[0])
            {
                char* chPtr3 = chPtr1;
                byte* numPtr2 = numPtr1;
                while (--iLen >= 0)
                {
                    char* chPtr4 = chPtr3;
                    int num1 = 2;
                    char* chPtr5 = (char*)((IntPtr)chPtr4 + num1);
                    int num2 = (int)*(ushort*)((IntPtr)chPtr2 + 2 * (((int)*numPtr2 & 240) >> 4));
                    *chPtr4 = (char)num2;
                    char* chPtr6 = chPtr5;
                    int num3 = 2;
                    chPtr3 = (char*)((IntPtr)chPtr6 + num3);
                    int num4 = (int)*(ushort*)((IntPtr)chPtr2 + 2 * ((int)*numPtr2 & 15));
                    *chPtr6 = (char)num4;
                    ++numPtr2;
                }
            }
            return new string(chArray2);
        }

        //private static void EnsureConfig()
        //{
        //    if (MachineKey.s_config != null)
        //        return;
        //    lock (MachineKey.s_initLock)
        //    {
        //        if (MachineKey.s_config != null)
        //            return;
        //        MachineKey.MachineKeyConfig local_0 = (MachineKey.MachineKeyConfig)HttpContext.GetAppConfig("system.web/machineKey");
        //        MachineKey.ConfigureEncryptionObject(local_0);
        //        MachineKey.s_config = local_0;
        //    }
        //}

        //private static void ConfigureEncryptionObject(MachineKey.MachineKeyConfig config)
        //{
        //    MachineKey.s_validationKey = config.ValidationKey;
        //    byte[] decryptionKey = config.DecryptionKey;
        //    ExtendedHMACSHA1.SetValidationKey(MachineKey.s_validationKey);
        //    config.DestroyKeys();
        //    if (config.AutogenKey)
        //    {
        //        try
        //        {
        //            MachineKey.s_oDes = (SymmetricAlgorithm)new TripleDESCryptoServiceProvider();
        //            MachineKey.s_oDes.Key = decryptionKey;
        //        }
        //        catch (Exception ex)
        //        {
        //            if (config.ValidationMode == MachineKeyValidationMode.TripleDES)
        //            {
        //                throw;
        //            }
        //            else
        //            {
        //                MachineKey.s_oDes = (SymmetricAlgorithm)new DESCryptoServiceProvider();
        //                byte[] numArray = new byte[8];
        //                System.Buffer.BlockCopy((Array)decryptionKey, 0, (Array)numArray, 0, 8);
        //                MachineKey.s_oDes.Key = numArray;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        MachineKey.s_oDes = decryptionKey.Length == 8 ? (SymmetricAlgorithm)new DESCryptoServiceProvider() : (SymmetricAlgorithm)new TripleDESCryptoServiceProvider();
        //        MachineKey.s_oDes.Key = decryptionKey;
        //    }
        //    MachineKey.s_oDes.IV = new byte[8];
        //    MachineKey.s_oEncryptorStack = new Stack();
        //    MachineKey.s_oDecryptorStack = new Stack();
        //    MachineKey.DestroyByteArray(decryptionKey);
        //}

        //private static ICryptoTransform GetCryptoTransform(bool fEncrypt)
        //{
        //    Stack stack = fEncrypt ? MachineKey.s_oEncryptorStack : MachineKey.s_oDecryptorStack;
        //    lock (stack)
        //    {
        //        if (stack.Count > 0)
        //            return (ICryptoTransform)stack.Pop();
        //    }
        //    lock (MachineKey.s_oDes)
        //        return fEncrypt ? MachineKey.s_oDes.CreateEncryptor() : MachineKey.s_oDes.CreateDecryptor();
        //}

        //private static void ReturnCryptoTransform(bool fEncrypt, ICryptoTransform ct)
        //{
        //    Stack stack = fEncrypt ? MachineKey.s_oEncryptorStack : MachineKey.s_oDecryptorStack;
        //    lock (stack)
        //    {
        //        if (stack.Count > 100)
        //            return;
        //        stack.Push((object)ct);
        //    }
        //}

        //internal static object CreateConfig(object parent, object configContextObj, XmlNode section)
        //{
        //    return (object)new MachineKey.MachineKeyConfig(parent, configContextObj, section);
        //}

        //private class MachineKeyConfig
        //{
        //    private MachineKeyValidationMode _ValidationMode;
        //    private bool _AutogenKey;
        //    private byte[] _ValidationKey;
        //    private byte[] _DecryptionKey;

        //    internal byte[] ValidationKey
        //    {
        //        get
        //        {
        //            return (byte[])this._ValidationKey.Clone();
        //        }
        //    }

        //    internal byte[] DecryptionKey
        //    {
        //        get
        //        {
        //            return (byte[])this._DecryptionKey.Clone();
        //        }
        //    }

        //    internal MachineKeyValidationMode ValidationMode
        //    {
        //        get
        //        {
        //            return this._ValidationMode;
        //        }
        //    }

        //    internal bool AutogenKey
        //    {
        //        get
        //        {
        //            return this._AutogenKey;
        //        }
        //    }

        //    internal MachineKeyConfig(object parentObject, object contextObject, XmlNode node)
        //    {
        //        MachineKey.MachineKeyConfig machineKeyConfig = (MachineKey.MachineKeyConfig)parentObject;
        //        if (HandlerBase.IsPathAtAppLevel((contextObject as HttpConfigurationContext).VirtualPath) == PathLevel.BelowApp)
        //            throw new ConfigurationException(HttpRuntime.FormatResourceString("No_MachineKey_Config_In_subdir"), node);
        //        if (machineKeyConfig != null)
        //        {
        //            this._ValidationKey = machineKeyConfig.ValidationKey;
        //            this._DecryptionKey = machineKeyConfig.DecryptionKey;
        //            this._ValidationMode = machineKeyConfig.ValidationMode;
        //            this._AutogenKey = machineKeyConfig.AutogenKey;
        //        }
        //        XmlNode node1 = node.Attributes.RemoveNamedItem("validationKey");
        //        XmlNode node2 = node.Attributes.RemoveNamedItem("decryptionKey");
        //        int val = 0;
        //        string[] values = new string[3]
        //{
        //  "SHA1",
        //  "MD5",
        //  "3DES"
        //};
        //        if (HandlerBase.GetAndRemoveEnumAttribute(node, "validation", values, ref val) != null)
        //            this._ValidationMode = (MachineKeyValidationMode)val;
        //        HandlerBase.CheckForUnrecognizedAttributes(node);
        //        HandlerBase.CheckForChildNodes(node);
        //        if (node1 != null && node1.Value != null)
        //        {
        //            string str = node1.Value;
        //            bool flag = str.EndsWith(",IsolateApps");
        //            if (flag)
        //                str = str.Substring(0, str.Length - ",IsolateApps".Length);
        //            if (str == "AutoGenerate")
        //            {
        //                this._ValidationKey = new byte[64];
        //                System.Buffer.BlockCopy((Array)HttpRuntime.s_autogenKeys, 0, (Array)this._ValidationKey, 0, 64);
        //            }
        //            else
        //            {
        //                if (str.Length > 128 || str.Length < 40)
        //                    throw new ConfigurationException(HttpRuntime.FormatResourceString("Unable_to_get_cookie_authentication_validation_key", str.Length.ToString()), node1);
        //                this._ValidationKey = MachineKey.HexStringToByteArray(str);
        //                if (this._ValidationKey == null)
        //                    throw new ConfigurationException(HttpRuntime.FormatResourceString("Invalid_validation_key"), node1);
        //            }
        //            if (flag)
        //            {
        //                int hashCode = SymbolHashCodeProvider.Default.GetHashCode((object)HttpContext.Current.Request.ApplicationPath);
        //                this._ValidationKey[0] = (byte)(hashCode & (int)byte.MaxValue);
        //                this._ValidationKey[1] = (byte)((hashCode & 65280) >> 8);
        //                this._ValidationKey[2] = (byte)((hashCode & 16711680) >> 16);
        //                this._ValidationKey[3] = (byte)(((long)hashCode & 4278190080L) >> 24);
        //            }
        //        }
        //        if (node2 == null)
        //            return;
        //        string str1 = node2.Value;
        //        bool flag1 = str1.EndsWith(",IsolateApps");
        //        if (flag1)
        //            str1 = str1.Substring(0, str1.Length - ",IsolateApps".Length);
        //        if (str1 == "AutoGenerate")
        //        {
        //            this._DecryptionKey = new byte[24];
        //            System.Buffer.BlockCopy((Array)HttpRuntime.s_autogenKeys, 64, (Array)this._DecryptionKey, 0, 24);
        //            this._AutogenKey = true;
        //        }
        //        else
        //        {
        //            this._AutogenKey = false;
        //            if (str1.Length == 48)
        //            {
        //                TripleDESCryptoServiceProvider cryptoServiceProvider = (TripleDESCryptoServiceProvider)null;
        //                try
        //                {
        //                    cryptoServiceProvider = new TripleDESCryptoServiceProvider();
        //                }
        //                catch (Exception ex)
        //                {
        //                }
        //                if (cryptoServiceProvider == null)
        //                    throw new ConfigurationException(HttpRuntime.FormatResourceString("cannot_use_Triple_DES"), node2);
        //            }
        //            if (str1.Length != 48 && str1.Length != 16)
        //                throw new ConfigurationException(HttpRuntime.FormatResourceString("Unable_to_get_cookie_authentication_decryption_key", str1.Length.ToString()), node2);
        //            this._DecryptionKey = MachineKey.HexStringToByteArray(str1);
        //            if (this._DecryptionKey == null)
        //                throw new ConfigurationException(HttpRuntime.FormatResourceString("Invalid_decryption_key"), node2);
        //        }
        //        if (!flag1)
        //            return;
        //        int hashCode1 = SymbolHashCodeProvider.Default.GetHashCode((object)HttpContext.Current.Request.ApplicationPath);
        //        this._DecryptionKey[0] = (byte)(hashCode1 & (int)byte.MaxValue);
        //        this._DecryptionKey[1] = (byte)((hashCode1 & 65280) >> 8);
        //        this._DecryptionKey[2] = (byte)((hashCode1 & 16711680) >> 16);
        //        this._DecryptionKey[3] = (byte)(((long)hashCode1 & 4278190080L) >> 24);
        //    }

        //    internal void DestroyKeys()
        //    {
        //        MachineKey.DestroyByteArray(this._ValidationKey);
        //        MachineKey.DestroyByteArray(this._DecryptionKey);
        //    }
        //}
    }
}