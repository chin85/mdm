﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace DMPortal.Manager
{
    public class XMLManager
    {
        public static void GenerateXMLFile(string path, string filename, object item)
        {
            try
            {
                XmlSerializer x = new XmlSerializer(item.GetType());
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };

                XmlWriter xmlWriter2 = XmlWriter.Create(path + filename, settings);
                x.Serialize(xmlWriter2, item);
                xmlWriter2.Close();
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static byte[] ConvertToByte(object item)
        {
            BinaryReader binReader = null;
            try
            {
                XmlSerializer x = new XmlSerializer(item.GetType());
                MemoryStream memoryStream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };

                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                x.Serialize(xmlWriter, item);


                //XmlWriter xmlWriter2 = XmlWriter.Create(@"c:\abc.xml", settings);
                //x.Serialize(xmlWriter2, item);
                //xmlWriter2.Close();

                binReader = new BinaryReader(memoryStream);
                binReader.BaseStream.Position = 0;
                byte[] binFile = binReader.ReadBytes(Convert.ToInt32(binReader.BaseStream.Length));

                return binFile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (binReader != null)
                {
                    binReader.Close();
                    binReader.Dispose();
                    binReader = null;
                }
            }
        }

        public static string ConvertToStr(object item)
        {

            StreamReader streamReader = null;

            try
            {
                XmlSerializer x = new XmlSerializer(item.GetType());
                MemoryStream memoryStream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    Indent = true
                };

                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                x.Serialize(xmlWriter, item);

                XmlWriter xmlWriter2 = XmlWriter.Create(@"c:\def.xml", settings);
                x.Serialize(xmlWriter2, item);
                xmlWriter2.Close();

                memoryStream.Position = 0;
                streamReader = new StreamReader(memoryStream);
                var myStr = streamReader.ReadToEnd();

                return myStr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                    streamReader.Dispose();
                    streamReader = null;
                }
            }
        }
    }
}