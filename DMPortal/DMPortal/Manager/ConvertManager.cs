﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using DMPortal.Object;

namespace DMPortal.Manager
{
    public static class ConvertManager
    {
        public static string ConvertTo(this object value)
        {
            return value.ConvertTo<string>();
        }
        public static T ConvertTo<T>(this object value)
        {
            var result = value.ConvertToTargetTypeValue(typeof(T));
            if (result == null)
                return default(T);
            else
                return (T)result;
        }
        public static object ConvertToTargetTypeValue(this object value, Type targetType)
        {
            if (value == null ||
                (string.Equals(value, "") &&
                 (typeof(decimal?).IsAssignableFrom(targetType) ||
                  typeof(short?).IsAssignableFrom(targetType) ||
                  typeof(int?).IsAssignableFrom(targetType) ||
                  typeof(long?).IsAssignableFrom(targetType) ||
                  typeof(byte?).IsAssignableFrom(targetType) ||
                  typeof(bool?).IsAssignableFrom(targetType) ||
                  typeof(double?).IsAssignableFrom(targetType) ||
                  typeof(DateTime?).IsAssignableFrom(targetType) ||
                  typeof(Time).IsAssignableFrom(targetType) ||
                  typeof(Guid?).IsAssignableFrom(targetType))))
                return null;
            else if (targetType.IsAssignableFrom(value.GetType()))
                return value;
            else if (typeof(decimal?).IsAssignableFrom(targetType))
            {
                if (value is string && value.ConvertTo<string>().StartsWith("("))
                    return Convert.ToDecimal(value.ConvertTo<string>().Replace("(", "-").Replace(")", ""));
                else
                    return Convert.ToDecimal(value);
            }
            else if (typeof(short?).IsAssignableFrom(targetType))
            {
                if (value is string && value.ConvertTo<string>().StartsWith("("))
                    return Convert.ToInt16(value.ConvertTo<string>().Replace("(", "-").Replace(")", ""));
                else
                    return Convert.ToInt16(value);
            }
            else if (typeof(int?).IsAssignableFrom(targetType))
            {
                if (value is string && value.ConvertTo<string>().StartsWith("("))
                    return Convert.ToInt32(value.ConvertTo<string>().Replace("(", "-").Replace(")", ""));
                else
                    return Convert.ToInt32(value);
            }
            else if (typeof(long?).IsAssignableFrom(targetType))
            {
                if (value is string && value.ConvertTo<string>().StartsWith("("))
                    return Convert.ToInt64(value.ConvertTo<string>().Replace("(", "-").Replace(")", ""));
                else
                    return Convert.ToInt64(value);
            }
            else if (typeof(byte?).IsAssignableFrom(targetType))
                return Convert.ToByte(value);
            else if (typeof(bool?).IsAssignableFrom(targetType))
            {
                if (value is string &&
                    !object.Equals(value.ConvertTo<string>().ToUpper(), bool.TrueString.ToUpper()) &&
                    !object.Equals(value.ConvertTo<string>().ToUpper(), bool.FalseString.ToUpper()))
                    return Convert.ToBoolean(Convert.ToByte(value));
                return Convert.ToBoolean(value);
            }
            else if (typeof(double?).IsAssignableFrom(targetType))
            {
                if (value is string && value.ConvertTo<string>().StartsWith("("))
                    return Convert.ToDouble(value.ConvertTo<string>().Replace("(", "-").Replace(")", ""));
                else
                    return Convert.ToDouble(value);
            }
            else if (typeof(DateTime?).IsAssignableFrom(targetType))
            {
                try
                {
                    DateTime? result;
                    var cultureInfo = new CultureInfo("en-US");
                    cultureInfo.DateTimeFormat.ShortDatePattern = ConfigurationManager.AppSettings["SystemDateFormat"];
                    result = Convert.ToDateTime(value.ToString(), cultureInfo);
                    return result;
                }
                catch
                {
                    return null;
                }
            }
            else if (typeof(Time).IsAssignableFrom(targetType))
            {
                return new Time(value.ConvertTo<DateTime?>().Value);
            }
            else if (typeof(Enum).IsAssignableFrom(targetType))
            {
                try
                {
                    return Enum.Parse(targetType, Convert.ToString(value).Trim());
                }
                catch
                {
                    foreach (var field in targetType.GetFields())
                    {
                        if (field.Name.ToUpper() == Convert.ToString(value).Trim().ToUpper())
                            return Enum.Parse(targetType, field.Name);
                        var description = field.GetCustomAttributes(typeof(DescriptionAttribute), true).OfType<DescriptionAttribute>().FirstOrDefault();
                        if (description != null && string.Equals(description.Description, value))
                            return Enum.Parse(targetType, field.Name);
                    }
                    throw new ArgumentOutOfRangeException("value");
                }
            }
            else if (typeof(Type).IsAssignableFrom(targetType))
            {
                try
                {
                    return Type.GetType(value.ConvertTo<string>());
                }
                catch
                {
                    return null;
                }
            }
            else if (typeof(Guid?).IsAssignableFrom(targetType))
            {
                return Guid.Parse(value.ConvertTo<string>());
            }
            else if (typeof(string).IsAssignableFrom(targetType))
            {
                if (value is DateTime?)
                {
                    DateTime date = (value as DateTime?).Value;
                    if (date.Hour > 0 || date.Minute > 0 || date.Second > 0)
                        return date.ToString(ConfigurationManager.AppSettings["SystemDateTimeFormat"]);
                    else
                        return date.ToString(ConfigurationManager.AppSettings["SystemDateFormat"]);
                }
                else
                {
                    var result = Convert.ToString(value);
                    if (result.StartsWith("-"))
                    {
                        if (value is short? ||
                            value is int? ||
                            value is long? ||
                            value is decimal? ||
                            value is double?)
                            result = string.Format("({0})", result.Replace("-", ""));
                    }

                    return result;
                }
            }
            else
                return Convert.ToString(value);
        }

        public static string ToSummaryString(this string value)
        {
            if (value.Length > 35)
                return string.Format("{0}...", value.Substring(0, 35, true));
            return value;
        }

        public static object GetDefaultValue(this Type type)
        {
            object item = null;
            if (type.IsValueType)
                item = Activator.CreateInstance(type);
            else
            {
                var constructor = type.GetConstructors().FirstOrDefault();
                if (constructor == null)
                    return item;

                var parameterInfos = constructor.GetParameters();
                if (parameterInfos.Length > 0)
                {
                    List<object> parameters = new List<object>();
                    foreach (var parameterInfo in parameterInfos)
                        parameters.Add(parameterInfo.ParameterType.GetDefaultValue());

                    try
                    {
                        item = Activator.CreateInstance(type, parameters.ToArray());
                    }
                    catch { }
                }
                else
                    item = Activator.CreateInstance(type);
            }

            return item;
        }

        public static string DefaultFormat(this DateTime date)
        {
            return date.ToString("dd/MM/yyyy HH:mm:ss");
        }
        public static string DefaultFormat(this DateTime? date)
        {
            if (date.HasValue)
                return DefaultFormat(date.Value);
            else
                return string.Empty;
        }

        public static string ConvertToAudatexFormat<T>(this T value)
        {
            return value.ConvertToAudatexFormat(false);
        }
        public static string ConvertToAudatexFormat<T>(this T value, bool isYear)
        {

            if (typeof(T).IsAssignableFrom(typeof(bool?)))
            {
                if (value.ConvertTo<bool>())
                    return "1";
                else
                    return "0";
            }
            else if (typeof(T).IsAssignableFrom(typeof(double?)) ||
               typeof(T).IsAssignableFrom(typeof(decimal?))
                )
            {
                string strValue = String.Format("{0:0.00}", value.ConvertTo<decimal>());
                return strValue.Replace(".", "");
            }
            else if (typeof(T).IsAssignableFrom(typeof(DateTime?)))
            {
                if (isYear)
                {
                    if (value.ConvertTo<DateTime?>() == null)
                    {

                        return " 0/ 0/ 0";

                    }
                    else
                    {
                        return " 0/ 0/" + value.ConvertTo<DateTime>().Year.ConvertTo<string>();
                    }
                }
                else
                {
                    if (value.ConvertTo<DateTime?>() == null)
                    {
                        return " 0/ 0/ 0";
                    }
                    else
                    {
                        return value.ConvertTo<string>();
                    }
                }
            }
            else if (typeof(T).IsAssignableFrom(typeof(Time)))
            {
                if (value.ConvertTo<Time>() == null)
                {
                    return "0: 0: 0.   0";
                }
                else
                {
                    return value.ConvertTo<string>() + ":00.0000";
                }
            }
            else if (typeof(T).IsAssignableFrom(typeof(int?)) ||
                   typeof(T).IsAssignableFrom(typeof(short?)) ||
                   typeof(T).IsAssignableFrom(typeof(long?)) ||
                   typeof(T).IsAssignableFrom(typeof(byte?))
            )
            {
                if (value == null)
                {
                    return "0";
                }
                else
                {
                    return value.ConvertTo<string>();
                }
            }
            else
            {
                if (value == null)
                {
                    return "";
                }
                else
                {
                    return value.ConvertTo<string>();
                }
            }
        }
    }
}


