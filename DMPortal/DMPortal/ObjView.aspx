﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ObjView.aspx.cs" Inherits="DMPortal.ObjView" %>

<%@ Register Src="Controls/Alerts/UserAlert.ascx" TagName="UserAlert" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="panel panel-default table-responsive">
            <div class="panel-heading">
                <h4>Object Table View</h4>
            </div>
        </div>
        <div>
            <fieldset>
                <div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <table runat="server">
                                <tr>
                                    <td class="col-4 control-label" style="width: 120px">Project : </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlproj" runat="server" CssClass="form-control" Width="290px" AutoPostBack="true" OnSelectedIndexChanged="ddlproj_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-4 control-label" style="width: 120px">Object : </td>
                                    <td colspan="3" class="auto-style1" style="width: 300px; text-align: left">
                                        <asp:DropDownList ID="ddlobjid" runat="server" CssClass="form-control" Width="290px" OnSelectedIndexChanged="ddlobjid_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td colspan="3" class="auto-style1" style="width: 100px; text-align: center">
                                        <asp:Button ID="ObjBtn" runat="server" OnClick="Obj_Search" Text="Search" CssClass="btn btn-success" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </fieldset>
        </div>
        <br />
        <div>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="gv" runat="server" CssClass="table table-hover table-bordered" EmptyDataText="No Records.">
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-md-12" style="text-align: right">
            <div style="margin: auto">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <legend>Export Attribute</legend>

                        <div id="ldiv" runat="server">
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">Primary Key :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddlpk" runat="server" CssClass="form-control">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">*Export To (DB) :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddlextdb" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlextdb_SelectedIndexChanged">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="ddlextdb" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>

                            </div>
                        </div>
                         <div>

                            <div class="col-md-4" style="text-align: right">
                               
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:Label ID="Label1" runat="server" Text="* Remark : Kindly please select desire table to publish or else table created as default object name." Font-Italic="true"></asp:Label>

                            </div>
                        </div>

                        <div>

                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">Export To (Table) :</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ExtTbl" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div>
                            <div class="col-md-4" style="text-align: right">
                                <label class="control-label" style="text-align: right" for="textinput">*Export Type:</label>
                            </div>

                            <div class="col-md-8" style="text-align: left">
                                <asp:DropDownList ID="ddltype" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">- SELECT -</asp:ListItem>
                                    <asp:ListItem Value="O">Overwrite / Replace Table</asp:ListItem>
                                    <asp:ListItem Value="A">Append to Existing Table</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddltype" InitialValue="0" runat="server" ForeColor="red" ErrorMessage="Can not be empty."></asp:RequiredFieldValidator>

                            </div>
                        </div>

                        <asp:Button ID="btnPublish" runat="server" Text="Publish" CssClass="btn btn-warning" OnClick="btnPublish_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc1:UserAlert runat="server" ID="UserAlert" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
