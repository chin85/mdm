﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class RefValue2 : System.Web.UI.Page
    {
        public string logonId { get; set; }
        public string isChecker { get; set; }
        public string isMaker { get; set; }
        public int userId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;
            this.isChecker = oDOSession.isChecker;
            this.isMaker = oDOSession.isMaker;
            this.userId = oDOSession.Id;

            if (!Page.IsPostBack)
            {
                lblErrormsg.Text = string.Empty;
                ViewState["PageIndex"] = 0;
                ViewState["searchtxt"] = string.Empty;
                ViewState["att"] = string.Empty;
                bindDLL();
                //BindGrid();
            }
        }

        private void BindGrid()
        {
            string txt = ViewState["searchtxt"].ToString();
            string searchby = ViewState["att"].ToString();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetObjAttVal", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = ddlobj.SelectedValue.ConvertTo<int>();
                cmd.Parameters.Add("@Searchby", SqlDbType.VarChar, 50).Value = searchby;//searchby == string.Empty ? null: searchby;
                cmd.Parameters.Add("@Searchval", SqlDbType.VarChar, 50).Value = txt;//txt == string.Empty ? null : txt;
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "Obj");
                DataTable dt = ds.Tables["Obj"];
                Session["TaskTable"] = dt;

                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables["Obj"].Rows.Count > 0)
                    {
                        gv.DataSource = ds.Tables["Obj"];
                        gv.DataBind();

                        if (string.IsNullOrEmpty(ds.Tables["Obj"].Rows[0]["row_id"].ToString()))
                        {
                            gv.Rows[0].Visible = false;
                        }
                    }
                }
                con.Close();
            }
        }

        private string LoadUDFFile()
        {
            string Path = ConfigurationManager.AppSettings["UDFDir"].ToString();
            string text = string.Empty;
            if (File.Exists(Path))
            {
                text = System.IO.File.ReadAllText(Path);
            }

            return text;
        }

        private void LoadvalidationRule()
        {
            ViewState["AttRules"] = null;
            int l = 0;
            int objid = ddlobj.SelectedValue.ConvertTo<int>();

            using (MDMEntities db = new MDMEntities())
            {
                var att = db.Ref_Obj_Att.Where(i => i.obj_id == objid).ToList();
                var rule = att.Select(j => j.ref_rule_id).Distinct().ToList();
                string[] sVRule = new string[att.Count()];
                List<String> lrules = new List<string>();

                if (rule.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("ref_rule_id");
                    dt.Columns.Add("obj_attribute");
                    dt.Columns.Add("rule_no");
                    dt.Columns.Add("rules_colnm");
                    dt.Columns.Add("rules_desc");
                    dt.Columns.Add("rules");
                    dt.Columns.Add("DB_ref_tbl");
                    dt.Columns.Add("DB_ref_field");
                    dt.Columns.Add("att_desc");

                    DataTable dtrule = new DataTable();
                    dtrule.Columns.Add("ref_rule_id");
                    dtrule.Columns.Add("obj_attribute");
                    dtrule.Columns.Add("rule_no");
                    dtrule.Columns.Add("rules_colnm");
                    dtrule.Columns.Add("rules_desc");
                    dtrule.Columns.Add("rules");
                    dtrule.Columns.Add("DB_ref_tbl");
                    dtrule.Columns.Add("DB_ref_field");
                    dtrule.Columns.Add("att_desc");

                    string[] sRule = new string[att.Count()];
                    foreach (var item in rule)
                    {
                        //var ruleid = att.SingleOrDefault(i => i.ID == item.ID);
                        var rulefile = db.Ref_Rule.Where(m => m.ID == item.Value).SingleOrDefault();
                        var attfule = db.Ref_Obj_Att.Where(k => k.obj_id == objid && k.ref_rule_id == item.Value).ToList();
                        string[] array = new string[attfule.Count];

                        if (rulefile != null)
                        {
                            for (l = 0; l < attfule.Count; l++)
                            {
                                array[l] = attfule[l].ref_rule_id + "|" + attfule[l].obj_attribute + "|" + attfule[l].field_no + "|" + attfule[l].rule_row + "|" + attfule[l].rule_col + "|" + attfule[l].att_desc;

                            }

                            sRule = readValidationRule(rulefile.File_Path, array);

                            if (sRule != null)
                            {
                                for (int n = 0; n < sRule.GetLength(0); n++)
                                {
                                    lrules.Add(sRule[n]);
                                    //string[] result = sRule[n].Split('|');
                                    //DataRow dr = dt.NewRow();
                                    //dr["ref_rule_id"] = result[0];
                                    //dr["obj_attribute"] = result[1];
                                    //dr["rule_no"] = result[2];
                                    //dr["rules_colnm"] = result[3];
                                    //dr["rules_desc"] = result[4];
                                    //dr["rules"] = result[5];
                                    //dt.Rows.Add(dr);
                                }
                            }
                        }
                    }

                    //read all rule into datatable
                    foreach (var irule in lrules)
                    {
                        string[] result = irule.Split('|');
                        DataRow dr = dtrule.NewRow();
                        dr["ref_rule_id"] = result[0];
                        dr["obj_attribute"] = result[1];
                        dr["rule_no"] = result[2];
                        dr["rules_colnm"] = result[3];
                        dr["rules_desc"] = result[4];
                        dr["rules"] = result[5];
                        dr["DB_ref_tbl"] = result[6];
                        dr["DB_ref_field"] = result[7];
                        dr["att_desc"] = result[8];
                        dtrule.Rows.Add(dr);
                    }

                    //assign rule to each att
                    foreach (var ratt in att)
                    {
                        var rules = dtrule.Select("").FirstOrDefault(x => (string)x["obj_attribute"] == ratt.obj_attribute);

                        if (rules != null)
                        {
                            DataRow dr = dt.NewRow();
                            dr["ref_rule_id"] = rules["ref_rule_id"].ToString();
                            dr["obj_attribute"] = rules["obj_attribute"].ToString();
                            dr["rule_no"] = rules["rule_no"].ToString();
                            dr["rules_colnm"] = rules["rules_colnm"].ToString();
                            dr["rules_desc"] = rules["rules_desc"].ToString();
                            dr["rules"] = rules["rules"].ToString();
                            dr["DB_ref_tbl"] = rules["DB_ref_tbl"].ToString();
                            dr["DB_ref_field"] = rules["DB_ref_field"].ToString();
                            dr["att_desc"] = rules["att_desc"].ToString();
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataRow dr = dt.NewRow();
                            dr["ref_rule_id"] = 0;
                            dr["obj_attribute"] = ratt.obj_attribute;
                            dr["rule_no"] = "";
                            dr["rules_colnm"] = "";
                            dr["rules_desc"] = "";
                            dr["rules"] = "";
                            dr["DB_ref_tbl"] = "";
                            dr["DB_ref_field"] = "";
                            dr["att_desc"] = "";
                            dt.Rows.Add(dr);
                        }
                    }

                    ViewState["AttRules"] = dt;
                }
            }
        }

        private string[] readValidationRule(string filepath, string[] array)
        {
            string[] sRule = new string[array.Length];

            //if (File.Exists(filepath))
            //{
            //    Microsoft.Office.Interop.Excel.Application xlexcel;
            //    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            //    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            //    int Cnum = 0;
            //    Microsoft.Office.Interop.Excel.Range xlRange;
            //    int Rnum = 0;

            //    object misValue = System.Reflection.Missing.Value;
            //    xlexcel = new Microsoft.Office.Interop.Excel.Application();

            //    xlexcel.Visible = false;

            //    //Open a File
            //    xlWorkBook = xlexcel.Workbooks.Open(filepath, 0, false, 5, "", "", true,
            //    Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

            //    //Set Sheet 1 as the sheet you want to work with
            //    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //    //gives the used cells in sheet
            //    xlRange = xlWorkSheet.UsedRange;

            //    int rowCount = xlRange.Rows.Count;
            //    int colCount = xlRange.Columns.Count;
            //    int colRule = 0;
            //    string strRule;

            //    if (colCount > 0 && rowCount > 0)
            //    {
            //        for (int i = 0; i < array.GetLength(0); i++)
            //        {
            //            string[] attdtl = array[i].Split('|');
            //            Rnum = attdtl[3].ConvertTo<int>();
            //            Cnum = attdtl[4].ConvertTo<int>();

            //            if ((xlRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value2 == null)
            //                sRule[i] = attdtl[0] + "|" + attdtl[1] + "|1|" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|" + string.Empty;
            //            else
            //                sRule[i] = attdtl[0] + "|" + attdtl[1] + "|" + (xlRange.Cells[Rnum, 7] as Microsoft.Office.Interop.Excel.Range).Value2.ToString() + "|" + (xlRange.Cells[Rnum, 4] as Microsoft.Office.Interop.Excel.Range).Value2.ToString() + "|" + (xlRange.Cells[Rnum, Cnum - 1] as Microsoft.Office.Interop.Excel.Range).Value2.ToString() + "|" + (xlRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value2.ToString() + "|" + (xlRange.Cells[Rnum, 14] as Microsoft.Office.Interop.Excel.Range).Value + "|" + (xlRange.Cells[Rnum, 15] as Microsoft.Office.Interop.Excel.Range).Value2;
            //        }
            //    }
            //    xlWorkBook.Close();
            //    xlexcel.Quit();
            //}

            int Cnum = 0;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                string[] attdtl = array[i].Split('|');
                Cnum = attdtl[4].ConvertTo<int>();

                if (Cnum > 0)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        var oruledtl = context.Ref_Rule_Dtl.Where(k => k.ID == Cnum).SingleOrDefault();

                        sRule[i] = attdtl[0] + "|" + attdtl[1] + "|" + oruledtl.Rule_No + "|" + oruledtl.Field_Name + "|" + oruledtl.Rules + "|" + oruledtl.Visual_Basic + "|" + oruledtl.DB_Ref_Table + "|" + oruledtl.DB_Ref_Field + "|" + attdtl[5];
                    }
                }
                else
                    sRule[i] = attdtl[0] + "|" + attdtl[1] + "|1|" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|" + string.Empty;
            }

            return sRule;
        }

        private void bindLov(DropDownList dl, int attributeid)
        {
            using (MDMEntities db = new MDMEntities())
            {
                var lovVal = (from c in db.Ref_Obj_Value
                              where c.obj_Att_id == attributeid
                              select new { c.ID, objDESC = c.obj_value }).OrderBy(i => i.objDESC).ToList();

                if (lovVal.Count > 0)
                {
                    foreach (var item in lovVal)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(dl, datatext, datavalue);
                    }
                    dl.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindDLL()
        {
            using (MDMEntities db = new MDMEntities())
            {
                //    var role = (from c in db.Ref_Obj
                //                select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                //    if (role.Count > 0)
                //    {
                //        foreach (var item in role)
                //        {
                //            var datavalue = item.ID.ToString();
                //            var datatext = item.objDESC;
                //            bindddlobject(ddlobj, datatext, datavalue);
                //        }
                //        ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                //    }

                var role = (from c in db.Ref_Proj
                            select new { c.ID, objDESC = c.Project }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void bindddlobject(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void ddlobj_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadUDFFile();
            ddlatt.Items.Clear();
            txtsearch.Text = string.Empty;
            ViewState["searchtxt"] = string.Empty;
            ViewState["att"] = string.Empty;
            LoadvalidationRule();
            bindAtt();
            BindGrid();
        }

        private void bindAtt()
        {
            ddlatt.Items.Clear();
            var _obj = ddlobj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {
                var att = (from c in db.Ref_Obj_Att
                           where c.obj_id == _obj
                           select new { c.obj_attribute, objDESC = c.obj_attribute + " - " + c.att_desc }).OrderBy(i => i.objDESC).ToList();

                if (att.Count > 0)
                {
                    foreach (var item in att)
                    {
                        var datavalue = item.obj_attribute;
                        var datatext = item.objDESC;
                        bindddlobject(ddlatt, datatext, datavalue);
                    }
                    ddlatt.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void gv_RowCreated(object sender, GridViewRowEventArgs e)
        {
            int cellCnt = e.Row.Cells.Count;
            int objid = ddlobj.SelectedValue.ConvertTo<int>();

            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                LinkButton lnkFooter = new LinkButton();
                lnkFooter.Text = "Add New";
                lnkFooter.CommandName = "Insert";
                lnkFooter.CssClass = "btn btn-success";
                lnkFooter.ID = "lnkAdd";
                e.Row.Cells[0].Controls.Add(lnkFooter);
                int j = 0;

                List<Ref_Obj_Att> oAtt = new List<Ref_Obj_Att>();
                using (MDMEntities db = new MDMEntities())
                {
                    oAtt = db.Ref_Obj_Att.Where(i => i.obj_id == objid).ToList();
                }

                for (int i = 3; i < cellCnt; i++)
                //for (int i = 2; i < cellCnt; i++)
                {
                    var olov = oAtt[j].lov_att_id;

                    if (olov != null)
                    {
                        DropDownList ddlFooter = new DropDownList();
                        ddlFooter.ID = "ddlFooter" + i.ToString();
                        ddlFooter.CssClass = "form-control";
                        e.Row.Cells[i].Controls.Add(ddlFooter);
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        bindLov(ddlFooter, olov.ConvertTo<int>());
                    }
                    else
                    {
                        TextBox txtFooter = new TextBox();
                        txtFooter.ID = "txtFooter" + i.ToString();
                        txtFooter.CssClass = "form-control input-md";
                        txtFooter.Width = 100;
                        e.Row.Cells[i].Controls.Add(txtFooter);
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                    }

                    j++;
                }
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    e.Row.Cells[2].Visible = false;
            //}

            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Cells[2].Visible = false;
            //}

            //if ((e.Row.RowState & DataControlRowState.Edit) > 0) // Only on Edit row
            //{
            //    for (Int16 I = 2; I < e.Row.Cells.Count; I++)    // Start @ 2nd col if 1st is Command Buttons
            //    {
            //        if (I == 3 || I == 4)                                 // Allow Editing in 6th column
            //        {
            //            TextBox tb = e.Row.Cells[I].Controls[0] as TextBox;   // Try to cast control to TextBox
            //            if ((tb != null))
            //            {
            //                tb.ReadOnly = true;                    // Set ReadOnly
            //                tb.Enabled = false;                    // Disable TextBox
            //            }
            //        }
            //    }
            //}
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            bool validate;
            if (e.CommandName == "Insert" && Page.IsValid)
            {
                ArrayList txtArray = new ArrayList();
                GridViewRow fRow = gv.FooterRow;
                string sValidated = "N";
                int cellCnt = gv.FooterRow.Cells.Count;
                int objid = ddlobj.SelectedValue.ConvertTo<int>();
                long rowid = 0;
                int k = 0;
                string txt;

                List<Ref_Obj_Att> oAtt = new List<Ref_Obj_Att>();
                using (MDMEntities db = new MDMEntities())
                {
                    oAtt = db.Ref_Obj_Att.Where(i => i.obj_id == objid).ToList();
                }

                for (int i = 3; i < cellCnt; i++)
                //for (int i = 2; i < cellCnt; i++)
                {
                    var olov = oAtt[k].lov_att_id;
                    string fieldnm = oAtt[k].obj_attribute; //gv.HeaderRow.Cells[i].Text;  // modified by CC : 20190226

                    if (olov != null)
                        txt = ((DropDownList)fRow.FindControl("ddlFooter" + i.ToString())).SelectedItem.Text;
                    else
                        txt = ((TextBox)fRow.FindControl("txtFooter" + i.ToString())).Text;

                    if (string.IsNullOrEmpty(txt) || txt == "- SELECT -")
                    {
                        UserAlert1.ShowAlert("Value can not be blank.", CommonEnum.AlertType.Exclamation);
                        return;
                    }

                    using (MDMEntities db = new MDMEntities())
                    {
                        var attdtl = db.Ref_Obj_Att.Where(j => j.obj_id == objid && j.obj_attribute == fieldnm).SingleOrDefault();
                        if (attdtl != null)
                        {
                            if ((string.IsNullOrEmpty(attdtl.ref_rule_id.ToString()) || attdtl.ref_rule_id == 0) || (string.IsNullOrEmpty(attdtl.rule_col.ToString()) || attdtl.rule_col == 0)) // modified by CC : 20190226
                                sValidated = "N";
                            else
                                sValidated = "Y";
                        }
                    }

                    txtArray.Add(txt + "|" + sValidated);
                    k++;
                }

                validate = BoolValidation(txtArray);

                if (validate)
                {
                    using (MDMEntities db = new MDMEntities())
                    {
                        var att = db.Ref_Obj_Att.Where(i => i.obj_id == objid).ToList();
                        var objv = db.Ref_Obj_Value.Where(i => i.obj_id == objid).ToList();

                        if (objv.Count > 0)
                            rowid = objv.Max(i => i.row_id).Value;

                        if (att.Count > 0)
                        {
                            for (int i = 0; i < att.Count; i++)
                            {
                                Ref_Obj_Value rov = new Ref_Obj_Value();
                                string[] result = txtArray[i].ToString().Split('|');

                                rov.obj_id = objid;
                                rov.row_id = rowid + 1;
                                rov.obj_Att_id = att[i].ID;
                                rov.obj_value = result[0].ToString();//txtArray[i].ToString();
                                rov.obj_type = "LIST";
                                rov.min = 0;
                                rov.max = 0;
                                rov.CREATED_BY = this.logonId;
                                rov.CREATE_DATETIME = DateTime.Now;
                                rov.MODIFIER_ID = this.logonId;
                                rov.MODIFY_DATETIME = DateTime.Now;

                                if (this.isMaker == CommonEnum.statusEnum.Y.ToString())
                                {
                                    rov.status = CommonEnum.RecordStatusEnum.O.ToString();
                                    rov.maker_id = this.userId;
                                }
                                else
                                    rov.status = CommonEnum.RecordStatusEnum.A.ToString();

                                db.Ref_Obj_Value.Add(rov);
                                db.SaveChanges();
                            }
                        }
                    }
                    BindGrid();
                    lblErrormsg.Text = string.Empty;
                }
            }
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["PageIndex"] = e.NewPageIndex;
            this.gv.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            int objid = ddlobj.SelectedValue.ConvertTo<int>();
            foreach (GridViewRow row in gv.Rows)
            {
                LiteralControl c = row.Cells[1].Controls[0] as LiteralControl;
                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;
                int id = row.Cells[2].Text.ConvertTo<int>();

                if (ctl != null && ctl.Checked)
                {
                    using (MDMEntities context = new MDMEntities())
                    {
                        var rov = context.Ref_Obj_Value.Where(i => i.obj_id == objid && i.row_id == id).ToList();

                        if (rov.Count > 0)
                        {
                            foreach (var item in rov)
                            {
                                Ref_Obj_Value _robjatt = context.Ref_Obj_Value.SingleOrDefault(i => i.ID == item.ID);

                                if (_robjatt != null)
                                {
                                    context.Ref_Obj_Value.Remove(_robjatt);
                                    context.SaveChanges();
                                    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = id, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_value " + _robjatt.obj_id + ": " + _robjatt.obj_value + " has been deleted.", CREATOR_ID = this.logonId });
                                }
                            }
                            AuditTrail oAudittrl = new AuditTrail();
                            oAudittrl.AddAuditTrail(oAudit);
                        }
                    }
                }
            }
            BindGrid();
        }

        private bool BoolValidation(ArrayList list)
        {
            ClassLibrary1.Logic_Parser lvalidation = new ClassLibrary1.Logic_Parser();
            Boolean sResult;
            string strResult = "";
            List<String> Result = new List<string>();
            DataTable dt = (DataTable)ViewState["AttRules"];
            string udf = LoadUDFFile();

            for (int i = 0; i < list.Count; i++)
            {
                string[] result = list[i].ToString().Split('|');

                if (result[1] == "Y")
                {
                    if (dt != null)
                    {
                        string input = result[0];//list[i].ToString();
                        string attname = dt.Rows[i]["att_desc"].ToString();//dt.Rows[i]["obj_attribute"].ToString();
                        string fieldruledesc = dt.Rows[i]["rules_desc"].ToString();
                        string fieldname = dt.Rows[i]["rules_colnm"].ToString();
                        int fieldruleno = 1;
                        if (dt.Rows[i]["rule_no"] != null)
                            fieldruleno = dt.Rows[i]["rule_no"].ConvertTo<int>();
                        string fieldrule = dt.Rows[i]["rules"].ToString();
                        string obj = dt.Rows[i]["DB_ref_tbl"].ToString();
                        string objatt = dt.Rows[i]["DB_ref_field"].ToString();

                        object[] param = new object[2] { input, fieldruleno };
                        string paralist = string.IsNullOrEmpty(GetPara(obj, objatt)) ? string.Empty : "~" + GetPara(obj, objatt);

                        sResult = lvalidation.Parse(fieldrule, fieldname, udf, paralist, param);

                        if (sResult == false)
                        {
                            if (lvalidation.Err_Description.Count > 0)
                                Result.Add(attname + ": " + input + ", Validation = " + sResult + ", Validation Msg: " + lvalidation.Err_Description[0].ToString());
                            else
                                Result.Add(attname + ": " + input + ", Validation = " + sResult + ", Validation Msg: " + fieldruledesc);
                        }
                    }
                }
            }

            strResult = string.Join(" <br/> ", Result.ToArray());

            if (!string.IsNullOrEmpty(strResult))
            {
                //lblErrormsg.Text = strResult;
                UserAlert1.ShowAlert(strResult, CommonEnum.AlertType.Exclamation);
                return false;
            }

            return true;
        }

        protected void gv_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Reset the edit index.
            gv.EditIndex = -1;
            //Bind data to the GridView control.
            BindGrid();
        }

        protected void gv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //Set the edit index.
            gv.EditIndex = e.NewEditIndex;
            //Bind data to the GridView control.
            BindGrid();
        }

        protected void gv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            MDMEntities context = new MDMEntities();
            List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
            DataTable dt = (DataTable)Session["TaskTable"];
            int row_id = Convert.ToInt32(gv.DataKeys[e.RowIndex].Values[0]);
            int obj_id = ddlobj.SelectedValue.ConvertTo<int>();
            string col_nm = string.Empty;
            string new_val = string.Empty;
            string sValidated = "N";
            ArrayList txtArray = new ArrayList();
            bool validate;

            //Update the values.
            GridViewRow row = gv.Rows[e.RowIndex];
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (i > 0)
                {
                    col_nm = dt.Columns[i].ColumnName;
                    new_val = ((TextBox)(row.Cells[i + 2].Controls[0])).Text;
                    //new_val = ((TextBox)(row.Cells[i + 1].Controls[0])).Text;

                    //var Attdtl = context.Ref_Obj_Att.Where(j => j.obj_id == obj_id && j.obj_attribute == col_nm).SingleOrDefault();
                    var Attdtl = context.Ref_Obj_Att.Where(j => j.obj_id == obj_id && j.att_desc == col_nm).SingleOrDefault();
                    if (Attdtl != null)
                    {
                        if (string.IsNullOrEmpty(Attdtl.ref_rule_id.ToString()) || Attdtl.ref_rule_id == 0)
                            sValidated = "N";
                        else
                            sValidated = "Y";

                        var valdtl = context.Ref_Obj_Value.Where(k => k.obj_id == obj_id && k.row_id == row_id && k.obj_Att_id == Attdtl.ID).SingleOrDefault();

                        if (valdtl != null)
                        {
                            if (valdtl.obj_value != new_val)
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = valdtl.ID.ConvertTo<int>(), TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "Obj_Value", OLD_VALUE = valdtl.obj_value, NEW_VALUE = new_val, DESC = "Value has been modified.", CREATOR_ID = this.logonId });

                            if (valdtl.status == CommonEnum.RecordStatusEnum.R.ToString())
                            {
                                if (this.isMaker == CommonEnum.statusEnum.Y.ToString())
                                {
                                    valdtl.status = CommonEnum.RecordStatusEnum.O.ToString();
                                }
                                else
                                    valdtl.status = CommonEnum.RecordStatusEnum.A.ToString();
                            }
                            valdtl.obj_value = new_val;
                            valdtl.MODIFIER_ID = this.logonId;
                            valdtl.MODIFY_DATETIME = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(new_val))
                            {
                                Ref_Obj_Value srefatt = new Ref_Obj_Value();
                                srefatt.obj_id = obj_id;
                                srefatt.row_id = row_id;
                                srefatt.obj_Att_id = Attdtl.ID;
                                srefatt.obj_value = new_val;
                                srefatt.CREATED_BY = this.logonId;
                                srefatt.CREATE_DATETIME = DateTime.Now;
                                srefatt.MODIFIER_ID = this.logonId;
                                srefatt.MODIFY_DATETIME = DateTime.Now;
                                srefatt.min = 0;
                                srefatt.max = 0;
                                srefatt.obj_type = "LIST";
                                if (this.isMaker == CommonEnum.statusEnum.Y.ToString())
                                {
                                    srefatt.status = CommonEnum.RecordStatusEnum.O.ToString();
                                    srefatt.maker_id = this.userId;
                                }
                                else
                                    srefatt.status = CommonEnum.RecordStatusEnum.A.ToString();

                                srefatt.checker_id = null;
                                srefatt.appv_rejct_dt = null;
                                srefatt.reject_reasons = null;
                                context.Ref_Obj_Value.Add(srefatt);
                                context.SaveChanges();
                                //pkid = (int)srefatt.ID;
                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = obj_id, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "Obj_Value", OLD_VALUE = "", NEW_VALUE = new_val = "New obj value added.", CREATOR_ID = this.logonId });
                            }
                        }
                    }
                    txtArray.Add(new_val + "|" + sValidated);
                }
            }

            validate = BoolValidation(txtArray);

            if (validate)
            {
                context.SaveChanges();
                AuditTrail oAudittrl = new AuditTrail();
                oAudittrl.AddAuditTrail(oAudit);
            }

            //Reset the edit index.
            gv.EditIndex = -1;

            //Bind data to the GridView control.
            BindGrid();
        }

        private string GetPara(string obj, string objatt)
        {
            string paralist = string.Empty;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetParaList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@proj_id", SqlDbType.Int).Value = ddlproj.SelectedValue.ConvertTo<int>();
                cmd.Parameters.Add("@obj", SqlDbType.VarChar, 50).Value = obj;
                cmd.Parameters.Add("@obj_attribute", SqlDbType.VarChar, 50).Value = objatt;
                SqlDataAdapter sql = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sql.Fill(ds, "para");
                DataTable dt = ds.Tables["para"];

                if (ds.Tables["para"].Rows.Count > 0)
                    paralist = ds.Tables["para"].Rows[0]["objvalue"].ToString();

                con.Close();
            }

            return paralist;
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobj.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {
                //var role = (from c in db.Ref_Obj
                //            select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj + " - " + pd.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindddlobject(ddlobj, datatext, datavalue);
                    }
                    ddlobj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
            ddlatt.Items.Clear();
            txtsearch.Text = string.Empty;
            ViewState["searchtxt"] = string.Empty;
            ViewState["att"] = string.Empty;
            gv.DataSource = null;
            gv.DataBind();
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            ViewState["searchtxt"] = txtsearch.Text;
            ViewState["att"] = ddlatt.SelectedValue;
            BindGrid();
        }

        //protected void btnMerge_Click(object sender, EventArgs e)
        //{
        //    ViewState["PageMode"] = "M";
        //    List<int> ids = new List<int>();
        //    GridViewRow fRow = gv.FooterRow;

        //    foreach (GridViewRow row in gv.Rows)
        //    {
        //        LiteralControl c = row.Cells[1].Controls[0] as LiteralControl;
        //        HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;

        //        int oid = ddlobj.SelectedValue.ConvertTo<int>();
        //        int mid = ctl.Value.ConvertTo<int>(); //rowid

        //        if (ctl != null && ctl.Checked)
        //        {
        //            using (MDMEntities context = new MDMEntities())
        //            {
        //                ids.Add(mid);
        //                int fir = ids[0];

        //                var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id == fir).ToList();
        //                if (upd.Count > 0)
        //                {
        //                    for (int i = 0; i < upd.Count; i++)
        //                    {
        //                        ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text = upd[i].obj_value;
        //                    }

        //                    ((LinkButton)fRow.FindControl("lnkAdd")).Visible = false;
        //                    btnsave.Visible = true;
        //                    btncancel.Visible = true;
        //                    btnMerge.Visible = false;
        //                    btndelete.Visible = false;

        //                    UserAlert1.ShowAlert("Please specify a new value or use the default .", CommonEnum.AlertType.Exclamation);
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //}

        //protected void btnsave_Click(object sender, EventArgs e)
        //{
        //    if (!Page.IsValid)
        //        return;

        //    List<AUDIT_TRAIL> oAudit = new List<AUDIT_TRAIL>();
        //    List<MERGE_SPLIT_TRAIL> oMerge = new List<MERGE_SPLIT_TRAIL>();
        //    List<int> ids = new List<int>();
        //    List<int> idm = new List<int>();
        //    GridViewRow fRow = gv.FooterRow;

        //    if (ViewState["PageMode"].ToString() == "M")
        //    {
        //        using (MDMEntities context = new MDMEntities())
        //        {
        //            foreach (GridViewRow row in gv.Rows)
        //            {
        //                LiteralControl c = row.Cells[1].Controls[0] as LiteralControl;
        //                HtmlInputCheckBox ctl = c.FindControl("chkid") as HtmlInputCheckBox;

        //                int oid = ddlobj.SelectedValue.ConvertTo<int>();
        //                int mid = ctl.Value.ConvertTo<int>(); // rowid

        //                if (ctl != null && ctl.Checked)
        //                {
        //                    ids.Add(mid);
        //                    int fir = ids[0];

        //                    var upd = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id == fir).ToList();
        //                    if (upd.Count > 0)
        //                    {
        //                        for (int i = 0; i < upd.Count; i++)
        //                        {
        //                            string newVal = ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text;
        //                            int updid = upd[i].ID.ConvertTo<int>();
        //                            idm.Add(updid);

        //                            oMerge.Add(new MERGE_SPLIT_TRAIL() { OBJECT_ID = updid, MERGE_ID = updid, TBL_NAME = "Ref_Obj_Value", OLD_ITEM = null, OLD_VALUE = upd[i].obj_value, OLD_VALUE_DESC = null, NEW_ITEM = null, NEW_VALUE = newVal, NEW_VALUE_DESC = null, CREATOR_ID = this.logonId, CREATE_DATETIME = DateTime.Now, STS = "M", DESC = "Object value has been merged" });

        //                            Ref_Obj_Value objval = context.Ref_Obj_Value.SingleOrDefault(j => j.ID == updid);
        //                            if (objval != null)
        //                            {
        //                                if (objval.obj_value != newVal)
        //                                    oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = updid, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "obj_value", OLD_VALUE = objval.obj_value, NEW_VALUE = newVal, DESC = "Ref. obj value has been modified", CREATOR_ID = this.logonId });
        //                                objval.obj_value = newVal;
        //                                objval.MODIFIER_ID = this.logonId;
        //                                objval.MODIFY_DATETIME = DateTime.Now;
        //                                objval.min = 0;
        //                                objval.max = 0;
        //                                context.SaveChanges();
        //                            }
        //                        }
        //                    }

        //                    var sel = context.Ref_Obj_Value.Where(i => i.obj_id == oid && i.row_id == mid && i.row_id != fir).ToList();
        //                    if (sel.Count > 0)
        //                    {
        //                        for (int i = 0; i < sel.Count; i++)
        //                        {
        //                            string newVal = ((TextBox)fRow.FindControl("txtFooter" + (i + 3).ToString())).Text;
        //                            int selid = sel[i].ID.ConvertTo<int>();
        //                            int idms = idm[i];

        //                            Ref_Obj_Value _robjatt = context.Ref_Obj_Value.SingleOrDefault(k => k.ID == selid);
        //                            if (_robjatt != null)
        //                            {
        //                                oMerge.Add(new MERGE_SPLIT_TRAIL() { OBJECT_ID = selid, MERGE_ID = idms, TBL_NAME = "Ref_Obj_Value", OLD_ITEM = null, OLD_VALUE = _robjatt.obj_value, OLD_VALUE_DESC = null, NEW_ITEM = null, NEW_VALUE = newVal, NEW_VALUE_DESC = null, CREATOR_ID = this.logonId, CREATE_DATETIME = DateTime.Now, STS = "M", DESC = "Object value has been merged" });
        //                                context.Ref_Obj_Value.Remove(_robjatt);
        //                                context.SaveChanges();
        //                                oAudit.Add(new AUDIT_TRAIL() { OBJECT_ID = selid, TBL_NAME = "Ref_Obj_Value", FIELD_NAME = "", OLD_VALUE = "", NEW_VALUE = "", DESC = "Ref_Obj_value " + _robjatt.obj_id + ": " + _robjatt.obj_value + " has been deleted due to merge.", CREATOR_ID = this.logonId });
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            context.SaveChanges();

        //            AuditTrail oAudittrl = new AuditTrail();
        //            oAudittrl.AddAuditTrail(oAudit);
        //            oAudittrl.AddMergeTrail(oMerge);
        //        }

        //        BindGrid();
        //        btnsave.Visible = false;
        //        btncancel.Visible = false;
        //        btnMerge.Visible = true;
        //        btndelete.Visible = true;
        //        ViewState["PageMode"] = "A";
        //    }
        //}

        //protected void btncancel_Click(object sender, EventArgs e)
        //{
        //    BindGrid();
        //    btnsave.Visible = false;
        //    btncancel.Visible = false;
        //    btnMerge.Visible = true;
        //    btndelete.Visible = true;
        //}
    }
}