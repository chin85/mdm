﻿using DMPortal.Manager;
using DMPortal.Object;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DMPortal
{
    public partial class ObjView : System.Web.UI.Page
    {
        public string logonId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionInfo oDOSession = new SessionInfo();
            oDOSession = (SessionInfo)Session["SessionInfo"];
            this.logonId = oDOSession.LogonId;

            if (!Page.IsPostBack)
            {
                ViewState["PageMode"] = "A";
                ViewState["PageIndex"] = 0;
                BindDB();
                bindObj();
                //BindView();
            }
        }


        protected void BindDB()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                SqlCommand sql = new SqlCommand("select name from sys.sysdatabases where name not in ('master', 'model', 'msdb', 'tempdb')", con);
                using (SqlDataReader dr = sql.ExecuteReader())
                {
                    int i = 0;

                    while (dr.Read())
                    {
                        i = i + 1;
                        bindDLL(ddlextdb, dr.GetString(0), i.ToString());
                    }
                }
            }
        }

        private void bindObj()
        {
            using (MDMEntities db = new MDMEntities())
            {
                //    var role = (from c in mdm.Ref_Obj
                //                select new { c.ID, objDESC = c.obj + " - " + c.Source_Sys }).OrderBy(i => i.objDESC).ToList();

                //    if (role.Count > 0)
                //    {
                //        bindDLL(ddlobjid, "- SELECT -", "0");
                //        foreach (var item in role)
                //        {
                //            var datavalue = item.ID.ToString();
                //            var datatext = item.objDESC;
                //            bindDLL(ddlobjid, datatext, datavalue);
                //        }
                //    }

                var role = (from c in db.Ref_Proj
                            select new { c.ID, objDESC = c.Project }).OrderBy(i => i.objDESC).ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindDLL(ddlproj, datatext, datavalue);
                    }
                    ddlproj.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        private void BindView()
        {
            int tbl = ddlobjid.SelectedValue.ConvertTo<int>();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                if (tbl != 0)
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("GetObjView", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = tbl;
                    SqlDataAdapter sql = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sql.Fill(ds, "Obj");
                    gv.DataSource = ds.Tables["Obj"];
                    gv.DataBind();
                    con.Close();
                }
            }
        }

        private void bindDLL(DropDownList ddl, string value, string text)
        {
            ddl.Items.Add(new ListItem(value, text));
        }

        protected void Obj_Search(object sender, EventArgs e)
        {
            BindView();
        }

        protected void btnPublish_Click(object sender, EventArgs e)
        {
            int tbl = ddlobjid.SelectedValue.ConvertTo<int>();
            string expType = ddltype.SelectedValue;
            string sourceSys = ddlextdb.SelectedItem.Text;
            string pk = ddlpk.SelectedItem.Text;
            string strtbl = ExtTbl.SelectedItem.Text;

            if (expType == "O")
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("ExportObj", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Source_Sys", SqlDbType.VarChar, 50).Value = sourceSys;
                    cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = tbl;
                    if (strtbl == "- SELECT -")
                        cmd.Parameters.Add("@tbl_NM", SqlDbType.VarChar, 50).Value =  DBNull.Value ;
                    else
                        cmd.Parameters.Add("@tbl_NM", SqlDbType.VarChar, 50).Value = strtbl;
                    SqlDataAdapter sql = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sql.Fill(ds, "Obj");
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", "alert('Table Exported Successfully')", true);
                    con.Close();
                }
            else
            {
                if (ddlpk.SelectedValue == "0" || strtbl == "- SELECT -")
                {
                    UserAlert.ShowAlert("Primary key / Export to (Table) can not be null for 'Append to Existing Table' option.", CommonEnum.AlertType.Exclamation);
                }
                else
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
                    {
                        con.Open();
                        SqlCommand cmd = new SqlCommand("ExportObjAppend", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Source_Sys", SqlDbType.VarChar, 50).Value = sourceSys;
                        cmd.Parameters.Add("@obj_id", SqlDbType.BigInt).Value = tbl;
                        cmd.Parameters.Add("@pk", SqlDbType.VarChar, 50).Value = pk;
                        cmd.Parameters.Add("@tbl_NM", SqlDbType.VarChar, 50).Value = strtbl;
                        SqlDataAdapter sql = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        sql.Fill(ds, "Obj");
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", "alert('Table Exported Successfully')", true);
                        con.Close();
                    }
            }
        }

        protected void ddlobjid_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCol();
        }

        private void BindCol()
        {
            int objid = ddlobjid.SelectedValue.ConvertTo<int>();
            ddlpk.Items.Clear();
            using (MDMEntities mdm = new MDMEntities())
            {
                var role = (from c in mdm.Ref_Obj_Att
                            where c.obj_id == objid
                            select new { c.ID, c.obj_attribute }).OrderBy(i => i.obj_attribute).ToList();

                if (role.Count > 0)
                {
                    bindDLL(ddlpk, "- SELECT -", "0");
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.obj_attribute;
                        bindDLL(ddlpk, datatext, datavalue);
                    }
                }
            }
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlobjid.Items.Clear();
            var _proj = ddlproj.SelectedValue.ConvertTo<int>();
            using (MDMEntities db = new MDMEntities())
            {

                var role = (from c in db.Ref_Obj
                            join pd in db.Ref_Source on c.Source_Sys equals pd.ID
                            where c.Proj_id == _proj
                            select new { c.ID, objDESC = c.obj + " - " + pd.Source_Sys }).OrderBy(i => i.objDESC).ToList();
                var validation = db.Ref_Rule.ToList();

                if (role.Count > 0)
                {
                    foreach (var item in role)
                    {
                        var datavalue = item.ID.ToString();
                        var datatext = item.objDESC;
                        bindDLL(ddlobjid, datatext, datavalue);
                    }
                    ddlobjid.Items.Insert(0, new ListItem("- SELECT -", "0"));
                }
            }
        }

        protected void ddlextdb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExtTbl.Items.Clear();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["MDMConnectionString"].ToString()))
            {
                con.Open();
                string sql = "SELECT DISTINCT TABLE_NAME FROM " + "@db" + ".INFORMATION_SCHEMA.COLUMNS ORDER BY TABLE_NAME";
                sql = sql.Replace("@db", ddlextdb.SelectedItem.ToString());
                SqlCommand cmd = new SqlCommand(sql, con);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        ExtTbl.Items.Add(dr.GetString(0));
                    }
                }
            }

            ExtTbl.Items.Insert(0, new ListItem("- SELECT -", "0"));
        }
    }
}