--EXEC [MDM].[dbo].[CreateTbl] @Source_Sys, @obj_id, @obj_value, @obj_value_desc
--GO

USE [MDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[ExportObj] 
	@Source_Sys varchar(50),
	@obj_id bigint,
	@tbl_NM varchar(50)

WITH ENCRYPTION
AS
BEGIN


	--Declare necessary variables
	DECLARE   @SQLQuery AS NVARCHAR(MAX)
	DECLARE   @SQLQueryDrop AS NVARCHAR(500)
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
	DECLARE   @PivotColumns2 AS NVARCHAR(MAX)
	DECLARE   @MinPivotColumns AS NVARCHAR(MAX)
	DECLARE   @MinPivotColumns2 AS NVARCHAR(MAX)
 
	--Get unique values of pivot column  
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute),@MinPivotColumns = COALESCE(@MinPivotColumns + ',','') + 'Min('+QUOTENAME(obj_attribute)+') as '+ QUOTENAME(obj_attribute)
	FROM (SELECT obj_attribute FROM [dbo].[Ref_Obj_Att] WHERE obj_id =@obj_id) AS Pivotcol

	SELECT @PivotColumns2= COALESCE(@PivotColumns2 + ',','') + QUOTENAME(mapobj), @MinPivotColumns2 = COALESCE(@MinPivotColumns2 + ',','') + 'Min('+QUOTENAME(mapobj)+') as '+ QUOTENAME(mapobj)
	FROM (select distinct mapobj from [MDM].[dbo].[Ref_Obj_Mapped]
			LEFT OUTER JOIN(
				select [Ref_Obj_Value].obj_id,[Ref_Obj_Value].ID,[Ref_Obj_Value].obj_value as mapval,obj +'_'+ [Ref_Source].Source_Sys as mapobj
				from [dbo].[Ref_Obj]
				left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
				left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
				left outer join [dbo].[Ref_Source] on [Ref_Obj].Source_Sys = [Ref_Source].ID and [Ref_Obj].Proj_id = [Ref_Source].Proj_Id
			)mapval on mapval.ID = m_obj_value_id and mapval.obj_id = m_obj_id
			where [Ref_Obj_Mapped].obj_id = @obj_id) AS Pivotcol2
 
	--Select unique object name as table name
	DECLARE @TableName nvarchar(128)
	IF @tbl_NM IS NOT NULL
		SET @TableName = @Source_Sys + '.[dbo].' + @tbl_NM
	ELSE
		SELECT @tbl_NM = [obj] 
			FROM MDM.[dbo].[Ref_Obj]
			WHERE [ID] = @obj_id 
		SET @TableName = @Source_Sys + '.[dbo].' + @tbl_NM
 
    --Drop Table if exist
	IF OBJECT_ID (@TableName,'U') IS NOT NULL
		SET @SQLQueryDrop = N'DROP TABLE ' + @TableName + '; ' 

		EXEC sp_executesql @SQLQueryDrop
	
			
	--Create the dynamic query with all the values for 
	--pivot column at runtime
	IF @PivotColumns2 IS NOT NULL
		SET   @SQLQuery = 
		N'SELECT * into ' + @TableName + '
		  FROM (
			SELECT ' + @MinPivotColumns + ',' + @MinPivotColumns2 + ' FROM (
				select Row_id,obj_attribute,[Ref_Obj_Value].obj_value,mapval.mapval,mapobj
				from [dbo].[Ref_Obj]
				left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
				left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
				left outer join dbo.Ref_Obj_Mapped on [Ref_Obj_Value].ID = Ref_Obj_Mapped.obj_value_id
				left outer join (
					select [Ref_Obj_Value].obj_id,[Ref_Obj_Value].ID,[Ref_Obj_Value].obj_value as mapval,obj +''_''+ [Ref_Source].Source_Sys as mapobj
					from [dbo].[Ref_Obj]
					left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
					left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
					left outer join [dbo].[Ref_Source] on [Ref_Obj].Source_Sys = [Ref_Source].ID and [Ref_Obj].Proj_id = [Ref_Source].Proj_Id
				)mapval on mapval.ID = m_obj_value_id and mapval.obj_id = m_obj_id
				where [Ref_Obj_Value].[status]=''A'' and Ref_Obj.ID =' + Convert(varchar,@obj_id) + ' 
			)a
			pivot ( 
			min(obj_value) for obj_attribute in (' + @PivotColumns + ')
			)b
			pivot(
			min(mapval) for mapobj in (' + @PivotColumns2 + ')
			)c
			group by Row_id
		 )tbl'

	ELSE
		SET   @SQLQuery = 
		N'SELECT * into ' + @TableName + '
		  FROM (
			SELECT ' + @MinPivotColumns + ' FROM (
				select Row_id,obj_attribute,[Ref_Obj_Value].obj_value
				from [dbo].[Ref_Obj]
				left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
				left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
				where [Ref_Obj_Value].[status]=''A'' and Ref_Obj.ID =' + Convert(varchar,@obj_id) + ' 
			)a
			pivot ( 
			min(obj_value) for obj_attribute in (' + @PivotColumns + ')
			)b
			group by Row_id
		)tbl'

 
    --print @SQLQuery
	--Execute dynamic query
	EXEC sp_executesql @SQLQuery
 
 END
 
