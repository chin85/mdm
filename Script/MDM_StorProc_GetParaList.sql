--EXEC [MDM].[dbo].[CreateTbl] @Source_Sys, @obj_id, @obj_value, @obj_value_desc
--GO

USE [MDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetParaList] 
	@proj_id bigint,
	@obj as varchar(50),
	@obj_attribute as varchar(50)

WITH ENCRYPTION
AS
BEGIN


	--Declare necessary variables
	DECLARE   @SQLQuery AS NVARCHAR(MAX)
	SET   @SQLQuery = 
	N'SELECT DISTINCT objvalue
		FROM MDM.[dbo].Ref_Obj_Value p1
		CROSS APPLY ( select obj_value + ''~''
			from MDM.[dbo].[Ref_Obj]
			left outer join MDM.[dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
			left outer join MDM.[dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
			where [Ref_Obj].Proj_id = ' + Convert(varchar,@proj_id) + ' and Ref_Obj.obj =''' + @obj + ''' and Ref_Obj_Att.obj_attribute = ''' + @obj_attribute + '''
			FOR XML PATH('''') 
		)  D ( objvalue )'
 
	--Execute dynamic query
	EXEC sp_executesql @SQLQuery
	--print @SQLQuery
 
 END
 
 --exec [dbo].[GetParaList] 1,'Ref_10_IOC','Ref_ID'
