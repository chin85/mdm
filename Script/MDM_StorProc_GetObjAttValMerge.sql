--EXEC [MDM].[dbo].[CreateTbl] @Source_Sys, @obj_id, @obj_value, @obj_value_desc
--GO

USE [MDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[GetObjAttValMerge] 
	@obj_id bigint

WITH ENCRYPTION
AS
BEGIN


	--Declare necessary variables
	DECLARE   @SQLQuery AS NVARCHAR(MAX)
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
 
	--Get unique values of pivot column  
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute)
	FROM (SELECT obj_attribute FROM [dbo].[Ref_Obj_Att] WHERE obj_id =@obj_id) AS Pivotcol
 
 
	--Create the dynamic query with all the values for 
	--pivot column at runtime
	SET   @SQLQuery = 
	N'SELECT * FROM (
		select Row_id,obj_attribute,obj_value
		from [dbo].[Ref_Obj]
		left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
		left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
		where [Ref_Obj_Value].[status]=''A'' and Ref_Obj.ID = ' + Convert(varchar,@obj_id) + ' 
	)a
	pivot ( 
	  min(obj_value) for obj_attribute in (' + @PivotColumns + ' )
	)b'
 
	--Execute dynamic query
	EXEC sp_executesql @SQLQuery
 
END