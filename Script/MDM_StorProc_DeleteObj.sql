USE [MDM]
GO
/****** Object:  StoredProcedure [dbo].[DeleteObj]    Script Date: 1/10/2018 10:43:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DeleteObj] 
	@obj_id bigint

AS
BEGIN

DELETE MDM.[dbo].[Ref_Obj_Value] where obj_id = @obj_id

DELETE MDM.[dbo].[Ref_Obj_Att]
FROM MDM.[dbo].[Ref_Obj_Att]
INNER JOIN MDM.[dbo].[Ref_Obj] ON [Ref_Obj].ID = [Ref_Obj_Att].obj_id
WHERE [Ref_Obj].ID = @obj_id

DELETE FROM MDM.[dbo].[Ref_Obj] where ID = @obj_id
 
END