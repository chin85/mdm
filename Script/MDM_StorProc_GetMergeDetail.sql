--EXEC [MDM].[dbo].[CreateTbl] @Source_Sys, @obj_id, @obj_value, @obj_value_desc
--GO

USE [MDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[GetMergeDetail] 
	@obj_id bigint,
	@merge_id varchar (500)

WITH ENCRYPTION
AS
BEGIN


	--Declare necessary variables
	DECLARE   @SQLQuery AS NVARCHAR(MAX)
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
 
	--Get unique values of pivot column  
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute)
	FROM (SELECT obj_attribute FROM [dbo].[Ref_Obj_Att] WHERE obj_id =@obj_id) AS Pivotcol
 
 
	--Create the dynamic query with all the values for 
	--pivot column at runtime
	SET   @SQLQuery = 
	N'SELECT ' + @PivotColumns + '  FROM (
		select OLD_VALUE,OLD_ITEM,ROW_NUMBER() OVER(PARTITION BY merge_id ORDER BY merge_id ASC) as rowid
		from [MDM].[dbo].[MERGE_SPLIT_TRAIL] where MERGE_ID in (' + Convert(varchar,@merge_id) + ')  and STS =''M''
	)a
	pivot ( 
		max([OLD_VALUE]) for [OLD_ITEM] in (' + @PivotColumns + ' )
	)b'

	--Execute dynamic query
	EXEC sp_executesql @SQLQuery
	--print @SQLQuery
 
END

