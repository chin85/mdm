/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[SCREEN_CODE]
      ,[SCREEN_NAME]
      ,[MODULE]
      ,[PATH]
      ,[CREATOR_ID]
      ,[CREATE_DATETIME]
      ,[MODIFIER_ID]
      ,[MODIFY_DATETIME]
  FROM [MDM].[dbo].[SCREEN_MAINTENANCE]

insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'PM001','RefProj','Param Maintenance','RefProj.aspx','sysadmin',getdate(),'sysadmin',getdate()

insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'PM006','RefSource','Param Maintenance','RefSource.aspx','sysadmin',getdate(),'sysadmin',getdate()

insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'PM007','ObjView','Param Maintenance','ObjView.aspx','sysadmin',getdate(),'sysadmin',getdate()

insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'PM008','ObjImport','Param Maintenance','ObjImport.aspx','sysadmin',getdate(),'sysadmin',getdate()

insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'PM009','MergeSplit','Param Maintenance','MergeSplit.aspx','sysadmin',getdate(),'sysadmin',getdate()


insert into [MDM].[dbo].[SCREEN_MAINTENANCE]
select 'RP001','AuditLog','Reports','AuditLog.aspx','sysadmin',getdate(),'sysadmin',getdate()

