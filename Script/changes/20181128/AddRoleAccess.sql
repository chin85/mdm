USE [MDM]
GO
/****** Object:  StoredProcedure [dbo].[AddRoleAccess]    Script Date: 26/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AddRoleAccess] 
	@Roleid int

	
AS

BEGIN
	insert into ROLE_ACCESS([ROLE_ID],[SCREEN_ID],[ACCESS],[CREATOR_ID],[CREATED_DATETIME],[MODIFIER_ID],[MODIFIED_DATETIME])
	select @Roleid,ID,'N','sysadmin',GETDATE(), 'sysadmin',GETDATE()
	from SCREEN_MAINTENANCE sm
	--where sm.SCREEN_CODE <>'UP004'
END




