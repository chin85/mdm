USE [MDM]
GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ImportData] 
	@obj_id bigint,
    @db varchar(128),
    @tbl varchar(128),
	@ImportOpt varchar(1)

WITH ENCRYPTION
AS
BEGIN
 
    DECLARE @SQL as nvarchar(max)
	DECLARE @StartRowId as int = 0

	 IF (@ImportOpt ='O')
		BEGIN
			DELETE FROM MDM.[dbo].[Ref_Obj_Value] WHERE obj_id = @obj_id
		END
	 ELSE 
		BEGIN
			SELECT @StartRowId = MAX(row_id) FROM MDM.[dbo].[Ref_Obj_Value] WHERE obj_id = @obj_id
		END

	--Get unique values of pivot column  
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
	DECLARE   @PivotColumnsCast AS NVARCHAR(MAX)
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute),@PivotColumnsCast = COALESCE(@PivotColumnsCast + ',','') + 'Cast('+QUOTENAME(obj_attribute)+' AS VARCHAR) as '+ QUOTENAME(obj_attribute)
	FROM (SELECT obj_attribute FROM MDM.[dbo].[Ref_Obj_Att] WHERE obj_id =@obj_id) AS Pivotcol

	--Ref_Obj_Value
	DECLARE @TableName nvarchar(128)
	SET @TableName = @db + '.[dbo].' + @Tbl
	SET @SQL =N'INSERT INTO MDM.[dbo].[Ref_Obj_Value]
	SELECT Ref_Obj.ID AS OBJ_ID,Row_id,[Ref_Obj_Att].ID AS OBJ_ATT_ID,REF_VALUE
	,''importAdmin'' AS [CREATED_BY],getdate() AS [CREATE_DATETIME] 
	,''importAdmin'' AS [MODIFIER_ID],getdate() AS [MODIFY_DATETIME]
	,0 Min, 0 Max, ''List'' AS [obj_type],''A'' AS [STATUS],NULL AS [MAKER_ID],NULL AS [CHECKER_ID], NULL AS [APPV_REJCT_DT], NULL AS [REJECT_REASONS]
	FROM (
		select *
		from 
		(
		  select ROW_NUMBER() OVER (Order by (select 1)) + ' + Convert(varchar,@StartRowId) + ' AS Row_id,' + @PivotColumnsCast + '
		  from ' + @TableName + '
		)RV
		unpivot
		(
		  REF_VALUE
		  for Ref_Obj_Att in (' + @PivotColumns + ')
		) unpiv
	)RV1
	LEFT OUTER JOIN  MDM.[dbo].[Ref_Obj_Att] ON [Ref_Obj_Att].obj_attribute = RV1.Ref_Obj_Att
	LEFT OUTER JOIN  MDM.[dbo].[Ref_Obj] ON [Ref_Obj].ID = [Ref_Obj_Att].obj_id
	WHERE Ref_Obj.ID = ' + Convert(varchar,@obj_id) + ''

    EXEC sp_executesql @SQL
    SET @SQL = Null

END


