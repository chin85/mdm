--EXEC [MDM].[dbo].[CreateTbl] @Source_Sys, @obj_id, @obj_value, @obj_value_desc
--GO

USE [MDM]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[GetObjAttVal] 
	@obj_id bigint,
	@Searchby varchar(50),
	@Searchval varchar(50)

WITH ENCRYPTION
AS
BEGIN


	--Declare necessary variables
	DECLARE   @SQLQuery AS NVARCHAR(MAX)
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
	DECLARE   @DislayPivotColumns AS NVARCHAR(MAX)
 
	--Get unique values of pivot column  
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute),@DislayPivotColumns = COALESCE(@DislayPivotColumns + ',','') + QUOTENAME(obj_attribute)+' as '+ QUOTENAME(att_desc)
	FROM (SELECT obj_attribute,att_desc FROM [dbo].[Ref_Obj_Att] where obj_id = @obj_id ) AS Pivotcol

 
	--Create the dynamic query with all the values for 
	--pivot column at runtime
	IF @Searchby <>'' AND @Searchval <>''
		SET   @SQLQuery = 
		N'SELECT Row_id ,' + @DislayPivotColumns + ' from (
			SELECT * FROM (
					select Row_id,obj_attribute,obj_value
					from [dbo].[Ref_Obj]
					left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
					left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
					where Ref_Obj.ID = ' + Convert(varchar,@obj_id) + '
			)a
			pivot ( 
				min(obj_value) for obj_attribute in (' + @PivotColumns + ' )
			)b
		)c
		WHERE ' + @Searchby + '  like ''%' + @Searchval + '%'''
	ELSE
		SET   @SQLQuery = 
		N'SELECT Row_id ,' + @DislayPivotColumns + ' FROM (
			select Row_id,obj_attribute,obj_value
			from [dbo].[Ref_Obj]
			left outer join [dbo].[Ref_Obj_Att] on [Ref_Obj].ID = [Ref_Obj_Att].obj_id
			left outer join [dbo].[Ref_Obj_Value] on [Ref_Obj_Value].obj_id = [Ref_Obj].ID and [Ref_Obj_Value].obj_Att_id = [Ref_Obj_Att].ID
			where Ref_Obj.ID = ' + Convert(varchar,@obj_id) + ' 
		)a
		pivot ( 
		  min(obj_value) for obj_attribute in (' + @PivotColumns + ' )
		)b'
 
	--Execute dynamic query
	EXEC sp_executesql @SQLQuery
	--print @SQLQuery
 
END



