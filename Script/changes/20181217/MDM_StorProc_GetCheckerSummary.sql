USE [MDM]
GO
/****** Object:  StoredProcedure [dbo].[GetObjVal]    Script Date: 17/12/2018 6:09:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetCheckerSummary]
@obj_id bigint
WITH ENCRYPTION
AS

BEGIN
IF @obj_id > 0
	SELECT Ref_Obj_Value.obj_id,Ref_proj.Project, Ref_Source.Source_Sys,Ref_Obj.obj,COUNT(DISTINCT Ref_Obj_Value.row_id)AS RcdCnt 
	FROM [MDM]..Ref_Obj_Value
	LEFT OUTER JOIN [MDM]..Ref_Obj on Ref_Obj_Value.obj_id = Ref_Obj.ID
	LEFT OUTER JOIN [MDM]..Ref_Source on Ref_Source.ID = Ref_Obj.Source_Sys and Ref_Obj.Proj_id = Ref_Source.Proj_Id
	LEFT OUTER JOIN [MDM]..Ref_proj on Ref_Source.Proj_Id = Ref_Proj.ID
	WHERE Ref_Obj_Value.[status]='O' AND Ref_Obj_Value.obj_id = @obj_id
	GROUP BY Ref_Obj_Value.obj_id ,Ref_proj.Project, Ref_Source.Source_Sys,Ref_Obj.obj
ELSE
	SELECT Ref_Obj_Value.obj_id,Ref_proj.Project, Ref_Source.Source_Sys,Ref_Obj.obj,COUNT(DISTINCT Ref_Obj_Value.row_id)AS RcdCnt 
	FROM [MDM]..Ref_Obj_Value
	LEFT OUTER JOIN [MDM]..Ref_Obj on Ref_Obj_Value.obj_id = Ref_Obj.ID
	LEFT OUTER JOIN [MDM]..Ref_Source on Ref_Source.ID = Ref_Obj.Source_Sys and Ref_Obj.Proj_id = Ref_Source.Proj_Id
	LEFT OUTER JOIN [MDM]..Ref_proj on Ref_Source.Proj_Id = Ref_Proj.ID
	WHERE Ref_Obj_Value.[status]='O'
	GROUP BY Ref_Obj_Value.obj_id,Ref_proj.Project, Ref_Source.Source_Sys,Ref_Obj.obj

END
