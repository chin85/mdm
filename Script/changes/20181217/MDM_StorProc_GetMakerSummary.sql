USE [MDM]
GO
/****** Object:  StoredProcedure [dbo].[GetObjVal]    Script Date: 17/12/2018 6:09:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetMakerSummary]
@obj_id bigint
WITH ENCRYPTION
AS

BEGIN
IF @obj_id > 0

	SELECT OBJ_ID,PROJECT,SOURCE_SYS,OBJ,SUM(APPROVED) AS APPROVED,SUM(REJECTED) AS REJECTED,SUM(AWAITING_APPROVAL) AS AWAITING_APPROVAL FROM(
		SELECT ROW_ID,OBJ_ID,PROJECT, SOURCE_SYS,OBJ,CASE WHEN [STATUS] ='A' THEN 1 ELSE 0 END AS APPROVED
		,CASE WHEN [STATUS] ='R' THEN 1 ELSE 0 END AS REJECTED
		,CASE WHEN [STATUS] ='O' THEN 1 ELSE 0 END AS AWAITING_APPROVAL
		FROM(
			SELECT ROW_ID,REF_OBJ_VALUE.OBJ_ID,REF_PROJ.PROJECT, REF_SOURCE.SOURCE_SYS,REF_OBJ.OBJ,STATUS
			FROM [MDM]..REF_OBJ_VALUE
			LEFT OUTER JOIN [MDM]..REF_OBJ ON REF_OBJ_VALUE.OBJ_ID = REF_OBJ.ID
			LEFT OUTER JOIN [MDM]..REF_SOURCE ON REF_SOURCE.ID = REF_OBJ.SOURCE_SYS AND REF_OBJ.PROJ_ID = REF_SOURCE.PROJ_ID
			LEFT OUTER JOIN [MDM]..REF_PROJ ON REF_SOURCE.PROJ_ID = REF_PROJ.ID
			WHERE MAKER_ID IS NOT NULL AND Ref_Obj_Value.obj_id = @obj_id
			GROUP BY ROW_ID,REF_OBJ_VALUE.OBJ_ID,REF_PROJ.PROJECT, REF_SOURCE.SOURCE_SYS,REF_OBJ.OBJ,STATUS
		)M1 
		)M2
		GROUP BY OBJ_ID,PROJECT,SOURCE_SYS,OBJ
		ORDER BY OBJ

ELSE
	SELECT OBJ_ID,PROJECT,SOURCE_SYS,OBJ,SUM(APPROVED) AS APPROVED,SUM(REJECTED) AS REJECTED,SUM(AWAITING_APPROVAL) AS AWAITING_APPROVAL FROM(
		SELECT ROW_ID,OBJ_ID,PROJECT, SOURCE_SYS,OBJ,CASE WHEN [STATUS] ='A' THEN 1 ELSE 0 END AS APPROVED
		,CASE WHEN [STATUS] ='R' THEN 1 ELSE 0 END AS REJECTED
		,CASE WHEN [STATUS] ='O' THEN 1 ELSE 0 END AS AWAITING_APPROVAL
		FROM(
			SELECT ROW_ID,REF_OBJ_VALUE.OBJ_ID,REF_PROJ.PROJECT, REF_SOURCE.SOURCE_SYS,REF_OBJ.OBJ,STATUS
			FROM [MDM]..REF_OBJ_VALUE
			LEFT OUTER JOIN [MDM]..REF_OBJ ON REF_OBJ_VALUE.OBJ_ID = REF_OBJ.ID
			LEFT OUTER JOIN [MDM]..REF_SOURCE ON REF_SOURCE.ID = REF_OBJ.SOURCE_SYS AND REF_OBJ.PROJ_ID = REF_SOURCE.PROJ_ID
			LEFT OUTER JOIN [MDM]..REF_PROJ ON REF_SOURCE.PROJ_ID = REF_PROJ.ID
			WHERE MAKER_ID IS NOT NULL
			GROUP BY ROW_ID,REF_OBJ_VALUE.OBJ_ID,REF_PROJ.PROJECT, REF_SOURCE.SOURCE_SYS,REF_OBJ.OBJ,STATUS
		)M1 
		)M2
		GROUP BY OBJ_ID,PROJECT,SOURCE_SYS,OBJ
	ORDER BY OBJ
END
