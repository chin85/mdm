/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[SCREEN_CODE]
      ,[SCREEN_NAME]
      ,[MODULE]
      ,[PATH]
      ,[CREATOR_ID]
      ,[CREATE_DATETIME]
      ,[MODIFIER_ID]
      ,[MODIFY_DATETIME]
  FROM [MDM].[dbo].[SCREEN_MAINTENANCE]

  insert [MDM].[dbo].[SCREEN_MAINTENANCE]
  select 'DB001','CheckerDashboard','Dashboard','CheckerDashboard.aspx','sysadmin',getdate(),'sysadmin',getdate()

   insert [MDM].[dbo].[SCREEN_MAINTENANCE]
  select 'DB002','MakerDashboard','Dashboard','MakerDashboard.aspx','sysadmin',getdate(),'sysadmin',getdate()