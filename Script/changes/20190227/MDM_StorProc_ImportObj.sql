USE [MDM]
GO
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter procedure [dbo].[ImportObj] 
	@projid bigint,
    @db varchar(128),
    @tbl varchar(128),
    @obj varchar(50),
    @constraint varchar(max),
    @coll varchar(max),
    @sys bigint,
    @login varchar(50)
WITH ENCRYPTION
AS
BEGIN
 
     DECLARE @SQL as nvarchar(max)
 
    --Ref_Obj
    INSERT INTO MDM.[dbo].[Ref_Obj] 
    SELECT 
		@projid proj_id,
        @obj obj, 
        @sys Source_Sys, 
        @login CREATED_BY, 
        CURRENT_TIMESTAMP CREATE_DATETIME, 
        @login MODIFIER_ID, 
        CURRENT_TIMESTAMP MODIFY_DATETIME


	DECLARE @obj_id int
	SET @obj_id = Coalesce((select max(ID) from MDM.[dbo].[Ref_Obj] WHERE obj = @obj AND Source_Sys = @sys AND Proj_id = @projid ), 0)
	set @coll = '''' + Replace(@coll, ',', ''',''') + ''''


	--Ref_Obj_Att
	  IF (@constraint ='0')
		BEGIN
			SET @SQL = N'INSERT INTO MDM.[dbo].[Ref_Obj_Att]
			SELECT ' + Convert(varchar,@obj_id) + ',COLUMN_NAME as [obj_attribute],COLUMN_NAME as att_desc,''N'',0,null,null,null,null,'''+ @login + ''', CURRENT_TIMESTAMP,'''+ @login + ''', CURRENT_TIMESTAMP FROM '+ @db +'.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '''+ @tbl +''''
		END
	  ELSE 
		BEGIN
			SET @SQL = N'INSERT INTO MDM.[dbo].[Ref_Obj_Att]
				SELECT ' + Convert(varchar,@obj_id) + ',COLUMN_NAME as [obj_attribute],COLUMN_NAME as att_desc,''N'',0,null,null,null,null,'''+ @login + ''', CURRENT_TIMESTAMP,'''+ @login + ''', CURRENT_TIMESTAMP
				FROM ' + coalesce(@db + '.', '') + 'INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = ''' + @tbl + ''' AND COLUMN_NAME in (' + @coll + ')'
		END

	EXEC sp_executesql @SQL
	SET @SQL = Null
	
	--Get unique values of pivot column  
	DECLARE   @PivotColumns AS NVARCHAR(MAX)
	DECLARE   @PivotColumnsCast AS NVARCHAR(MAX)
	SELECT @PivotColumns= COALESCE(@PivotColumns + ',','') + QUOTENAME(obj_attribute),@PivotColumnsCast = COALESCE(@PivotColumnsCast + ',','') + 'Cast('+QUOTENAME(obj_attribute)+' AS VARCHAR) collate database_default as '+ QUOTENAME(obj_attribute)
	FROM (SELECT obj_attribute FROM MDM.[dbo].[Ref_Obj_Att] WHERE obj_id =@obj_id) AS Pivotcol

	--Ref_Obj_Value
	DECLARE @TableName nvarchar(128)
	SET @TableName = @db + '.[dbo].' + @Tbl
	SET @SQL =N'INSERT INTO MDM.[dbo].[Ref_Obj_Value]
	SELECT Ref_Obj.ID AS OBJ_ID,Row_id,[Ref_Obj_Att].ID AS OBJ_ATT_ID,REF_VALUE
	,''importAdmin'' AS [CREATED_BY],getdate() AS [CREATE_DATETIME] 
	,''importAdmin'' AS [MODIFIER_ID],getdate() AS [MODIFY_DATETIME]
	,0 Min, 0 Max, ''List'' AS [obj_type],''A'' AS [STATUS],NULL AS [MAKER_ID],NULL AS [CHECKER_ID], NULL AS [APPV_REJCT_DT], NULL AS [REJECT_REASONS]
	FROM (
		select *
		from 
		(
		  select ROW_NUMBER() OVER (Order by (select 1)) AS Row_id,' + @PivotColumnsCast + '
		  from ' + @TableName + '
		)RV
		unpivot
		(
		  REF_VALUE
		  for Ref_Obj_Att in (' + @PivotColumns + ')
		) unpiv
	)RV1
	LEFT OUTER JOIN  MDM.[dbo].[Ref_Obj_Att] ON [Ref_Obj_Att].obj_attribute = RV1.Ref_Obj_Att
	LEFT OUTER JOIN  MDM.[dbo].[Ref_Obj] ON [Ref_Obj].ID = [Ref_Obj_Att].obj_id
	WHERE Ref_Obj.ID = ' + Convert(varchar,@obj_id) + ''

    EXEC sp_executesql @SQL
    SET @SQL = Null

END


--exec  [dbo].[ImportObj] 
--	1,
--    'CTR',
--    'Ref_CTR',
--    'Ref_CTR',
--    '2',
--    'RefCTR_ID,RefCTR_NM',
--    2,
--    'sysadmin'
