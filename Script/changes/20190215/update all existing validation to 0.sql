/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ID]
      ,[obj_id]
      ,[obj_attribute]
      ,[att_desc]
      ,[att_data_mapped]
      ,[ref_rule_id]
      ,[field_no]
      ,[rule_col]
      ,[rule_row]
      ,[lov_att_id]
      ,[CREATED_BY]
      ,[CREATE_DATETIME]
      ,[MODIFIER_ID]
      ,[MODIFY_DATETIME]
  FROM [MDM].[dbo].[Ref_Obj_Att]

  update [MDM].[dbo].[Ref_Obj_Att] set ref_rule_id =0,field_no = 0,rule_col =0,rule_row = 0 where id <> 10021