USE [MDM]
GO

/****** Object:  Table [dbo].[Ref_Rule]    Script Date: 15/2/2019 9:47:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Ref_Rule_Dtl](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Ref_Rule_ID] bigint NULL,
	[No]varchar(50) null,
	[Item]varchar(256) null,
	Field_No varchar(256) null,
	Field_Name varchar(256) null,
	Field_Type varchar(256) null,
	Field_Length varchar(256) null,
	Rule_No varchar(256) null,
	Rules_Short_Name varchar(256) null,
	Rules varchar(256) null,
	Visual_Basic varchar(512) null,
	Primary_Key  varchar(256) null,
	Valid_Flag  varchar(256) null,
	Reference_Table  varchar(256) null,
	DB_Ref_Table  varchar(256) null,
	DB_Ref_Field  varchar(256) null,
	[CREATED_BY] [varchar](100) NOT NULL,
	[CREATE_DATETIME] [datetime] NOT NULL,
	[MODIFIER_ID] [varchar](100) NOT NULL,
	[MODIFY_DATETIME] [datetime] NOT NULL,
 CONSTRAINT [PK_Ref_Rule_Dtl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


