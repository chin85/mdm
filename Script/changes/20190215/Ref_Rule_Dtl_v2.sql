USE [MDM]
GO
/****** Object:  Table [dbo].[Ref_Rule_Dtl]    Script Date: 16/2/2019 2:51:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ref_Rule_Dtl](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Ref_Rule_ID] [bigint] NULL,
	[No] [varchar](50) NULL,
	[Item] [varchar](256) NULL,
	[Field_No] [varchar](256) NULL,
	[Field_Name] [varchar](256) NULL,
	[Field_Type] [varchar](256) NULL,
	[Field_Length] [varchar](256) NULL,
	[Rule_No] [varchar](256) NULL,
	[Rules_Short_Name] [varchar](256) NULL,
	[Rules] [varchar](256) NULL,
	[Visual_Basic] [varchar](512) NULL,
	[Primary_Key] [varchar](256) NULL,
	[Valid_Flag] [varchar](256) NULL,
	[Reference_Table] [varchar](256) NULL,
	[DB_Ref_Table] [varchar](256) NULL,
	[DB_Ref_Field] [varchar](256) NULL,
	[CREATED_BY] [varchar](100) NOT NULL,
	[CREATE_DATETIME] [datetime] NOT NULL,
	[MODIFIER_ID] [varchar](100) NOT NULL,
	[MODIFY_DATETIME] [datetime] NOT NULL,
 CONSTRAINT [PK_Ref_Rule_Dtl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Ref_Rule_Dtl] ON 

GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (1, 10002, N'1', N'no', N'1', N'no', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed.', N'IF IsNumeric(A(no)) = True AND A(no) <> "" THEN 
Result(no, 1) = True 
END IF', N'N', N'N', N'N', N'Ref_10_IOC', N'Ref_ID', N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (2, 10002, N'2', N'rim_no', N'2', N'rim_no', N'varchar', N'256', N'1', N'Mandatory. Blank is not allowed.', N'Mandatory.
Blank is not allowed. Only numeric is allowed.', N'IF IsNumeric(A(rim_no)) = True AND A(rim_no) <> "" THEN 
Result(rim_no, 1) = True 
END IF', N'N', N'Y', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (3, 10002, N'3', N'group', N'3', N'group', N'varchar', N'256', N'1', N'Mandatory. Blank is not allowed.', N'Mandatory.
Blank is not allowed.', N'IF isAlpha(A(group )) = True AND A(group ) <> ""  THEN 
Result(group, 1) = True
END IF', N'N', N'Y', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (4, 10002, N'4', N'company', N'4', N'company', N'varchar', N'256', N'1', N'Mandatory. Blank is not allowed.', N'Mandatory.
Blank is not allowed.', N'IF isAlpha(A(company )) = True AND A(company ) <> "" THEN 
Result(company, 1) = True
END IF', N'N', N'Y', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (5, 10002, N'5', N'limit', N'5', N'limit', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed. Only numeric is allowed.', N'IF IsNumeric(A(limit )) = True  THEN 
Result(limit, 1) = True 
END IF', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (6, 10002, N'6', N'type_of_col', N'6', N'type_of_col', N'varchar', N'256', N'1', N'Mandatory. Refer Q_type_of_col.', N'Mandatory.
Blank is not allowed.
Refer Q_type_of_col.', N'IF isAlpha(A(type_of_col )) = True AND A(type_of_col ) <> "" THEN 
Result(type_of_col, 1) = True
END IF', N'N', N'Y', N'Y', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (7, 10002, N'7', N'tgb_secu', N'7', N'tgb_secu', N'varchar', N'256', N'1', N'Mandatory. Blank is not allowed.', N'Mandatory.
Blank is not allowed.', N'IF isAlphaNum(A(tgb_secu)) = True AND A(tgb_secu) <> "" THEN 
Result(tgb_secu, 1) = True
END IF', N'N', N'Y', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (8, 10002, N'8', N'omv', N'8', N'omv', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed. Only numeric is allowed.', N'IF IsNumeric(A(omv )) = True  THEN 
Result(omv, 1) = True 
END IF', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (9, 10002, N'9', N'secu_cover', N'9', N'secu_cover', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed. Only numeric is allowed.', N'IF IsNumeric(A(secu_cover )) = True  THEN 
Result(secu_cover, 1) = True 
END IF', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (10, 10002, N'10', N'ascr_val', N'10', N'ascr_val', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed. Only numeric is allowed.', N'IF IsNumeric(A(ascr_val )) = True  THEN 
Result(ascr_val, 1) = True 
END IF', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (11, 10002, N'11', N'secu_cover_ascr', N'11', N'secu_cover_ascr', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed. Only numeric is allowed.', N'IF IsNumeric(A(secu_cover_ascr )) = True  THEN 
Result(secu_cover_ascr, 1) = True 
END IF', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
INSERT [dbo].[Ref_Rule_Dtl] ([ID], [Ref_Rule_ID], [No], [Item], [Field_No], [Field_Name], [Field_Type], [Field_Length], [Rule_No], [Rules_Short_Name], [Rules], [Visual_Basic], [Primary_Key], [Valid_Flag], [Reference_Table], [DB_Ref_Table], [DB_Ref_Field], [CREATED_BY], [CREATE_DATETIME], [MODIFIER_ID], [MODIFY_DATETIME]) VALUES (12, 10002, N'12', N'remarks', N'12', N'remarks', N'varchar', N'256', N'1', N'Optional. Blank is allowed.', N'Optional.
Blank is allowed.', N'Result(remarks, 1) = True', N'N', N'N', N'N', NULL, NULL, N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime), N'sysadmin', CAST(N'2019-02-15 15:04:51.210' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Ref_Rule_Dtl] OFF
GO
